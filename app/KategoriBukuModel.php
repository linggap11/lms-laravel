<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriBukuModel extends Model
{
  protected $table = 'tb_kategori';
  protected $primaryKey = 'id_kategori';
  public $timestamps = false;
}
