<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BeritaModel extends Model
{
    protected $table = 'tb_berita';
    protected $primaryKey = 'id_berita';
    public $timestamps = false;
}
