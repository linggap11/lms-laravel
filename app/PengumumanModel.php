<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PengumumanModel extends Model
{
  protected $table = 'tb_pengumuman';
  protected $primaryKey = 'id_pengumuman';
  public $timestamps = false;
}
