<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PetugasModel extends Model
{
    protected $table = 'tb_petugas';
    protected $primaryKey = 'id_petugas';
    public $timestamps = false;

}
