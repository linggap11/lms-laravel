<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnggotaModel extends Model
{
  protected $table = 'tb_anggota';
  protected $primaryKey = 'id_anggota';
  public $timestamps = false;
}
