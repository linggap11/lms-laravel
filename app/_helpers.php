<?php

  function maks_length_admin($body) {
    $string = strip_tags($body);
    if (strlen($string) > 300) {

        // truncate string
        $stringCut = substr($string, 0, 300);
        $endPoint = strrpos($stringCut, ' ');

        //if the string doesn't contain any space then it will cut without word basis.
        $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
        $string .= '...';
    }
    return $string;
  }

  function maks_length($body) {
  	$string = strip_tags($body);
  	if (strlen($string) > 160) {

  	    // truncate string
  	    $stringCut = substr($string, 0, 160);
  	    $endPoint = strrpos($stringCut, ' ');

  	    //if the string doesn't contain any space then it will cut without word basis.
  	    $string = $endPoint? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
  	    $string .= '...';
  	}
  	return $string;
  }
  
 ?>
