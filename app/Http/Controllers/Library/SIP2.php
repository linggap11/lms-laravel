<?php

namespace App\Http\Controllers\Library;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SIP2 extends Controller
{
  /**
  * Attempt to authenticate the current user.
  *
  * @return object User object if successful, PEAR_Error otherwise.
  * @access public
  */
  public function authenticate(Request $request)
  {
     global $configArray;
     $rfid = $request->rfid;
     if (isset($_POST['username']) && isset($_POST['password'])) {
         $username = $_POST['username'];
         $password = $_POST['password'];
         if ($username != '' && $password != '') {
             // Attempt SIP2 Authentication
             $mysip = new sip2();
             $mysip->hostname = $configArray['SIP2']['host'];
             $mysip->port = $configArray['SIP2']['port'];
             if ($mysip->connect()) {
                 //send selfcheck status message
                 $in = $mysip->msgSCStatus();
                 $msg_result = $mysip->get_message($in);
                 // Make sure the response is 98 as expected
                 if (preg_match("/^98/", $msg_result)) {
                     $result = $mysip->parseACSStatusResponse($msg_result);
                     //  Use result to populate SIP2 setings
                     $mysip->AO = $result['variable']['AO'][0];
                     $mysip->AN = $result['variable']['AN'][0];
                     $mysip->patron = $username;
                     $mysip->patronpwd = $password;
                     $in = $mysip->msgPatronStatusRequest();
                     $msg_result = $mysip->get_message($in);
                     // Make sure the response is 24 as expected
                     if (preg_match("/^24/", $msg_result)) {
                         $result = $mysip->parsePatronStatusResponse($msg_result);
                         if ($result['variable']['BL'][0] == 'Y' and $result['variable']['CQ'][0] == 'Y') {
                             // Success!!!
                             $user = $this->_processSIP2User($result, $username, $password);
                             // Set login cookie for 1 hour
                             $user->password = $password;
                             // Need this for Metalib
                         } else {
                             $user = "Error";
                         }
                     } else {
                         $user ="Error";
                     }
                 } else {
                     $user = "Error";
                 }
                 $mysip->disconnect();
             } else {
                 $user = "Error";
             }
         } else {
             $user = "Error";
         }
     } else {
         $user = "Error12312";
     }
     return $user;
  }
}
