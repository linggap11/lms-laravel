<?php

namespace App\Http\Controllers\Main;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AnggotaModel;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    public function login_proses(Request $request) {
      $nim = $request->nim;
      $password = $request->password;

      $cek_anggota = AnggotaModel::where('nim', $nim)->first();
      if ($cek_anggota) {
        if(Hash::check($password,$cek_anggota->password)){
          Session::put('id_anggota',$cek_anggota->id_anggota);
          Session::put('nim',$cek_anggota->nim);
          Session::put('nama_anggota',$cek_anggota->nama_anggota);
          Session::put('foto',$cek_anggota->foto);
          return redirect('/');
        } else {
          session()->flash('message', 'Password yang anda masukkan salah ...');
          return redirect('/login');
        }
      } else {
        session()->flash('message', 'NIM tidak diketahui !');
        return redirect('/login');
      }
    }

    public function logout(){
      Session::flush();
      return redirect('/')->with('alert','Kamu sudah logout');
    }

    public function register_submit(Request $request) {
      $anggota = new AnggotaModel;
      $anggota->nim = $request->nim;
      $anggota->nama_anggota = $request->nama_anggota;
      $anggota->alamat = $request->alamat;
      $anggota->no_hp = $request->telp;
      $anggota->email = $request->email;
      $anggota->password = $request->password;
      $anggota->save();
      session()->flash('message_tambah', $request->nim. ' Berhasil Didaftarkan ...');
      return redirect('/login');;
    }

    public function testApi(AnggotaModel $anggota) {
      $data_anggota = $anggota->all();
      return response()->json($data_anggota);
    }
}
