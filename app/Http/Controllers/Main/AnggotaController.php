<?php

namespace App\Http\Controllers\Main;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\AnggotaModel;
use App\PengumumanModel;
use DB;

class AnggotaController extends Controller
{
    public function dashboard() {
      if (Session::has('id_anggota')) {
        $id = Session::get('id_anggota');
        $data['data_anggota'] = AnggotaModel::find($id)->first();
        $data['pengumuman'] = PengumumanModel::take(1)->orderBy('id_pengumuman','desc')->first();
        return view('anggota.dashboard', compact('data'));
      } else {
        return Redirect('/login');
      }

    }

    public function riwayat_peminjaman() {
      $data['data_peminjaman'] = DB::table('tb_peminjaman')
        ->join('tb_anggota', 'tb_anggota.id_anggota', '=', 'tb_peminjaman.id_anggota')
        ->join('tb_det_peminjaman', 'tb_peminjaman.id_peminjaman', '=', 'tb_det_peminjaman.id_peminjaman')
        ->join('tb_buku', 'tb_det_peminjaman.id_buku', '=', 'tb_buku.id_buku')
        ->where('tb_peminjaman.id_anggota',Session::get('id_anggota'))
        ->orderBy('tb_peminjaman.tgl_pinjam','desc')
        ->get();
      return view('anggota.riwayat_peminjaman', compact('data'));
    }

    public function usulan_buku() {
      $data['data_usulan'] = DB::table('tb_usulan_buku')
        ->join('tb_anggota', 'tb_anggota.id_anggota', '=', 'tb_usulan_buku.id_anggota')
        ->where('tb_usulan_buku.id_anggota',Session::get('id_anggota'))
        ->orderBy('tb_usulan_buku.tanggal','desc')
        ->get();
      return view('anggota.usulan_buku', compact('data'));
    }

    public function profile($nim) {
      $data['profile'] = DB::table('tb_anggota')->where('nim', $nim)->first();
      return view('anggota.profile', compact('data'));
    }

    public function update(Request $request, $id) {
      $anggota = AnggotaModel::find($id);
      $foto = $request->file('foto');
      if ($foto != null) {
        $new_foto = rand().'.'.$foto->getClientOriginalExtension();
        $foto->move(public_path('uploads/anggota/'), $new_foto);
      } else {
        $new_foto = null;
      }
      $anggota->nim = $request->nim;
      $anggota->nama_anggota = $request->nama_anggota;
      $anggota->pangkat = $request->pangkat;
      $anggota->alamat = $request->alamat;
      $anggota->no_hp = $request->telp;
      $anggota->email = $request->email;
      $anggota->foto = $new_foto;
      $anggota->password = $request->password;
      $anggota->save();
      session()->flash('message_update', $request->nim.' Berhasil Diperbaharui');
      return redirect('/anggota/profile/'.Session::get('nim').'');;
    }

    public function registerAPI(Request $request) {
      $anggota = new AnggotaModel;
      $anggota->nim = $request->nim;
      $anggota->nama_anggota = $request->nama_anggota;
      $anggota->alamat = $request->alamat;
      $anggota->no_hp = $request->telp;
      $anggota->email = $request->email;
      $anggota->password = $request->password;
      // $anggota->save();

      $data = [
        'nim' =>  $request->input('nim'),
        'nama' =>  $request->input('nama_anggota'),
        'alamat' =>  $request->input('alamat'),
        'telp' =>  $request->input('telp'),
        'email' =>  $request->input('email'),
        'password' =>  Hash::make($request->input('password')),
        'method' =>'GET'
      ];

      $resp = [
        'message' => 'Registrasi',
        'data' => $data
      ];

      return response()->json($resp, 201);
    }

    public function loginAPI(Request $request) {
      $anggota = new AnggotaModel;
      $anggota->nim = $request->nim;
      $anggota->nama_anggota = $request->nama_anggota;
      $anggota->alamat = $request->alamat;
      $anggota->no_hp = $request->telp;
      $anggota->email = $request->email;
      $anggota->password = $request->password;
      // $anggota->save();

      $data = [
        'nim' =>  $request->input('nim'),
        'password' =>  Hash::make($request->input('password')),
        'method' =>'GET'
      ];

      $resp = [
        'message' => 'Login',
        'data' => $data
      ];

      return response()->json($resp, 201);
    }

    public function testApi(AnggotaModel $anggota) {
      $data_anggota = $anggota->all();
      return response()->json($data_anggota);
    }

}
