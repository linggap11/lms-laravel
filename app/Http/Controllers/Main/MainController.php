<?php

namespace App\Http\Controllers\Main;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Session;
use App\BeritaModel;
use App\PengumumanModel;
use App\AnggotaModel;
use DB;

class MainController extends Controller
{
    public function index() {
      $data['data_berita'] = BeritaModel::take(3)->orderBy('id_berita', 'desc')->get();
      $data['pengumuman'] = PengumumanModel::take(1)->orderBy('id_pengumuman','desc')->first();
      $data['slider'] = DB::table('slider')->first();
      return view('main.main', compact('data'));
    }

    public function login_page() {
      if (Session::get('id_anggota')) {
        return redirect('/');
      } else {
        $data['data_berita'] = BeritaModel::take(3)->orderBy('id_berita', 'desc')->get();
        $data['pengumuman'] = PengumumanModel::take(1)->orderBy('id_pengumuman','desc')->first();
        return view('main.login', compact('data'));
      }
    }

    public function register_page() {
      if (Session::get('id_anggota')) {
        return redirect('/');
      } else {
        $data['data_berita'] = BeritaModel::take(3)->orderBy('id_berita', 'desc')->get();
        $data['pengumuman'] = PengumumanModel::take(1)->orderBy('id_pengumuman','desc')->first();
        return view('main.register', compact('data'));
      }
    }

    public function akun() {
      if (!Session::get('id_anggota')) {
        return redirect('/login');
      } else {
        $data['data_berita'] = BeritaModel::take(3)->orderBy('id_berita', 'desc')->get();
        $data['pengumuman'] = PengumumanModel::take(1)->orderBy('id_pengumuman','desc')->first();
        $data['profile'] = AnggotaModel::find(Session::get('id_anggota'))->first();
        return view('main.akun', compact('data'));
      }
    }

}
