<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\PerpustakaanModel;

class PengaturanController extends Controller
{
    public function setting_perpus()
    {
      $data['title'] = "Pengaturan Perpustakaan - LMS";
      $data['data_perpus'] = DB::table('tb_perpus_profile')->first();

      return view('admin.pengaturan.perpustakaan', compact('data'));
    }


    public function update_perpus(Request $request, $id) {
      $perpus = PerpustakaanModel::find($id);
      $logo = $request->file('logo');
      if ($logo != null) {
        $new_logo = rand().'.'.$logo->getClientOriginalExtension();
        $logo->move(public_path('uploads/'), $new_logo);
        $perpus->logo = $new_logo;
      }
      $perpus->nama_perpus = $request->nama;
      $perpus->alias = $request->alias;
      $perpus->alamat = $request->alamat;
      $perpus->no_telp = $request->telp;
      $perpus->email = $request->email;
      $perpus->id_petugas = $request->id_petugas;
      $perpus->save();
      session()->flash('message_update', '');
      return redirect('admin/setting_perpus');
    }


    public function setting_tampilan() {
      $data['title'] = "Pengaturan Tampilan Website - LMS";
      $data['slider'] = DB::table('slider')->first();
      return view('admin.pengaturan.front_end', compact('data'));
    }

    public function setting_sop() {
      $data['title'] = "Pengaturan SOP - LMS";
      $data['sop'] = DB::table('sop')->orderBy('id', 'desc')->first();
      return view('admin.pengaturan.sop', compact('data'));
    }

    public function setting_keylock() {
      $data['title'] = "Keylock - LMS";
      $data['data_keylock'] = DB::table('tb_keylock')->orderBy('id', 'desc')->first();
      return view('admin.pengaturan.keylock', compact('data'));
    }

    public function update_keylock(Request $request) {
      DB::table('tb_keylock')->insert([
        'key1' => $request->key1,
        'key2' => $request->key2,
        'key3' => $request->key3,
      ]);
      session()->flash('message_update', '');
      return redirect('/admin/setting_keylock');
    }

    public function update_slider(Request $request) {
      $slider1 = $request->file('slider1');
      $slider2 = $request->file('slider2');
      $slider3 = $request->file('slider3');



      if ($slider1 != null) {
        $new_slider1 = rand().'.'.$slider1->getClientOriginalExtension();
        $slider1->move(public_path('uploads/slider/'), $new_slider1);
        DB::table('slider')->update(['pict1' => $new_slider1]);
      }
      if ($slider2 != null) {
        $new_slider2 = rand().'.'.$slider2->getClientOriginalExtension();
        $slider2->move(public_path('uploads/slider/'), $new_slider2);
        DB::table('slider')->update(['pict2' => $new_slider2]);
      }
      if ($slider3 != null) {
        $new_slider3 = rand().'.'.$slider3->getClientOriginalExtension();
        $slider3->move(public_path('uploads/slider/'), $new_slider3);
        DB::table('slider')->update(['pict3' => $new_slider3]);
      }
      session()->flash('message_update', '');
      return redirect('/admin/setting_tampilan');

    }

    public function submit_sop(Request $request) {
      $sop = $request->file('sop');
      $nama_file = $sop->getClientOriginalName();
      $sop->move(public_path('uploads/files/'), $nama_file);
      DB::table('sop')->insert(['file' => $nama_file]);
      session()->flash('message_update', '');
      return redirect('/admin/setting_sop');
    }
}
