<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\KategoriBukuModel;


class KategoriBukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data['title'] = "Kategori Buku - LMS";
      $data['kategori'] = DB::table('tb_buku')
                ->select(DB::raw('nama_kategori, count(judul) as jumlah_buku, tb_kategori.id_kategori '))
                ->rightJoin('tb_kategori', 'tb_buku.id_kategori' ,'=','tb_kategori.id_kategori')
                ->groupBy('tb_kategori.nama_kategori')
                ->groupBy('tb_kategori.id_kategori')
                ->orderBy('nama_kategori','asc')
                ->get();
      return view('admin.buku.kategori_buku', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $kategori = new KategoriBukuModel;
      $this->validate($request, [
        'nama_kategori' => 'required',
      ]);
      $kategori->nama_kategori = $request->nama_kategori;
      $kategori->save();
      session()->flash('message_tambah',$request->nama_kategori);
      return redirect('admin/kategori_buku');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $kategori = KategoriBukuModel::find($id);
      echo json_encode($kategori);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $kategori = KategoriBukuModel::find($id);
      $this->validate($request, [
        'nama_kategori' => 'required',
      ]);
      $kategori->nama_kategori = $request->nama_kategori;
      $kategori->save();
      session()->flash('message_update',$request->nama_kategori);
      return redirect('admin/kategori_buku');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kategori = KategoriBukuModel::find($id);
        $kategori->delete();
        session()->flash('message_hapus', $kategori->nama_kategori);
        return redirect('admin/kategori_buku');
    }
}
