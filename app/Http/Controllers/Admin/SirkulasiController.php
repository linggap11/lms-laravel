<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\AnggotaModel;
use App\BukuModel;
use DB;

class SirkulasiController extends Controller
{
    public function peminjaman() {
      $data['title'] = "Daftar Peminjaman - LMS";
      $data['data_peminjaman'] = DB::table('tb_peminjaman')
        ->join('tb_anggota', 'tb_anggota.id_anggota', '=', 'tb_peminjaman.id_anggota')
        ->join('tb_det_peminjaman', 'tb_peminjaman.id_peminjaman', '=', 'tb_det_peminjaman.id_peminjaman')
        ->join('tb_buku', 'tb_det_peminjaman.id_buku', '=', 'tb_buku.id_buku')
        ->where('tb_det_peminjaman.status','PINJAM')
        ->orderBy('tb_peminjaman.tgl_pinjam','desc')
        ->get();
      return view('admin.sirkulasi.peminjaman', compact('data'));
    }

    public function tambah_peminjaman(Request $request) {
      $anggota = AnggotaModel::where('nim', $request->nim)->select('id_anggota')->first();
      $buku = BukuModel::where('isbn', $request->isbn)->select('id_buku')->first();

      DB::table('tb_peminjaman')->insert([
        'tgl_pinjam' => $request->tgl_pinjam,
        'tgl_kembali' => $request->tgl_kembali,
        'id_anggota' => $anggota->id_anggota
      ]);

      $peminjaman = DB::table('tb_peminjaman')->select('id_peminjaman')->orderBy('id_peminjaman', 'desc')->first();
      DB::table('tb_det_peminjaman')->insert([
        'id_buku' => $buku->id_buku,
        'status' => "PINJAM",
        'keterangan' => $request->keterangan,
        'id_peminjaman' => $peminjaman->id_peminjaman,
      ]);

      session()->flash('message_tambah','');
      return redirect('admin/peminjaman');
    }

    public function get_detail_peminjaman($id) {
      $peminjaman = DB::table('tb_peminjaman')
      ->join('tb_anggota', 'tb_anggota.id_anggota', '=', 'tb_peminjaman.id_anggota')
      ->join('tb_det_peminjaman', 'tb_peminjaman.id_peminjaman', '=', 'tb_det_peminjaman.id_peminjaman')
      ->join('tb_buku', 'tb_det_peminjaman.id_buku', '=', 'tb_buku.id_buku')
      ->where('tb_peminjaman.id_peminjaman', $id)
      ->first();
      echo json_encode($peminjaman);
    }

    public function perpanjangan_buku(Request $request, $id) {
      DB::table('tb_peminjaman')->where('id_peminjaman', $id)->update([
        'tgl_kembali' => $request->tgl_kembali
      ]);
      session()->flash('message_perpanjangan','');
      return redirect('admin/peminjaman');
    }

    public function pengembalian_buku($id) {
      DB::table('tb_det_peminjaman')->where('id_peminjaman', $id)->update([
        'status' => "KEMBALI"
      ]);
      DB::table('tb_pengembalian')->insert([
        'terlambat' => 0,
        'id_peminjaman' => $id,
        'id_petugas' => Session::get('id_petugas'),
      ]);
      session()->flash('message_pengembalian', '');
      return redirect('admin/peminjaman');
    }

    public function get_anggota() {
      $data = AnggotaModel::select('nim')->get();
      echo json_encode($data);
    }

    public function get_buku() {
      $data = BukuModel::select('isbn')->get();
      echo json_encode($data);
    }




    public function pengembalian() {
      $data['title'] = "Daftar Pengembalian - LMS";
      $data['data_pengembalian'] = DB::table('tb_pengembalian')
        ->join('tb_peminjaman', 'tb_peminjaman.id_peminjaman', '=' ,'tb_pengembalian.id_peminjaman')
        ->join('tb_anggota', 'tb_anggota.id_anggota', '=' ,'tb_peminjaman.id_anggota')
        ->join('tb_det_peminjaman', 'tb_det_peminjaman.id_peminjaman', '=' ,'tb_peminjaman.id_peminjaman')
        ->join('tb_buku', 'tb_buku.id_buku', '=' ,'tb_det_peminjaman.id_buku')
        ->orderBy('tb_pengembalian.tgl_pengembalian', 'desc')
        ->get();
      return view('admin.sirkulasi.pengembalian', compact('data'));
    }

    public function riwayat_sirkulasi() {
      $data['title'] = "Riwayat Sirkulasi - LMS";
      return view('admin.sirkulasi.riwayat_sirkulasi' , compact('data'));
    }




}
