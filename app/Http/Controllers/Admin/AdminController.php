<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use App\PetugasModel;
use App\PerpustakaanModel;

class AdminController extends Controller
{
    public function index() {
      if (Session::get('id_petugas') == null) {
        return redirect('/admin');
      } else {
        $data['title'] = "Dashboard - LMS";
        $data['user_data'] = Session::get('id_petugas');
        $data['data_petugas'] = PetugasModel::where('id_petugas', Session::get('id_petugas'))->first();        
        return view('admin.dashboard', compact('data'));
      }
    }
}
