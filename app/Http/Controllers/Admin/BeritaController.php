<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\BeritaModel;
use App\PengumumanModel;
use DB;

class BeritaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data['title'] = "Berita & Artikel - LMS";
      $data['data_berita'] = BeritaModel::orderBy('id_berita', 'desc')->get();
      $data['pengumuman'] = DB::table('tb_pengumuman')->orderBy('id_pengumuman', 'desc')->first();

      return view('admin.berita.berita', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $berita = new BeritaModel;
      $berita->judul = $request->judul;
      $berita->body = $request->body;
      $berita->save();
      session()->flash('message_tambah', $berita->judul);
      return redirect('admin/berita');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $berita = BeritaModel::find($id);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title'] = 'Edit Berita - LMS';
        $data['berita'] = BeritaModel::find($id);
        return view('admin.berita.edit_berita', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $berita = BeritaModel::find($id);
      $berita->judul = $request->judul;
      $berita->body = $request->body;
      $berita->save();
      session()->flash('message_update', $berita->judul);
      return redirect('admin/berita');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $berita = BeritaModel::find($id);
      $berita->delete();
      session()->flash('message_hapus', $berita->judul);
      return redirect('/admin/berita');
    }

    public function tambah_berita()
    {
      $data['title'] = 'Tambah Berita - LMS';
      return view('admin.berita.tambah_berita', compact('data'));
    }

    public function update_pengumuman(Request $request) {
      $pengumuman = new PengumumanModel;
      $pengumuman->header = $request->header;
      $pengumuman->body = $request->isi;
      $pengumuman->id_petugas = $request->id_petugas;
      $pengumuman->save();
      session()->flash('message_pengumuman','');
      return redirect('admin/berita');
    }

}
