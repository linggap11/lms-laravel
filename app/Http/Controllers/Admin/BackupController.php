<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BackupController extends Controller
{
    public function index()
    {
      $data['title'] = "Back Up & Restore - LMS";

      return view('admin.backup_restore.back_up', compact('data'));
    }
}
