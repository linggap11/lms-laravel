<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\BukuModel;
use Illuminate\Support\Facades\Session;
use PDF;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data['title'] = "Buku - LMS";
      $data['data_buku'] = BukuModel::orderBy('id_buku', 'desc')->get();
      $data['kategori'] = DB::table('tb_kategori')->get();
      return view('admin.buku.daftar_buku', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $buku = new BukuModel;
        $cover = $request->file('cover_buku');
        if ($cover != null) {
          $new_cover = rand().'.'.$cover->getClientOriginalExtension();
          $cover->move(public_path('uploads/buku/'), $new_cover);
        } else {
          $new_cover = null;
        }
        $buku->isbn = $request->isbn;
        $buku->judul = $request->judul;
        $buku->pengarang = $request->pengarang;
        $buku->penerbit = $request->penerbit;
        $buku->edisi = $request->edisi;
        $buku->tahun_terbit = $request->tahun_terbit;
        $buku->jumlah_buku = $request->jumlah_buku;
        $buku->bentuk_fisik = $request->bentuk_fisik;
        $buku->jenis = $request->jenis;
        $buku->gambar_cover = $new_cover;
        $buku->id_kategori = $request->kategori;
        $buku->id_petugas = $request->id_petugas;
        $buku->save();
        session()->flash('message_tambah', $request->judul);
        return redirect('/admin/buku');;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $buku = BukuModel::find($id);
      echo json_encode($buku);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $data['title'] = 'Edit Buku - LMS';
      $data['data_buku'] = BukuModel::find($id);
      $data['kategori'] = DB::table('tb_kategori')->get();

      return view('admin.buku.edit_buku', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $buku = BukuModel::find($id);
        $this->validate($request, [
          'judul' => 'required',
          'pengarang' => 'required',
        ]);
        $cover = $request->file('cover_buku');
        if ($cover != null) {
          $new_cover = rand().'.'.$cover->getClientOriginalExtension();
          $cover->move(public_path('uploads/buku/'), $new_cover);
        } else {
          $new_cover = null;
        }
        $buku->isbn = $request->isbn;
        $buku->judul = $request->judul;
        $buku->pengarang = $request->pengarang;
        $buku->penerbit = $request->penerbit;
        $buku->edisi = $request->edisi;
        $buku->tahun_terbit = $request->tahun_terbit;
        $buku->jumlah_buku = $request->jumlah_buku;
        $buku->bentuk_fisik = $request->bentuk_fisik;
        $buku->jenis = $request->jenis;
        $buku->gambar_cover = $new_cover;
        $buku->id_kategori = $request->kategori;
        $buku->save();
        session()->flash('message_update', $request->judul);
        return redirect('/admin/buku');;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $buku = BukuModel::find($id);
        $buku->delete();
        session()->flash('message_hapus', $buku->judul);
        return redirect('/admin/buku');
    }

    public function label_buku()
    {
      $data['title'] = "Label Buku - LMS";
      $data['data_buku'] = DB::table('tb_buku')->get();

      return view('admin.buku.label_buku', compact('data'));
    }

    public function cetak_label(Request $request) {

      $id_buku = $request->cetak_label;
      $data['data_buku']= array();
      foreach ($id_buku as $buku) {
        $get_buku = BukuModel::find($buku);
        array_push($data['data_buku'], $get_buku);
      }
      $pdf = PDF::loadView('admin.buku.label_pdf', $data);
      return $pdf->stream('laebl_buku.pdf');
    }

    public function usulan_buku()
    {
      $data['title'] = "Usulan Buku - LMS";

      return view('admin.buku.usulan_buku', compact('data'));
    }

}
