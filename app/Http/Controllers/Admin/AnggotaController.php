<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use DB;
use PDF;
use App\AnggotaModel;

class AnggotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['title'] = "Anggota - LMS";
        $data['data_anggota'] = AnggotaModel::orderBy('id_anggota','DESC')->get();
        return view('admin.anggota.daftar_anggota', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $anggota = new AnggotaModel;
        $foto = $request->file('foto');
        if ($foto != null) {
          $new_foto = rand().'.'.$foto->getClientOriginalExtension();
          $foto->move(public_path('uploads/anggota/'), $new_foto);
        } else {
          $new_foto = null;
        }
        $anggota->nim = $request->nim;
        $anggota->nama_anggota = $request->nama_anggota;
        $anggota->pangkat = $request->pangkat;
        $anggota->alamat = $request->alamat;
        $anggota->no_hp = $request->telp;
        $anggota->email = $request->email;
        $anggota->foto = $new_foto;
        $anggota->password = Hash::make($request->password);
        $anggota->save();
        session()->flash('message_tambah', $request->nim);
        return redirect('/admin/anggota');;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data_anggota = AnggotaModel::find($id);
        echo json_encode($data_anggota);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_anggota = AnggotaModel::find($id);
        echo json_encode($data_anggota);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $anggota = AnggotaModel::find($id);
        $foto = $request->file('foto');
        if ($foto != null) {
          $new_foto = rand().'.'.$foto->getClientOriginalExtension();
          $foto->move(public_path('uploads/anggota/'), $new_foto);
        } else {
          $new_foto = null;
        }
        $anggota->nim = $request->nim;
        $anggota->nama_anggota = $request->nama_anggota;
        $anggota->pangkat = $request->pangkat;
        $anggota->alamat = $request->alamat;
        $anggota->no_hp = $request->telp;
        $anggota->email = $request->email;
        $anggota->foto = $request->$new_foto;
        $anggota->password = Hash::make($request->password);
        $anggota->save();
        session()->flash('message_update', $request->nim);
        return redirect('/admin/anggota');;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

      $anggota = AnggotaModel::find($id);
      $anggota->delete();
      session()->flash('massage_hapus', $anggota->nim);
      return redirect('/admin/anggota');
    }

    public function generate_memberid()
    {
      $data['title'] = "Generate Kartu Anggota - LMS";
      $data['data_anggota'] = DB::table('tb_anggota')->get();
      return view('admin.anggota.generate_memberid', compact('data'));
    }

    public function cetak_kartu(Request $request) {

      $id_anggota = $request->cetak_kartu;
      $data['data_anggota']= array();
      foreach ($id_anggota as $anggota) {
        $get_anggota = AnggotaModel::find($anggota);
        array_push($data['data_anggota'], $get_anggota);
      }
      $pdf = PDF::loadView('admin.anggota.kartu_pdf', $data);
      return $pdf->stream('kartu_anggota.pdf');
    }
}
