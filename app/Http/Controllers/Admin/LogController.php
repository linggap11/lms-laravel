<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class LogController extends Controller
{
    public function index() {
      $data['title'] = "Log Perpustakaan - LMS";

      return view('admin.log.log_perpus', compact('data'));
    }
}
