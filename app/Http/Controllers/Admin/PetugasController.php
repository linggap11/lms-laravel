<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\PetugasModel;

class PetugasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $data['title'] = "Pengaturan Petugas - LMS";
      $data['data_petugas'] = PetugasModel::get();
      return view('admin.petugas.petugas', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $petugas = new PetugasModel;
      $petugas->foto = $request->foto;
      $petugas->nama_petugas = $request->nama_petugas;
      $petugas->jenis_kelamin = $request->jenis_kelamin;
      $petugas->telp = $request->telp;
      $petugas->alamat = $request->alamat;
      $petugas->email = $request->email;
      $petugas->role = $request->role;
      $petugas->username = $request->username;
      $petugas->password = Hash::make($request->password);

      $petugas->save();
      session()->flash('message_tambah',$petugas->nama_petugas);
      return redirect('admin/setting_petugas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $data['title'] = 'Profile Petugas - LMS';
      $data['data_petugas'] = PetugasModel::find($id)->first();
      return view('admin.petugas.detail_petugas', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['title'] = 'Profile Petugas - LMS';
        $data['data_petugas'] = PetugasModel::find($id)->first();
        return view('admin.petugas.profile');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
