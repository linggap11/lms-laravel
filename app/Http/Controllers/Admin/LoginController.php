<?php

namespace App\Http\Controllers\Admin;

use App\LoginModel;
use App\PerpustakaanModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
  public function index(){
    if(!Session::has('login')){
      return view('admin.login');
    }
    else {
      return redirect('/admin/home');
    }
  }

  public function login(Request $request){
    $username = $request->username;
    $password = $request->password;

    $data = LoginModel::where('username',$username)->first();
    $perpus = PerpustakaanModel::first();
    if ($data) {
      if(Hash::check($password,$data->password)){
        Session::put('id_petugas',$data->id_petugas);
        Session::put('nama_petugas',$data->nama_petugas);
        Session::put('email',$data->email);
        Session::put('foto',$data->foto);
        Session::put('role',$data->role);
        Session::put('logo_perpus',$perpus->logo);
        Session::put('alias_perpus',$perpus->alias);
        Session::put('login',TRUE);
        return redirect('/admin/home');
      }
      else {
        return redirect('/admin')->with('alert','username atau password anda, Salah !');
      }
    }
    else {
      return redirect('/admin')->with('alert','username atau password anda, Salah!');
    }
  }

  public function logout(){
    Session::flush();
    return redirect('/admin')->with('alert',"You've been logged out successfully..");
  }

}
