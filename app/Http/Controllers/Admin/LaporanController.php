<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\BukuModel;

class LaporanController extends Controller
{
    public function laporan_buku()
    {
      $data['title'] = "Laporan Buku - LMS";
      $data['data_buku'] = BukuModel::get();

      return view('admin.laporan.laporan_buku', compact('data'));
    }

    public function laporan_anggota()
    {
      $data['title'] = "Laporan Anggota - LMS";

      return view('admin.laporan.laporan_anggota', compact('data'));
    }

    public function laporan_ketersediaan_buku()
    {
      $data['title'] = "Laporan Ketersediaan Buku - LMS";

      return view('admin.laporan.laporan_ketersediaan_buku', compact('data'));
    }

    public function laporan_sirkulasi()
    {
      $data['title'] = "Laporan Sirkulasi - LMS";

      return view('admin.laporan.laporan_sirkulasi', compact('data'));
    }

    public function laporan_lokasi_buku()
    {
      $data['title'] = "Laporan Lokasi Buku - LMS";

      return view('admin.laporan.laporan_lokasi_buku', compact('data'));
    }
}
