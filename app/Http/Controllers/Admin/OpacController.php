<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class OpacController extends Controller
{
    public function index()
    {
      $data['title'] = "OPAC - LMS";
      return view('admin.opac.opac', compact('data'));
    }
}
