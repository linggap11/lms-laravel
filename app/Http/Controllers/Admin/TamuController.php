<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class TamuController extends Controller
{
    public function index()
    {
      $data['title'] = "Data Tamu - LMS";
      return view('admin.tamu.daftar_tamu', compact('data'));
    }

    public function riwayat_tamu()
    {
      $data['title'] = "Riwayat Tamu - LMS";
      return view('admin.tamu.riwayat_tamu', compact('data'));
    }
}
