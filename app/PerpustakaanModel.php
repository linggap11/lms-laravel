<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PerpustakaanModel extends Model
{
    protected $table = 'tb_perpus_profile';
    public $timestamps = false;
}
