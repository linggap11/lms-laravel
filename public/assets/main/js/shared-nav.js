function formatLink(url) {
    if((url.charAt(0) === "/") && (document.location.href.indexOf("//lms.linggapr.my.id") >= 0))
    {
        url = "https://lms.linggapr.my.id" + url;
    }
    return url;
}
function sectionToHTML(label, root, links) {
    var html = '<div class="subnav-section">';
    html += '<a href="' + formatLink(root) + '" class="subnav-header">' + label + "</a>";
    for (var link in links) {
        html += '<a href="' + formatLink(links[link]) + '">' + link + "</a>";
    }
    return html + "</div>";
}
function buildSubnav(label, root, sections) {
    var html = '<li class="nav-link">';
    html += '<a href="' + formatLink(root) + '" class="nav-link-anchor">' + label + "</a>";
    html += '<div class="nav-submenu">';
    var length = 0;
    for (var section in sections) {
        length += 1;
    }
    var count = 0;
    html += '<div class="flex-col">';
    for (var section in sections) {
        if (count === Math.ceil(length / 2)) {
            html += '</div><div class="flex-col">';
        }
        html += sectionToHTML(section, sections[section].root, sections[section].header);
        count += 1;
    }
    return html + "</div></div>";
}

// Load header drop-downs
function loadSharedSubnav() {
    var json = JSON.parse(this.responseText);
    var html = '<ul class="nav-col">';
    var footerHTML = '<h2 class="hidden">Footer Links</h2>';
    var count = 0;
    for (var link in json) {
        // Build header
        if (count === 2) {
            html += '</ul><ul class="nav-col">';
        }
        html += buildSubnav(link, json[link].root, json[link].sections);
        // Build footer
        footerHTML += '<div class="footer-section">';
        footerHTML += '<h3><a href="' + formatLink(json[link].root) + '">' + link + "</a></h3>";
        for (var section in json[link].sections) {
            var sectionObj = json[link].sections[section];
            for (var sub in sectionObj.header) {
                sectionObj.footer[sub] = sectionObj.header[sub];
            }
            footerHTML += sectionToHTML(section, sectionObj.root, sectionObj.footer);
        }
        footerHTML += "</div>";
        count++;
    }
    var falveyNav = document.getElementById("falvey-nav");
    if (falveyNav) {
        falveyNav.innerHTML = html + "</ul>";
    }
    var footerLinks = document.getElementById("footer-links");
    if (footerLinks) {
        footerLinks.innerHTML = footerHTML;
    }
}
var http = new XMLHttpRequest();

// Triggered after navigation load to allow page to render before finding elements
function loadPlaceholders() {
    var selectEl = document.getElementById("searchForm_type");
    var search = document.getElementById("searchForm_lookfor");
    var srLabel = document.getElementById("sr-search-label");
    var placeholders = {
        "VuFind:Semua|": "judul buku, pengarang, penerbit, jenis buku",
        "VuFind:Solr|AllFields": "books, dvds, CDs, other media",
        "VuFind:Solr|Title": "judul buku pada perpustakaan",
        "VuFind:Solr|Jenis": "jenis buku pada perpustakaan",
        "VuFind:Solr|Author": "penulis buku pada perpustakaan",
        "VuFind:Solr|CallNumber": "call numbers of books, dvds, CDs, other media",
        "VuFind:Solr|ISN": "ISBN buku pada perpustakaan",
        "VuFind:Solr|tag": "community tags of books, dvds, CDs, other media",
        "VuFind:Summon|AllFields": "electronic articles, newspapers, and more",
        "VuFind:Summon|Title": "titles of electronic articles, newspapers, and more",
        "VuFind:Summon|Author": "authors of electronic articles, newspapers, and more",
        "VuFind:Summon|SubjectTerms": "subjects of electronic articles, newspapers, and more",
        "VuFind:WorldCat|srw.kw": "books and media from other libraries",
        "VuFind:WorldCat|srw.ti:srw.se": "titles of books and media from other libraries",
        "VuFind:WorldCat|srw.au": "authors of books and media from other libraries",
        "VuFind:WorldCat|srw.su": "subjects of books and media from other libraries",
        "VuFind:WorldCat|srw.dd:srw.lc": "call numbers of books and media from other libraries",
        "VuFind:WorldCat|srw.bn:srw.in": "ISBNs of books and media from other libraries",
        "VuFind:SolrWeb|AllFields": "all pages on the library website",
        "VuFind:SolrWeb|Guides": "course and subjects guides from our librarians",
        "External:http://digital.library.villanova.edu/Search/Results?lookfor=": "digitally archived books, artifacts, university papers"
    };
    function typePlaceholder(text, index) {
        if (typeof index === "undefined") {
            index = 0;
        }
        search.setAttribute("placeholder", text.substr(0, index));
        if (index < text.length) {
            requestAnimationFrame(function nextType() {
                typePlaceholder(text, index + 1);
            });
        }
    }
    function changePlaceholder() {
        if (typeof placeholders[selectEl.value] !== "undefined") {
            typePlaceholder(placeholders[selectEl.value]);
            if (srLabel) {
                srLabel.innerHTML = "Search for " + placeholders[selectEl.value];
            }
        } else {
            search.setAttribute("placeholder", "");
            if (srLabel) {
                srLabel.innerHTML = "Search the library";
            }
        }
    }
    selectEl.addEventListener("change", changePlaceholder, false);
    changePlaceholder();
}
