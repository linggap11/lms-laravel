// Use data-toggle to empower menu toggles and dropdowns
// Doesn't apply to nav menu hovers, see vu-common.css
(function activateToggles() {
    var toggles = document.querySelectorAll("[data-toggle]");
    for (var i = 0; i < toggles.length; i++) {
        const data = toggles[i].dataset.toggle.split(",");
        let selectors = [];
        for (var j = 0; j < data.length; j++) {
            selectors.push(toggles[i].dataset.toggle + ".toggle");
            selectors.push(toggles[i].dataset.toggle + ".toggle-sm");
        }
        let menus = document.querySelectorAll(selectors.join(","));
        toggles[i].addEventListener(
            "click",
            function toggleTarget(e) {
                //    e.preventDefault();
                for (var m = 0; m < menus.length; m++) {
                    menus[m].classList.toggle("open");
                }
            },
            false
        );
    }
})();

// Trim sidenav and content paragraphs
var asides = document.querySelectorAll("aside,.main-content p,.optional");
for (var i = 0; i < asides.length; i++) {
    let html = asides[i].innerHTML.trim();
    if (html.match(/^(&nbsp;)\1*$/)) {
        asides[i].innerHTML = "";
    } else {
        asides[i].innerHTML = html;
    }
}

// Close mobile sidenav for jump links
var sidebar = document.querySelector(".sidebar");
if (sidebar) {
    var sidebarLinks = sidebar.querySelectorAll("a");
    for (var i = 0; i < sidebarLinks.length; i++) {
        sidebarLinks[i].addEventListener(
            "click",
            function closeSidebar() {
                sidebar.classList.remove("open");
            },
            false
        );
    }
}
