<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
  <meta charset="utf-8" />
  <title>Library Management System - SESKOAU </title>

  <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta content="" name="description" />
  <meta content="" name="keywords" />
  <meta content="" name="author" />
  <link rel="shortcut icon" href="{{ asset('assets/universal/images/favicon.png') }}" />

  <!-- Global Plugins START -->
  <link href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('plugins/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('plugins/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('plugins/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('plugins/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet" />
  <!-- Global Plugins END -->

  <!-- Theme styles START -->
  <link href="{{ asset('assets/main/css/animate.css') }}" rel="stylesheet" />

  <!--<link href="webforms/css/components.css" rel="stylesheet">-->
  <link href="{{ asset('assets/main/css/slider.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/main/css/style.css') }}" rel="stylesheet" />
  <link href="{{ asset('assets/main/css/style-responsive.css') }}" rel="stylesheet" />
  <!-- Theme styles END -->

  <!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-114519222-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-114519222-1');
    </script>


</head>
