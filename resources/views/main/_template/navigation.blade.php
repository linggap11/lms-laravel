<div class="header-navigation pull-left font-transform-inherit">
  <ul>

     <li class="hm animation animated-menu-3"><!-- BEGIN MENU -->
      <a data-target="#" href="/">
        <i class="fa fa-home fa-2x grow" aria-hidden="true"></i>
      </a>
     </li><!-- END MENU -->
     @if (Session::has('id_anggota'))
       <li class="animation animated-menu-5"><!-- BEGIN MENU -->
        <a href="/dashboard">
          Dashboard
        </a>
       </li><!-- END MENU -->
     @endif
    <li class="dropdown dropdown-megamenu animation animated-menu-5"><!-- BEGIN MENU -->
      <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="javascript:;">
        Koleksi Digital
      </a>
      <ul class="dropdown-menu">
        <li>
          <div class="header-navigation-content">
            <div class="row">
              <div class="col-md-12 header-navigation-col">
                <h4>Koleksi </h4>
                <ul>
                  <li><a href="">Jurnal</a></li>
                  <li><a href="">Buku</a></li>
                </ul>
              </div>

            </div>
          </div>
        </li>
      </ul>
    </li><!-- END MENU -->

    <li class="dropdown dropdown-megamenu animation animated-menu-6"><!-- BEGIN MENU -->
      <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="javascript:;">
        Layanan
      </a>
      <ul class="dropdown-menu">
        <li>
          <div class="header-navigation-content">
            <div class="row">
              <div class="col-md-6 header-navigation-col">
                <h4>Katalog</h4>
                <ul>
                  <li><a href="" target="_blank">Bibliografi</a></li>
                  <li><a href="" target="_blank">Open Public Access Catalog (OPAC)</a></li>

                </ul>
              </div>
              <div class="col-md-6 header-navigation-col">
                <h4>Umum</h4>
                <ul>
                  <li><a href="" target="_blank">Keanggotaan</a></li>
                  <li><a href="" target="_blank">Usulan Buku</a></li>
                  <li><a href="" target="_blank">Berita</a></li>

                </ul>

              </div>

            </div>
          </div>
        </li>
      </ul>
    </li><!-- END MENU -->

    <li class="dropdown dropdown-megamenu animation animated-menu-5"><!-- BEGIN MENU -->
      <a class="dropdown-toggle" data-toggle="dropdown" data-target="#" href="javascript:;">
        Profile
      </a>
      <ul class="dropdown-menu">
        <li>
          <div class="header-navigation-content">
            <div class="row">
              <div class="col-md-6 header-navigation-col">
                <h4>Profile</h4>
                <ul>
                  <li><a href="falsafah9ed2.html?lang=en">SESKO AU</a></li>
                  <li><a href="visi_misi9ed2.html?lang=en">Perpustakaan SESKO AU</a></li>
                  <li><a href="tugas_fungsi_wewenang9ed2.html?lang=en">Lokasi</a></li>
                </ul>
              </div>
              <div class="col-md-6 header-navigation-col">

                  <h4>Struktur Organisasi</h4>
                    <ul>
                      <li><a href="bentuk_skema_struktural9ed2.html?lang=en">Jajaran Unit Kerja</a></li>
                    </ul>

              </div>
            </div>
          </div>
        </li>
      </ul>
    </li><!-- END MENU -->
  </ul>
</div>
