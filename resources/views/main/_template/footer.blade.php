<!-- BEGIN FOOTER -->
    <div class="footer">

      <div class="container">
        <div class="row">
          <!-- BEGIN COPYRIGHT -->
          <div class="col-sm-6 padding-top-10">
            Copyright 2019 © Perpustakaan SESKOAU. All Rights Reserved. <br>
            <a href="sitemap9ed2.html?lang=en">Sitemap</a> | <a href="javascript:;">Privacy Policy</a> | <a href="javascript:;">Terms of Service</a><br />

            <!-- End of Statcounter Code -->
          </div>
          <!-- END COPYRIGHT -->
          
        </div>
      </div>
    </div>
    <!-- END FOOTER -->

    <!-- Load javascripts at bottom, this will reduce page load time -->
    <!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->
    <!--[if lt IE 9]>
    <script src="webforms/plugins/respond.min.js"></script>
    <![endif]-->
    <script src="{{ asset('plugins/jquery.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/jquery-migrate.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('corporate/scripts/back-to-top.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
    <script src="{{ asset('plugins/datatables.net-scroller/js/datatables.scroller.min.js') }}"></script>
    <!-- END CORE PLUGINS -->

    <!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
    <script src="{{ asset('plugins/fancybox/source/jquery.fancybox.pack.js') }}" type="text/javascript"></script><!-- pop up -->
    <script src="{{ asset('plugins/owl.carousel/owl.carousel.min.js') }}" type="text/javascript"></script><!-- slider for products -->

    <script src="{{ asset('corporate/scripts/layout.js') }}" type="text/javascript"></script>
    <script src="{{ asset('corporate/scripts/bs-carousel.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugins/wow.min.js') }}"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            Layout.init();
            Layout.initOWL();
            Layout.initTwitter();
            //Layout.initFixHeaderWithPreHeader(); /* Switch On Header Fixing (only if you have pre-header) */
            //Layout.initNavScrolling();
        });
        function readURL(input) {
          if (input.files && input.files[0]) {
              reader.readAsDataURL(input.files[0]);
            }
          }

        $("#imgInp").change(function() {
          readURL(this);
        });
        var reader = new FileReader();
        reader.onload = function(e) {
          $('#preview').attr('src', e.target.result);
        }
    </script>
    <!-- END PAGE LEVEL JAVASCRIPTS --><!-- END FOOTER -->
