@include('main/_template/header')
<!-- BODY BEGIN -->
<link href="{{ asset('assets/main/css/bootstrap.min_18.1101000.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/main/css/opac.css') }}" rel="stylesheet" />
<body>
<!-- PREHEADER BEGIN -->
<!-- BEGIN TOP BAR -->
    <div class="pre-header">
        <div class="container">
            <div class="row">
                <!-- BEGIN TOP BAR LEFT PART -->
                <div class="col-sm-6 pre-header-logo animation animated-menu-1">
                    <a  href=""><img src="{{ asset('assets/main/img/logo.png') }}" alt="LOGO"  width="50%" class="img-responsive "></a>
                </div>
                <!-- END TOP BAR LEFT PART -->
                <!-- BEGIN TOP BAR MENU -->
                <div class="col-sm-6 animation animated-menu-2" >
                	<div class="row pre-header-config" >
                		<p>&nbsp;</p>
                		<p>&nbsp;</p>

                    <p>
                      @if (session()->get('id_anggota') != '' || session()->get('id_anggota') != null)
                        <a href="/akun" target=""><i class="fa fa-user grow"></i><span class="hidden-sm ">{{ session()->get('nama_anggota') }}</span></a>&nbsp;&nbsp;|
                        <a href="/logout" target=""><i class="fa fa-sign-out grow"></i><span class="hidden-sm ">Logout</span></a>&nbsp;&nbsp;
                      @else
                        <a href="/register" target=""><i class="fa fa-user-plus grow"></i><span class="hidden-sm ">Registrasi</span></a>&nbsp;&nbsp;|
                        <a href="/login" target=""><i class="fa fa-user grow"></i><span class="hidden-sm ">Login</span></a>&nbsp;&nbsp;
                      @endif
                    </p>

                    </div>
                    <!-- -->
                    <div class="row">
                        <div class="col-lg-8 pull-right">

                        </div><!-- end col-xs-8 -->
                    </div><!-- end row -->
                    <!-- -->
                </div>
                <!-- END TOP BAR MENU -->
            </div>
        </div>
    </div>
    <!-- END TOP BAR --><!-- PREHEADER END -->
<!-- HEADER BEGIN -->
<!-- BEGIN HEADER -->
    <div class="header">
      <div class="container">
        <a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars"></i></a>
        <!-- BEGIN NAVIGATION -->
        @include('main/_template/navigation')
        <!-- END NAVIGATION -->

      </div>
    </div>
    <!-- Header END -->
<!-- HEADER END -->
    <!-- BEGIN SLIDER -->
    <div class="page-slider margin-bottom-40">
      <br><br>
      <div class="container">
        <div class="row">
          <div id="opac-main-search">
            <div class="span12">
              <div class="mastheadsearch librarypulldown">
                <form name="searchform" method="get" action="opac/search" id="searchform" class="form-inline">
                  {{ csrf_field() }}
                  <label for="masthead_search"> Search</label>
                    <select name="indeks" id="masthead_search">
                      <option value="all">Semua</option>
                      <option value="judul">Judul</option>
                      <option value="penulis">Penulias</option>
                      <option value="subyek">Subyek</option>
                      <option value="isbn">ISBN</option>
                    </select>
                    <input type="text" title="Type search term" class="transl1" id = "translControl1" name="keywords" /><span id="translControl"></span>
                    <div class="input-append">
                      <select name="lokasi" id="select_library">
                        <option value="seskoau">Perpustakaan SESKOAU</option>
                        <option value="a">Perpustakaan A</option>
                      </select>
                      <button type="submit" id="searchsubmit" class="btn btn-primary">Go</button>
                    </div>
                  </form>
                </div> <!-- / .mastheadsearch -->
                  <div class="row-fluid">
                    <div id="moresearches">
                      <ul>
                        <li><a id="advanced-tab" data-toggle="tab" href="#advanced" role="tab" aria-controls="home">Advanced search</a></li>

                      </ul>
                    </div> <!-- /#moresearches -->
                    <hr>
                    <div class="tab-content" id="myTabContent">
                      <div class="tab-pane fade" id="advanced" role="tabpanel" aria-labelledby="advanced-tab">
                        <div class="container" style="max-width: 100%; overflow-x: hidden;">
                          <form class="" action="" method="get">
                            {{ csrf_field() }}
                            <div class="col-md-5">
                              <div class="form-group row">
                                <label for="" class="col-sm-2 col-form-label">Judul</label>
                                <div class="col-sm-10">
                                <input type="text" name="judul" class="transl1" style="width:100%" value="" placeholder="Judul Buku">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="" class="col-sm-2 col-form-label">Subyek</label>
                                <div class="col-sm-10">
                                  <input type="text" name="subyek" class="transl1" style="width:100%" value="" placeholder="Subyek Buku">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="" class="col-sm-2 col-form-label">ISBN</label>
                                <div class="col-sm-10">
                                  <input type="text" name="isbn" class="transl1" style="width:100%" value="" placeholder="ISBN Buku">
                                </div>
                              </div>
                            </div>
                            <div class="col-md-5">
                              <div class="form-group row">
                                <label for="" class="col-sm-2 col-form-label">Pengarang</label>
                                <div class="col-sm-10">
                                  <input type="text" class="transl1" style="width:100%" name="pengarang" id="pengarang" placeholder="Pengarang Buku">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="" class="col-sm-2 col-form-label">Penerbit</label>
                                <div class="col-sm-10">
                                  <input type="text" name="penerbit" class="transl1" style="width:100%" value="" placeholder="Penerbit Buku">
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="" class="col-sm-2 col-form-label">Tahun</label>
                                <div class="col-sm-10">
                                  <select name="tahun_terbit" class="transl1">
                                    <option value="all">Semua</option>
                                    <option value="2018">2018</option>
                                </select>
                                </div>
                              </div>
                              <div class="form-group row">
                                <label for="" class="col-sm-2 col-form-label">Lokasi</label>
                                <div class="col-sm-10">
                                  <select name="lokasi" class="transl1">
                                    <option value="seskoau">Perpustakaan SESKOAU</option>
                                    <option value="a">Perpustakaan A</option>
                                </select>
                                </div>
                              </div>
                              <div class="form-group row pull-right">
                                <button type="submit" class="btn btn-primary">Search</button>
                              </div>
                            </div>
                          </form>

                        </div>
                      </div>
                    </div>

                  </div> <!-- /.row-fluid -->
                </div> <!-- /.span10 -->
            </div> <!-- /.opac-main-search -->
        </div>
      </div>
      <br>
    </div>
    <!-- END SLIDER -->

    <div class="main">
      <div class="container">
        <!-- BEGIN NEWS TICKER -->
        <div class="row margin-bottom-10" >
        	<div class="col-lg-12">
            	 <marquee><h4></h4></marquee>
            </div>
        </div>
        <!-- END NEWS TICKER -->
        <!-- BEGIN SERVICE BOX -->
        <div class="row service-box margin-bottom-40 wow " >
          <div class="col-sm-12 pull-left animation animated-menu-3">
            <hr>
        	<h3>Layanan Perpustakaan  <b>SESKOAU</b>            </h3><br><br>
          </div>
          <div class="col-sm-3 col-xs-4 txt-center border-right animation animated-item-2">
            <a href="" target="_blank"><img src="{{ asset('assets/main/img/search.png') }}" alt="OPAC" width="72px" class="grow rotate"></a><br><br>
            <p><a href="" target="_blank"><b>OPAC</b><br>Online Public Access Catalog<br /><br /></a></p>
          </div>
          <div class="col-sm-3 col-xs-4 txt-center border-right animation animated-item-4">
            <a href="" target="_blank"><img src="{{ asset('assets/main/img/student.png') }}" alt="Anggota" width="72px" class="grow"></a><br><br>
            <p><a href="" target="_blank"><b>Anggota</b><br>
            Anggota Yang Terdaftar            </a></p>
          </div>
          <div class="col-sm-3 col-xs-4 txt-center border-right animation animated-item-4">
            <a href="" target="_blank"><img src="{{ asset('assets/main/img/library.png') }}" alt="Koleksi" width="72px" class="grow"></a><br><br>
            <p><a href="" target="_blank"><b>Koleksi</b><br>
            Koleksi Buku Perpustakaan SESKOAU</a></p>
          </div>
          <div class="col-sm-3 col-xs-4 txt-center border-right animation animated-item-4">
            <a href="" target="_blank"><img src="{{ asset('assets/main/img/text-lines.png') }}" alt="Koleksi" width="72px" class="grow"></a><br><br>
            <p><a href="" target="_blank"><b>Berita</b><br>
            Berita Terkini SESKOAU</a></p>
          </div>
        </div>
        <!-- END SERVICE BOX -->
	     </div>
     </div>
	 <div class="main">
      <div class="container">
        <!-- BEGIN NEWS -->

        <!-- END NEWS -->


      </div>
    </div>


<!-- BEGIN FOOTER -->
<!-- BEGIN PRE-FOOTER -->
    <div class="pre-footer">

      <div class="pre-grey"><!-- BEGIN GREY -->
      	<div class="container">
        	<div class="row margin-bottom-40" style="font-size:13px">
                <div class="col-sm-2 txt-left">
                    <h4><b>KOLEKSI </b></h4>
                    <p>
                    <br />
                    <a href="" target="_blank" class="txt-white">Buku</a><br>
                    <a href="" target="_blank">Jurnal</a>
                    </p>
                </div>
                 <div class="col-sm-2 txt-left ">
                    <h4><b>KATALOG</b></h4>
                    <br>
                    <p >
                    <a href="" target="_blank" class="txt-white">Judul Buku</a><br>
                    <a href="" target="_blank" class="txt-white">Abjad A - Z</a><br>
                    <a href="" target="_blank" class="txt-white">Pengarang Buku</a><br>
                    <a href="" target="_blank" class="txt-white">Penerbit Buku</a><br>
                    <a href="" target="_blank" class="txt-white">Subyek Buku</a><br>
                    </p>
                </div>
                <div class="col-sm-2 txt-left ">
                    <h4><b>UMUM</b></h4>
                    <p>
                    <br />
                    <a href="" target="_blank">Keanggotaan</a><br>
                    <a href="" target="_blank">Berita</a><br>
                    <a href="" target="_blank">OPAC</a><br>
                    </p>
                </div>
                <div class="col-sm-3 txt-left  ">
                    <h4><b>KONTAK KAMI</b></h4>
                    <p>
                     <br />
                    Lembang, Kabupaten Bandung Barat, Jawa Barat 40391 <br>
                    +62 896-3215-3053 <br>
                    Website : <a href="http://seskoau.mil.id/">seskoau.mil.id</a><br><br>
					</p>
                </div>
                <div class="col-sm-3 txt-left  ">
                	<h4><b>JAM OPERASIONAL</b></h4>
                    <br>
                    <p>
                    Senin - Sabtu 08.00 - 18.00<br />
                    Minggu 10.00 - 16.00<br />

                    </p>
                </div>
             </div>
        </div>
      </div> <!-- END PRE-GREY -->
    </div><!-- END PRE-FOOTER -->
    @include('main/_template/footer')
</body>
<!-- END BODY -->
</html>
