@include('main/_template/header')
<!-- BODY BEGIN -->
<body>
<!-- PREHEADER BEGIN -->
<!-- BEGIN TOP BAR -->
    <div class="pre-header">
        <div class="container">
            <div class="row">
                <!-- BEGIN TOP BAR LEFT PART -->
                <div class="col-sm-6 pre-header-logo animation animated-menu-1">
                    <a  href=""><img src="{{ asset('assets/main/img/logo.png') }}" alt="LOGO"  width="50%" class="img-responsive "></a>
                </div>
                <!-- END TOP BAR LEFT PART -->
                <!-- BEGIN TOP BAR MENU -->
                <div class="col-sm-6 animation animated-menu-2" >
                	<div class="row pre-header-config" >
                		<p>&nbsp;</p>
                		<p>&nbsp;</p>

                    <p>
                      @if (session()->get('id_anggota') != '' || session()->get('id_anggota') != null)
                        <a href="/akun" target=""><i class="fa fa-user grow"></i><span class="hidden-sm ">{{ session()->get('nama_anggota') }}</span></a>&nbsp;&nbsp;|
                        <a href="/logout" target=""><i class="fa fa-sign-out grow"></i><span class="hidden-sm ">Logout</span></a>&nbsp;&nbsp;
                      @else
                        <a href="/register" target=""><i class="fa fa-user-plus grow"></i><span class="hidden-sm ">Registrasi</span></a>&nbsp;&nbsp;|
                        <a href="/login" target=""><i class="fa fa-user grow"></i><span class="hidden-sm ">Login</span></a>&nbsp;&nbsp;
                      @endif
                    </p>

                    </div>
                    <!-- -->
                    <div class="row">
                        <div class="col-lg-8 pull-right">

                        </div><!-- end col-xs-8 -->
                    </div><!-- end row -->
                    <!-- -->
                </div>
                <!-- END TOP BAR MENU -->
            </div>
        </div>
    </div>
    <!-- END TOP BAR --><!-- PREHEADER END -->
<!-- HEADER BEGIN -->
<!-- BEGIN HEADER -->
    <div class="header">
      <div class="container">
        <a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars"></i></a>
        <!-- BEGIN NAVIGATION -->
        @include('main/_template/navigation')
        <!-- END NAVIGATION -->

      </div>
    </div>
    <!-- Header END -->
<!-- HEADER END -->
    <!-- BEGIN SLIDER -->
    <div class="page-slider margin-bottom-40">
      <br><br>
      <div class="container ">
        <div class="row">
          <h3>Profile</h3>
          <hr>
          @if (session()->has('message_update'))
            <div class="flash-message alert alert-success">
              <strong>{{ session()->get('message_update') }}</strong>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
            </div>
          @endif
          <form method="post" action="akun/update/{{ $data['profile']->id_anggota }}" class="form-login">
              {{ csrf_field() }}
              {{ method_field('PUT') }}
              <div class="col-sm-4">
                <div class="text-center">
                  @if (session()->get('foto') == null || session()->get('foto') == '')
                    <img id="preview" width="220px" src="{{ asset('uploads/anggota/default.png') }}" class="avatar img-circle" alt="avatar">
                    <h6>Upload a different photo...</h6>

                    <input type="file" name="foto" id="imgInp" class="form-control" value="">
                  @else
                    <img id="preview"  width="150px" height="150px" src="{{ asset('uploads/anggota/'.session()->get('foto').'') }}" class="avatar img-circle" alt="avatar">
                    <h6>Upload a different photo...</h6>

                    <input type="file" name="foto" id="imgInp" class="form-control" value="{{ session()->get('foto') }}">
                  @endif
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <label class="control-label" for="">NIM:</label>
                  <input type="text" name="nim" id="nim" value="{{ $data['profile']->nim }}" class="form-control">
                </div>
                <div class="form-group">
                  <label class="control-label" for="">Nama Lengkap:</label>
                  <input type="text" name="nama_anggota" id="nama_anggota" value="{{ $data['profile']->nama_anggota }}" class="form-control">
                </div>
                <div class="form-group">
                  <label class="control-label" for="">Alamat:</label>
                  <textarea name="alamat" rows="5" cols="80" class="form-control">{{ $data['profile']->alamat }}</textarea>
                </div>
                <div class="form-group">
                  <label class="control-label" for="">Email:</label>
                  <input type="text" name="email" id="email" class="form-control" value="{{ $data['profile']->email }}">
                </div>
                <div class="form-group">
                  <label class="control-label" for="">Telp:</label>
                  <input type="text" name="telp" id="telp"  class="form-control" value="{{ $data['profile']->no_hp }}">
                </div>
                <div class="form-group">
                  <label class="control-label" for="">Pangkat:</label>
                  <input type="text" name="semester" id="semester"  class="form-control" value="{{ $data['profile']->pangkat }}">
                </div>
                <br><br>
                <div class="form-group pull-right">
                  <button type="submit" class="btn btn-warning" style="color: #fff;background-color: #de9301; border-color: #de9301;"><i class="fa fa-save"></i> Simpan</button>
                </div>
              </div>

          </form>
        </div>
      </div>
      <br>
    </div>
    <!-- END SLIDER -->

    <div class="main">
      <div class="container">
        <!-- BEGIN NEWS TICKER -->
        <div class="row margin-bottom-10" >
        	<div class="col-lg-12">
            	 <marquee><h4></h4></marquee>
            </div>
        </div>
        <!-- END NEWS TICKER -->
        <!-- BEGIN SERVICE BOX -->
        <div class="row service-box margin-bottom-40 wow " >
          <div class="col-sm-12 pull-left animation animated-menu-3">
            <hr>
        	<h3>
			Layanan Perpustakaan  <b>SESKOAU</b>            </h3><br><br>
          </div>
          <div class="col-sm-3 col-xs-4 txt-center border-right animation animated-item-2">
            <a href="" target="_blank"><img src="{{ asset('assets/main/img/search.png') }}" alt="OPAC" width="72px" class="grow rotate"></a><br><br>
            <p><a href="" target="_blank"><b>OPAC</b><br>Online Public Access Catalog<br /><br /></a></p>
          </div>
          <div class="col-sm-3 col-xs-4 txt-center border-right animation animated-item-4">
            <a href="" target="_blank"><img src="{{ asset('assets/main/img/student.png') }}" alt="Anggota" width="72px" class="grow"></a><br><br>
            <p><a href="" target="_blank"><b>Anggota</b><br>
            Anggota Yang Terdaftar            </a></p>
          </div>
          <div class="col-sm-3 col-xs-4 txt-center border-right animation animated-item-4">
            <a href="" target="_blank"><img src="{{ asset('assets/main/img/library.png') }}" alt="Koleksi" width="72px" class="grow"></a><br><br>
            <p><a href="" target="_blank"><b>Koleksi</b><br>
            Koleksi Buku Perpustakaan SESKOAU</a></p>
          </div>
          <div class="col-sm-3 col-xs-4 txt-center border-right animation animated-item-4">
            <a href="" target="_blank"><img src="{{ asset('assets/main/img/text-lines.png') }}" alt="Koleksi" width="72px" class="grow"></a><br><br>
            <p><a href="" target="_blank"><b>Berita</b><br>
            Berita Terkini SESKOAU</a></p>
          </div>
        </div>
        <!-- END SERVICE BOX -->
	   </div>
     </div>


    <!-- BEGIN BLOCKQUOTE BLOCK -->
    <div class="row row-centered margin-bottom-30 agenda">
    </div>
    <!-- END BLOCKQUOTE BLOCK -->

	 <div class="main">
      <div class="container">
        <!-- BEGIN NEWS -->
        <div class="row margin-bottom-40">
          <div class="col-sm-12 pull-left animation animated-menu-5">
        	<h3>
            <b>Berita</b>  Terbaru           </h3><br><br>
          </div>
          @foreach ($data['data_berita'] as $berita)
            <div class="col-sm-4 padding-20">
         	    <div class="img-circle" style="min-height:250px">
                @if ($berita->cover == null || $berita->cover == '')
                  <a href=""> <img src="{{ asset('uploads/news/news.png') }}" class="img-rounded img-responsive center"></a>
                @else
                  <a href=""> <img src="{{ asset('uploads/news/'.$berita->cover.'') }}" class="img-rounded img-responsive"></a>
                @endif

                <br>
              </div>
              <p class="txt-13">
              <b>Berita - </b>{{ $berita->waktu }}</p>
          	  <a href=""><p class="txt-blue txt-left"><b>{{ $berita->judul }}</b></p></a>

            	<div class="txt-justify">
				      <p>{{ maks_length($berita->body) }}</div>
           </div>
          @endforeach
          <div class="col-sm-12 padding-top-10 " >
              <a href=""><p class="txt-blue txt-right"><b>Lainnya...</b></p></a>
           </div>
        </div>
        <!-- END NEWS -->


      </div>
    </div>


<!-- BEGIN FOOTER -->
<!-- BEGIN PRE-FOOTER -->
    <div class="pre-footer">

      <div class="container">
        <div class="row">
        	<div class="col-lg-12 pull-left animation animated-menu-7">
                <h3><b>Pengumuman</b></h3><br><br>
            </div>
            <h4><b>{{ $data['pengumuman']->header }}</b></h4><br>
            <p>{{ $data['pengumuman']->body }}</p>
        </div>
        <div class="row margin-bottom-40">

        </div>
      </div>

      <div class="pre-grey"><!-- BEGIN GREY -->
      	<div class="container">
        	<div class="row margin-bottom-40" style="font-size:13px">
                <div class="col-sm-2 txt-left">
                    <h4><b>KOLEKSI </b></h4>
                    <p>
                    <br />
                    <a href="" target="_blank" class="txt-white">Buku</a><br>
                    <a href="" target="_blank">Jurnal</a>
                    </p>
                </div>
                 <div class="col-sm-2 txt-left ">
                    <h4><b>KATALOG</b></h4>
                    <br>
                    <p >
                    <a href="" target="_blank" class="txt-white">Judul Buku</a><br>
                    <a href="" target="_blank" class="txt-white">Abjad A - Z</a><br>
                    <a href="" target="_blank" class="txt-white">Pengarang Buku</a><br>
                    <a href="" target="_blank" class="txt-white">Penerbit Buku</a><br>
                    <a href="" target="_blank" class="txt-white">Subyek Buku</a><br>
                    </p>
                </div>
                <div class="col-sm-2 txt-left ">
                    <h4><b>UMUM</b></h4>
                    <p>
                    <br />
                    <a href="" target="_blank">Keanggotaan</a><br>
                    <a href="" target="_blank">Berita</a><br>
                    <a href="" target="_blank">OPAC</a><br>
                    </p>
                </div>
                <div class="col-sm-3 txt-left  ">
                    <h4><b>KONTAK KAMI</b></h4>
                    <p>
                     <br />
                    Lembang, Kabupaten Bandung Barat, Jawa Barat 40391 <br>
                    +62 896-3215-3053 <br>
                    Website : <a href="http://seskoau.mil.id/">seskoau.mil.id</a><br><br>
					</p>
                </div>
                <div class="col-sm-3 txt-left  ">
                	<h4><b>JAM OPERASIONAL</b></h4>
                    <br>
                    <p>
                    Senin - Sabtu 08.00 - 18.00<br />
                    Minggu 10.00 - 16.00<br />

                    </p>
                </div>
             </div>
        </div>
      </div> <!-- END PRE-GREY -->
    </div><!-- END PRE-FOOTER -->
    @include('main/_template/footer')
</body>
<!-- END BODY -->
</html>
