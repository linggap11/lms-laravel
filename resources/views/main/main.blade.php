@include('main/_template/header')
<!-- BODY BEGIN -->
<body>
<!-- PREHEADER BEGIN -->
<!-- BEGIN TOP BAR -->
    <div class="pre-header">
        <div class="container">
            <div class="row">
                <!-- BEGIN TOP BAR LEFT PART -->
                <div class="col-sm-6 pre-header-logo animation animated-menu-1">
                    <a  href=""><img src="{{ asset('assets/main/img/logo.png') }}" alt="LOGO"  width="50%" class="img-responsive "></a>
                </div>
                <!-- END TOP BAR LEFT PART -->
                <!-- BEGIN TOP BAR MENU -->
                <div class="col-sm-6 animation animated-menu-2" >
                	<div class="row pre-header-config" >
                		<p>&nbsp;</p>
                		<p>&nbsp;</p>

                    <p>
                      @if (session()->get('id_anggota') != '' || session()->get('id_anggota') != null)
                        <a href="/akun" target=""><i class="fa fa-user grow"></i><span class="hidden-sm ">{{ session()->get('nama_anggota') }}</span></a>&nbsp;&nbsp;|
                        <a href="/logout" target=""><i class="fa fa-sign-out grow"></i><span class="hidden-sm ">Logout</span></a>&nbsp;&nbsp;
                      @else
                        <a href="/register" target=""><i class="fa fa-user-plus grow"></i><span class="hidden-sm ">Registrasi</span></a>&nbsp;&nbsp;|
                        <a href="/login" target=""><i class="fa fa-user grow"></i><span class="hidden-sm ">Login</span></a>&nbsp;&nbsp;
                      @endif
                    </p>

                    </div>
                    <!-- -->
                    <div class="row">
                        <div class="col-lg-8 pull-right">

                        </div><!-- end col-xs-8 -->
                    </div><!-- end row -->
                    <!-- -->
                </div>
                <!-- END TOP BAR MENU -->
            </div>
        </div>
    </div>
    <!-- END TOP BAR --><!-- PREHEADER END -->
<!-- HEADER BEGIN -->
<!-- BEGIN HEADER -->
    <div class="header">
      <div class="container">
        <a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars"></i></a>
        <!-- BEGIN NAVIGATION -->
        @include('main/_template/navigation')
        <!-- END NAVIGATION -->

      </div>
    </div>
    <!-- Header END -->
<!-- HEADER END -->
    <!-- BEGIN SLIDER -->
    <div class="page-slider margin-bottom-40">
        <div id="carousel-example-generic" class="carousel slide carousel-slider">
            <!-- Indicators -->
            <ol class="carousel-indicators carousel-indicators-frontend">
              <li data-target='#carousel-example-generic' data-slide-to='0' class='active'></li>
              <li data-target='#carousel-example-generic' data-slide-to='1'></li>
              <li data-target='#carousel-example-generic' data-slide-to='2'></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox" style="height: 500px">
              <div class='item active' style='background-image: url({{ asset("uploads/slider/". $data["slider"]->pict1 ."") }})'></div>
              <div class='item' style='background-image: url({{ asset("uploads/slider/". $data["slider"]->pict2 ."") }})'></div>
              <div class='item' style='background-image: url({{ asset("uploads/slider/". $data["slider"]->pict3 ."") }})'></div>
            </div>

            <!-- Controls -->
            <a class="left carousel-control carousel-control-shop carousel-control-frontend" href="#carousel-example-generic" role="button" data-slide="prev">
                <i class="fa fa-angle-left" aria-hidden="true"></i>
            </a>
            <a class="right carousel-control carousel-control-shop carousel-control-frontend" href="#carousel-example-generic" role="button" data-slide="next">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
            </a>
        </div>
    </div>
    <!-- END SLIDER -->

    <div class="main">
      <div class="container">
        <!-- BEGIN NEWS TICKER -->
        <div class="row margin-bottom-10" >
        	<div class="col-lg-12">
            	 <marquee><h4></h4></marquee>
            </div>
        </div>
        <!-- END NEWS TICKER -->
        <!-- BEGIN SERVICE BOX -->
        <div class="row service-box margin-bottom-40 wow " >
          <div class="col-sm-12 pull-left animation animated-menu-3">
        	<h3>
			     Layanan Perpustakaan  <b>SESKOAU</b>            </h3><br><br>
          </div>
          <div class="col-sm-3 col-xs-4 txt-center border-right animation animated-item-2">
            <a href="/opac" target=""><img src="{{ asset('assets/main/img/search.png') }}" alt="OPAC" width="72px" class="grow rotate"></a><br><br>
            <p><a href="/opac" target=""><b>OPAC</b><br>Online Public Access Catalog<br /><br /></a></p>
          </div>
          <div class="col-sm-3 col-xs-4 txt-center border-right animation animated-item-4">
            <a href="" target=""><img src="{{ asset('assets/main/img/student.png') }}" alt="Anggota" width="72px" class="grow"></a><br><br>
            <p><a href="" target=""><b>Anggota</b><br>
            Anggota Yang Terdaftar            </a></p>
          </div>
          <div class="col-sm-3 col-xs-4 txt-center border-right animation animated-item-4">
            <a href="" target=""><img src="{{ asset('assets/main/img/library.png') }}" alt="Koleksi" width="72px" class="grow"></a><br><br>
            <p><a href="" target=""><b>Koleksi</b><br>
            Koleksi Buku Perpustakaan SESKOAU</a></p>
          </div>
          <div class="col-sm-3 col-xs-4 txt-center border-right animation animated-item-4">
            <a href="" target=""><img src="{{ asset('assets/main/img/text-lines.png') }}" alt="Koleksi" width="72px" class="grow"></a><br><br>
            <p><a href="" target=""><b>Berita</b><br>
            Berita Terkini SESKOAU</a></p>
          </div>
        </div>
        <!-- END SERVICE BOX -->
	   </div>
     </div>


    <!-- BEGIN BLOCKQUOTE BLOCK -->
    <div class="row row-centered margin-bottom-30 agenda">
    </div>
    <!-- END BLOCKQUOTE BLOCK -->

	 <div class="main">
      <div class="container">
        <!-- BEGIN NEWS -->
        <div class="row margin-bottom-40">
          <div class="col-sm-12 pull-left animation animated-menu-5">
        	<h3>
            <b>Berita</b>  Terbaru           </h3><br><br>
          </div>
          @foreach ($data['data_berita'] as $berita)
            <div class="col-sm-4 padding-20">
         	    <div class="img-circle" style="min-height:250px">
                @if ($berita->cover == null || $berita->cover == '')
                  <a href=""> <img src="{{ asset('uploads/news/news.png') }}" class="img-rounded img-responsive center"></a>
                @else
                  <a href=""> <img src="{{ asset('uploads/news/'.$berita->cover.'') }}" class="img-rounded img-responsive"></a>
                @endif

                <br>
              </div>
              <p class="txt-13">
              <b>Berita - </b>{{ $berita->waktu }}</p>
          	  <a href=""><p class="txt-blue txt-left"><b>{{ $berita->judul }}</b></p></a>

            	<div class="txt-justify">
				      <p>{{ maks_length($berita->body) }}</div>
           </div>
          @endforeach
          <div class="col-sm-12 padding-top-10 " >
              <a href=""><p class="txt-blue txt-right"><b>Lainnya...</b></p></a>
           </div>
        </div>
        <!-- END NEWS -->


      </div>
    </div>


<!-- BEGIN FOOTER -->
<!-- BEGIN PRE-FOOTER -->
    <div class="pre-footer">

      <div class="container">
        <div class="row">
        	<div class="col-lg-12 pull-left animation animated-menu-7">
                <h3><b>Pengumuman</b></h3><br><br>
            </div>
            <h4><b>{{ $data['pengumuman']->header }}</b></h4><br>
            <p>{{ $data['pengumuman']->body }}</p>
        </div>
        <div class="row margin-bottom-40">

        </div>
      </div>

      <div class="pre-grey"><!-- BEGIN GREY -->
      	<div class="container">
        	<div class="row margin-bottom-40" style="font-size:13px">
                <div class="col-sm-2 txt-left">
                    <h4><b>KOLEKSI </b></h4>
                    <p>
                    <br />
                    <a href="" target="_blank" class="txt-white">Buku</a><br>
                    <a href="" target="_blank">Jurnal</a>
                    </p>
                </div>
                 <div class="col-sm-2 txt-left ">
                    <h4><b>KATALOG</b></h4>
                    <br>
                    <p >
                    <a href="" target="_blank" class="txt-white">Judul Buku</a><br>
                    <a href="" target="_blank" class="txt-white">Abjad A - Z</a><br>
                    <a href="" target="_blank" class="txt-white">Pengarang Buku</a><br>
                    <a href="" target="_blank" class="txt-white">Penerbit Buku</a><br>
                    <a href="" target="_blank" class="txt-white">Subyek Buku</a><br>
                    </p>
                </div>
                <div class="col-sm-2 txt-left ">
                    <h4><b>UMUM</b></h4>
                    <p>
                    <br />
                    <a href="" target="_blank">Keanggotaan</a><br>
                    <a href="" target="_blank">Berita</a><br>
                    <a href="" target="_blank">OPAC</a><br>
                    </p>
                </div>
                <div class="col-sm-3 txt-left  ">
                    <h4><b>KONTAK KAMI</b></h4>
                    <p>
                     <br />
                    Lembang, Kabupaten Bandung Barat, Jawa Barat 40391 <br>
                    +62 896-3215-3053 <br>
                    Website : <a href="http://seskoau.mil.id/">seskoau.mil.id</a><br><br>
					</p>
                </div>
                <div class="col-sm-3 txt-left  ">
                	<h4><b>JAM OPERASIONAL</b></h4>
                    <br>
                    <p>
                    Senin - Sabtu 08.00 - 18.00<br />
                    Minggu 10.00 - 16.00<br />

                    </p>
                </div>
             </div>
        </div>
      </div> <!-- END PRE-GREY -->
    </div><!-- END PRE-FOOTER -->
    @include('main/_template/footer')
</body>
<!-- END BODY -->
</html>
