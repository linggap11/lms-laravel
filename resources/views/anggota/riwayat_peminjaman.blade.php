@include('anggota/_template/css')
<link rel="stylesheet" href="{{ asset('assets/anggota/assets/DT_bootstrap.css') }}">

<body>
    @include('anggota/_template/header')
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span3" id="sidebar">
                <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                    <li>
                        <a href="{{ url('/dashboard') }}"><i class="icon-chevron-right"></i> Dashboard</a>
                    </li>
                    <li class="active">
                        <a href="{{ url('/anggota/riwayat_peminjaman') }}"><i class="icon-chevron-right"></i> Riwayat Peminjaman</a>
                    </li>
                    <li>
                        <a href="{{ url('/anggota/usulan_buku') }}"><i class="icon-chevron-right"></i> Usulan Buku</a>
                    </li>
                    <li>
                        <a href="{{ url('/anggota/profile/'.Session::get('nim').'') }}"><i class="icon-chevron-right"></i> Profile</a>
                    </li>
                    <li>
                        <a href="{{ url('/anggota/feedback') }}"><i class="icon-chevron-right"></i> Feedback</a>
                    </li>
                </ul>
            </div>

            <!--/span-->
            <div class="span9" id="content">
                <div class="row-fluid">
                    	<div class="navbar">
                        	<div class="navbar-inner">
                              <ul class="breadcrumb">
                                  <i class="icon-chevron-left hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
                                  <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
                                  <li>
                                      <a href="{{ url('/dashboard') }}">Dashboard</a> <span class="divider">/</span>
                                  </li>
                                  <li>
                                      Riwayat Peminjaman
                                  </li>
                              </ul>
                        	</div>
                    	</div>
                	</div>
                  <div class="row-fluid">
                    <!-- block -->
                    <div class="block">
                        <div class="navbar navbar-inner block-header">
                            <div class="muted pull-left">Riwayat Peminjaman</div>
                        </div>
                        <div class="block-content collapse in">
                            <div class="span12">
                              
                                <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example2">
                                    <thead>
                                        <tr>
                                            <th width="5%">No</th>
                                            <th>Judul</th>
                                            <th width="25%">Pengarang</th>
                                            <th width="17%">Tgl Peminjaman</th>
                                            <th width="20%">Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                          $no =1;
                                        @endphp
                                        @foreach ($data['data_peminjaman'] as $peminjaman)
                                          <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ $peminjaman->judul }}</td>
                                            <td>{{ $peminjaman->pengarang }}</td>
                                            <td>{{ $peminjaman->tgl_pinjam }}</td>
                                            @if ($peminjaman->status == "PINJAM")
                                              <td>Sedang Di Pinjam</td>
                                            @else
                                              <td>Sudah Dikembalikan</td>
                                            @endif
                                          </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- /block -->
                </div>
            </div>
        </div>
        <hr>
        <footer>

        </footer>
    </div>
    <!--/.fluid-container-->
    <script src="{{ asset('assets/anggota/vendors/jquery-1.9.1.min.js') }}"></script>
    <script src="{{ asset('assets/anggota/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/anggota/assets/scripts.js') }}"></script>
    <script src="{{ asset('assets/anggota/vendors/datatables/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/anggota/assets/DT_bootstrap.js') }}"></script>
</body>
