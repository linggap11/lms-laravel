<!DOCTYPE html>
<html>

    <head>
        <title>Dashboard Anggota</title>
        <!-- Bootstrap -->
        <link href="{{ asset('assets/anggota/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" media="screen">
        <link href="{{ asset('assets/anggota/bootstrap/css/bootstrap-responsive.min.css') }}" rel="stylesheet" media="screen">
        <link href="{{ asset('assets/anggota/assets/styles.css') }}" rel="stylesheet" media="screen">
        <link href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="vendors/flot/excanvas.min.js"></script><![endif]-->
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="{{ asset('assets/anggota/vendors/modernizr-2.6.2-respond-1.1.0.min.js') }}"></script>
        <style media="screen">
          .atas {
              /* background: #87cefa;	 */
	            color: #b0b0b0;
          	  background: -moz-linear-gradient(45deg, #87cefa 0%, #87cefa 1%, #d8d8d8 100%); /* ff3.6+ */
          	  background: -webkit-gradient(linear, left bottom, right top, color-stop(0%, #87cefa), color-stop(1%, #87cefa), color-stop(100%, #d8d8d8)); /* safari4+,chrome */
              background: -webkit-linear-gradient(45deg, #87cefa 0%, #87cefa 1%, #d8d8d8 100%); /* safari5.1+,chrome10+ */
              background: -o-linear-gradient(45deg, #87cefa 0%, #87cefa 1%, #d8d8d8 100%); /* opera 11.10+ */
              background: -ms-linear-gradient(45deg, #87cefa 0%, #87cefa 1%, #d8d8d8 100%); /* ie10+ */
              background: linear-gradient(45deg, #87cefa 0%, #87cefa 1%, #d8d8d8 100%); /* w3c */
          }
        </style>
    </head>

    <body>
      <div class="navbar navbar-fixed-top">
        <div class="navbar-inner atas">
            <div class="container-fluid">
                <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                 <span class="icon-bar"></span>
                 <span class="icon-bar"></span>
                </a>
                <a class="brand" href="#"><i class="fa fa-home "></i> PERPUSTAKAAN SEKOAU</a>
                <div class="nav-collapse collapse">
                    <ul class="nav pull-right">
                        <li class="dropdown">
                            <a href="{{ url('/anggota/profile/'.Session::get('nim').'') }}" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> {{ Session::get('nama_anggota') }}<i class="caret"></i>

                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a tabindex="-1" href="{{ url('/anggota/profile/'.Session::get('nim').'') }}">Profile</a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a tabindex="-1" href="{{ url('/logout') }}">Logout</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!--/.nav-collapse -->
            </div>
        </div>
      </div>

        <div class="container-fluid">
            <div class="row-fluid">
              <div class="span3" id="sidebar">
                  <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                      <li>
                          <a href="{{ url('/dashboard') }}"><i class="icon-chevron-right"></i> Dashboard</a>
                      </li>
                      <li>
                          <a href="{{ url('/anggota/riwayat_peminjaman') }}"><i class="icon-chevron-right"></i> Riwayat Peminjaman</a>
                      </li>
                      <li >
                          <a href="{{ url('/anggota/usulan_buku') }}"><i class="icon-chevron-right"></i> Usulan Buku</a>
                      </li>
                      <li class="active">
                          <a href="{{ url('/anggota/profile/'.Session::get('nim').'') }}"><i class="icon-chevron-right"></i> Profile</a>
                      </li>
                      <li>
                          <a href="{{ url('/anggota/feedback') }}"><i class="icon-chevron-right"></i> Feedback</a>
                      </li>
                  </ul>
              </div>
                <!--/span-->
                <div class="span9" id="content">
                      <!-- morris stacked chart -->
                      <div class="row-fluid">
                          	<div class="navbar">
                              	<div class="navbar-inner">
                                    <ul class="breadcrumb">
                                        <i class="icon-chevron-left hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
                                        <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
                                        <li>
                                            <a href="{{ url('/dashboard') }}">Dashboard</a> <span class="divider">/</span>
                                        </li>
                                        <li>
                                            Profile
                                        </li>
                                    </ul>
                              	</div>
                          	</div>
                      	</div>
                    <div class="row-fluid">
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Profile Anggota</div>
                            </div>
                            <div class="block-content collapse in">
                                <div class="span12">
                                     <form class="form-horizontal" action="{{ url('akun/update/'.$data['profile']->id_anggota.'') }}" method="post" enctype="multipart/form-data">
                                       {{ csrf_field() }}
                                       {{ method_field('PUT') }}
                                      <fieldset>
                                        <legend>{{ $data['profile']->nama_anggota }}({{ $data['profile']->nim }})</legend>
                                        <div class="control-group">
                                          <label class="control-label" for="focusedInput">NIM</label>
                                          <div class="controls">
                                            <input class="input-xlarge focused" name="nim" id="focusedInput" type="text" value="{{ $data['profile']->nim }}" readonly>
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="focusedInput">Nama Lengkap</label>
                                          <div class="controls">
                                            <input class="input-xlarge focused" name="nama_anggota" id="focusedInput" value="{{ $data['profile']->nama_anggota }}" type="text" >
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="focusedInput">Alamat</label>
                                          <div class="controls">
                                            <textarea name="alamat" class="input-xlarge focused" rows="5" cols="50">{{ $data['profile']->alamat }}</textarea>
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="focusedInput">Email</label>
                                          <div class="controls">
                                            <input class="input-xlarge focused" name="email" id="focusedInput" type="text"  value="{{ $data['profile']->email }}" >
                                          </div>
                                        </div>
                                        <div class="control-group">
                                          <label class="control-label" for="focusedInput">Pangkat</label>
                                          <div class="controls">
                                            <input class="input-xlarge focused" id="focusedInput" name="pangkat" type="text" value="{{ $data['profile']->pangkat }}"  >
                                          </div>
                                        </div>
                                        <div class="form-actions">
                                          <button type="submit" class="btn btn-primary">Save changes</button>
                                          <button type="reset" class="btn">Cancel</button>
                                        </div>
                                      </fieldset>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <!-- /block -->
                    </div>






                </div>
            </div>
            <hr>
            <footer>

            </footer>
        </div>
        <script src="{{ asset('assets/anggota/vendors/jquery-1.9.1.js') }}"></script>
        <script src="{{ asset('assets/anggota/bootstrap/js/bootstrap.min.js') }}"></script>
      	<script src="{{ asset('assets/anggota/assets/scripts.js') }}"></script>
        <script>

    </body>

</html>
