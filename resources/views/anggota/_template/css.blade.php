<html class="no-js">
    <head>
        <title>Dashboard Anggota</title>
        <!-- Bootstrap -->
        <link href="{{ asset('assets/anggota/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" media="screen">
        <link href="{{ asset('assets/anggota/bootstrap/css/bootstrap-responsive.min.css') }}" rel="stylesheet" media="screen">
        <link href="{{ asset('assets/anggota/assets/styles.css') }}" rel="stylesheet" media="screen">
        <link href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" />
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="{{ asset('assets/anggota/vendors/modernizr-2.6.2-respond-1.1.0.min.js') }}"></script>
        <style media="screen">
          .atas {
              /* background: #87cefa;	 */
	            color: #b0b0b0;
          	  background: -moz-linear-gradient(45deg, #87cefa 0%, #87cefa 1%, #d8d8d8 100%); /* ff3.6+ */
          	  background: -webkit-gradient(linear, left bottom, right top, color-stop(0%, #87cefa), color-stop(1%, #87cefa), color-stop(100%, #d8d8d8)); /* safari4+,chrome */
              background: -webkit-linear-gradient(45deg, #87cefa 0%, #87cefa 1%, #d8d8d8 100%); /* safari5.1+,chrome10+ */
              background: -o-linear-gradient(45deg, #87cefa 0%, #87cefa 1%, #d8d8d8 100%); /* opera 11.10+ */
              background: -ms-linear-gradient(45deg, #87cefa 0%, #87cefa 1%, #d8d8d8 100%); /* ie10+ */
              background: linear-gradient(45deg, #87cefa 0%, #87cefa 1%, #d8d8d8 100%); /* w3c */
          }
        </style>
    </head>
