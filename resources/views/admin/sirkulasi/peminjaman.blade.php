@include('admin/_template/css')
<link rel="stylesheet" href="{{ asset('plugins/easy-autocomplete/easy-autocomplete.min.css') }}">
<link rel="stylesheet" href="{{ asset('plugins/easy-autocomplete/easy-autocomplete.themes.min.css') }}">
<meta name="csrf-token" content="{{ Session::token() }}">
<title>{{ $data['title'] }}</title>
<style type="text/css" media="screen">
  th {
    text-align: center; border-right: 2px solid #dddddd;
    border: bo
  }
  .easy-autocomplete{
    width:100% !important
  }

  .easy-autocomplete input{
    width: 100%;
  }

  .form-wrapper{
    width: 500px;
  }
</style>
@include('admin/_template/header')
<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">

                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Peminjaman</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->

    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Data Peminjaman</h5>
                        <span>Data Anggota Yang Meminjam Buku</span>
                    </div>

                    <div class="card-body">
                        @if (session()->has('message_tambah'))
                          <div class="alert alert-success alert-dismissible fade show" role="alert">
                              <strong>Peminjaman Berhasil Ditambahkan!</strong>
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                        @elseif (session()->has('message_perpanjangan'))
                          <div class="alert alert-success alert-dismissible fade show" role="alert">
                              <strong>Peminjaman Berhasil Diperpanjang!</strong>
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                        @endif
                        @if (Session::get('role') == 'SU' || Session::get('role') == 'SIRKULASI')
                          <button type="" class="btn btn-primary" data-toggle="modal" data-target="#tambah_dataModal" data-whatever="@mdo"><i class="icofont icofont-plus"></i>&nbsp; Data Peminjaman</button>
                          <br><br>
                        @endif
                       <div class="table-responsive">
                            <table id="table_peminjam" class="cell-border display">
                                <thead>
                                  <tr>
                                    <th width="5%">No</th>
                                    <th width="35%">Judul Buku</th>
                                    <th width="25%">Anggota</th>
                                    <th width="">Peminjaman</th>
                                    <th width="">Pengembalian</th>
                                    <th>Info</th>
                                  </tr>
                                </thead>
                                <tbody>
                                    @php
                                      $no = 1;
                                    @endphp
                                    @foreach ($data['data_peminjaman'] as $peminjaman)
                                      <tr>
                                        <td align="center">{{ $no++ }}</td>
                                        <td>{{ $peminjaman->judul }}</td>
                                        <td>{{ $peminjaman->nama_anggota }}</td>
                                        <td align="center">{{ $peminjaman->tgl_pinjam }}</td>
                                        <td align="center">{{ $peminjaman->tgl_kembali }}</td>
                                        <td align="center"><a href="#" onclick="return false" data-id="{{ $peminjaman->id_peminjaman }}" title="Detail Buku" class="btn btn-primary btn-xs detail-peminjaman"><i class="fa fa-info-circle"> </i></a></td>
                                      </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Container-fluid starts -->
    <div class="modal fade" id="tambah_dataModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog " role="document">
        <div class="modal-content">
          <form action="{{ url('sirkulasi/tambah_peminjaman') }}" method="post">
            {{ csrf_field() }}
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data Peminjaman</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="recipient-name" class="col-form-label" >Nomor Induk Anggota:</label>
                    <input type="text" class="form-control" name="nim" id="anggota" placeholder="Ketikan 2 Nomor Induk ..." required>
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group">
                    <label for="recipient-name" class="col-form-label" >ISBN:</label>
                    <input type="text" class="form-control" name="isbn" id="buku"  placeholder="Ketikan 2 huruf ISBN ..." required>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="recipient-name" class="col-form-label" >Tgl Pinjam:</label>
                    <input type="text" class="form-control datepicker" name="tgl_pinjam" placeholder="Tanggal Pinjam" style="text-align:center;" value="{{ date('Y-m-d') }}" readonly required>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="recipient-name" class="col-form-label" >Tgl Kembali:</label>
                    <input type="text" class="form-control datepicker" name="tgl_kembali"  placeholder="Tanggal Kembali" style="text-align:center;" readonly required>
                  </div>
                </div>
              </div>

                <div class="form-group">
                  <label for="message-text" class="col-form-label">Keterangan:</label>
                  <textarea class="form-control" name="keterangan" ></textarea>
                </div>


            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Tambah</button>
            </div>
            </form>
        </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modalDetail"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
   <div class="modal-content">
     <div class="modal-header">
         <h5 class="modal-title">Detail Peminjaman</h5>
         <button type="button" class="close" data-dismiss="modal" id="close">&times;</button>
     </div>
     <div class="modal-body">
       <div class="container">
         <div class="row">
           <div class="table-responsive">
             <table class="table">
               <thead class="table-primary table-border-vertical ">
                 <tr>
                   <th scope="col" width="20%">#</th>
                   <th scope="col">Data Peminjaman</th>
                 </tr>
               </thead>
               <tbody>
                 <tr>
                   <th scope="row" class="text-left">Anggota</th>
                   <td><span id="nama_anggota"></span></td>
                 </tr>
                 <tr>
                   <th scope="row" class="text-left">ISBN</th>
                   <td><span id="isbn"></span></td>
                 </tr>
                 <tr>
                   <th scope="row" class="text-left">Judul</th>
                   <td><span id="judul"></span></td>
                 </tr>
                 <tr>
                   <th scope="row" class="text-left">Peminjaman</th>
                   <td><span id="peminjaman"></span></td>
                 </tr>
                 <tr>
                   <th scope="row" class="text-left">Pengembalian</th>
                   <td><span id="pengembalian"></span></td>
                 </tr>
               </tbody>
             </table>
             <hr>
           </div>

           <div id="tampil_perpanjangan" style="display:none">
             <br>
             <div class="card card-absolute">
               <div class="card-header bg-primary">
                 <h5>Perpanjangan</h5>
               </div>
               <div class="card-body">
                 <form class="form-perpanjangan" action="" method="post">
                   {{ csrf_field() }}
                   {{ method_field('PUT') }}
                   <table>
                     <tr>
                       <th></th>
                       <th></th>
                     </tr>
                     <tr>
                       <td>
                         <div class="form-group">
                           <label for="recipient-name" class="col-form-label" > Tanggal Pengembalian</label>
                           <input type="text" class="form-control datepicker" name="tgl_kembali" placeholder="Tanggal Kembali" style="text-align:center;" value="{{ date('Y-m-d') }}" readonly required>
                         </div>
                       </td>
                       <td>
                         <div class="form-group">
                           <label for="recipient-name" class="col-form-label" >:</label>
                           {{-- <button type="submit" class="btn btn-primary">Simpan!</button> --}}
                           <input type="submit" class="form-control btn btn-primary"  value="Simpan">
                         </div>
                       </td>
                     </tr>
                   </table>

                 </form>
               </div>
             </div>
           </div>
         </div>
       </div>

     </div>
     <div class="modal-footer">
       @if (Session::get('role') == 'SU' || Session::get('role') == 'SIRKULASI')
         <button type="button" id="pengembalian-button" class="btn btn-warning">Pengembalian</button>
         <button type="button" id="perpanjangan-button" class="btn btn-success">Perpanjang ?</button>
       @else
         <button type="button" class="btn btn-warning disabled">Pengembalian</button>
         <button type="button" class="btn btn-success disabled">Perpanjang ?</button>
       @endif

     </div>
   </div>
 </div>
</div>
@include('admin/_template/js')
<script src="{{ asset('plugins/easy-autocomplete/jquery.easy-autocomplete.min.js') }}" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}">
<script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" charset="utf-8"></script>
@include('admin/_template/footer')
<script type="text/javascript">
    $('#table_peminjam').DataTable({
      "bLengthChange": false,
      "bInfo": false,
    });

    var data_anggota = {
      url: "{{ url('sirkulasi/get_anggota') }}",
      getValue: "nim",
      list: {
        match: {
          enabled: true
        }
      }
    };

    var data_buku = {
      url: "{{ url('sirkulasi/get_buku') }}",
      getValue: "isbn",
      list: {

        match: {
          enabled: true
        }
      }
    };

    $("#anggota").easyAutocomplete(data_anggota);
    $("#buku").easyAutocomplete(data_buku);

    $('.datepicker').datepicker({
      autoclose: true,
      format: "yyyy-mm-dd",
      todayHighlight: true,
      orientation: "top auto",
      todayBtn: true,
      todayHighlight: true,
    });

    var d = new Date();
    var month = d.getMonth()+1;
    var day = d.getDate();

    var currDate = d.getFullYear() + '/' +
        (month<10 ? '0' : '') + month + '/' +
        (day<10 ? '0' : '') + day;
    const current_date = new Date(currDate);
    console.log(currDate);

    $('.detail-peminjaman').click(function() {
      var id_peminjaman = $(this).data('id');
      var terlambat = 0;
      $.get("{{ url('sirkulasi/get_detail_peminjaman') }}/"+id_peminjaman, function(resp) {
        var data = JSON.parse(resp);
        $('#nama_anggota').html(data.nama_anggota);
        $('#isbn').html(data.isbn);
        $('#judul').html(data.judul);
        $('#peminjaman').html(data.tgl_pinjam);
        $('#pengembalian').html(data.tgl_kembali);
        $('#modalDetail').modal('show');
        $('.form-perpanjangan').attr('action', '{{ url('sirkulasi/perpanjangan_buku') }}'+'/'+data.id_peminjaman);
        $('#pengembalian-button').data('id', data.id_peminjaman);
        const tgl_kembali = new Date('');

        if(tgl_kembali > current_date) {
          const diffTime = Math.abs(current_date.getTime() - tgl_kembali.getTime());
          const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24))

        } else {
          const diffTime = Math.abs(current_date.getTime() - tgl_kembali.getTime());
          const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24))

        }
        $('#pengembalian-button').data('keterlamabatan', );
      });

      $('#perpanjangan-button').click(function() {
        $('#tampil_perpanjangan').show();
      });


      $('#close').click(function() {
        $('#nama_anggota').html("");
        $('#isbn').html("");
        $('#judul').html("");
        $('#peminjaman').html("");
        $('#pengembalian').html("");
        $('#tampil_perpanjangan').hide();
        $('#pengembalian-button').data('id', "");
      });

      $('#modalDetail').modal({
        backdrop: 'static',
        keyboard: false
      })
    });

    $('#pengembalian-button').click(function() {
      var id = $(this).data('id');
      var keterlamabatan = $(this).data('keterlamabatan');
      swal({
        title: "Pengembalian Buku ?",
        text: "Data peminjam akan disimpan di menu Pengembalian...",
        icon: "warning",
        buttons: ["Batal", "Ya"],
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          $.post("{{ url('sirkulasi/pengembalian_buku') }}"+"/"+id, {'_token': $('meta[name=csrf-token]').attr('content')});
          setTimeout(location.reload.bind(location), 2000);
        } else {
          swal("Pengembalian Dibatalkan!");
        }
      });

    });

    const date1 = new Date('7/13/2010');
  const date2 = new Date('12/15/2010');
  const diffTime = Math.abs(date2.getTime() - date1.getTime());
  const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));

</script>
