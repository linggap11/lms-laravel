@include('admin/_template/css')
<title>{{ $data['title'] }}</title>
<style type="text/css" media="screen">
  th {
    text-align: center; border-right: 2px solid #dddddd;
    border: bo
  }
  td {
    border-right: 2px solid #dddddd;
  }
</style>
@include('admin/_template/header')
<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">

                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Pengembalian</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->

    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Data Pengembalian</h5>
                        <span>Data Anggota Yang Telah Mengembalikan Buku</span>
                    </div>
                    <div class="card-body">
                       <div class="table-responsive">
                            <table id="table_pengembalian" class="display">
                                <thead>
                                    <tr>
                                      <th width="5%">No</th>
                                      <th width="35%">Judul Buku</th>
                                      <th width="25%">Anggota</th>
                                      <th width="10%">Pengembalian</th>
                                      <th width="5%">Terlambat</th>
                                      <th width="5%">Info</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  @php
                                    $no = 1;
                                  @endphp
                                  @foreach ($data['data_pengembalian'] as $pengembalian)
                                    <tr>
                                      <td align="center">{{ $no++ }}</td>
                                      <td>{{ $pengembalian->judul }}</td>
                                      <td>{{ $pengembalian->nama_anggota }}</td>
                                      <td align='center'>{{ date('Y-m-d', strtotime($pengembalian->tgl_pengembalian)) }}</td>
                                      @if ($pengembalian->terlambat > 0)
                                        <td align="center"><span class="btn btn-danger btn-xs">{{ $pengembalian->terlambat }} Hari</span></td>
                                      @else
                                        <td align="center"><span class="btn btn-info btn-xs">{{ $pengembalian->terlambat }} Hari</span></td>
                                      @endif
                                      <td align="center"><a href="#"  onclick="return false" data-id="{{ $pengembalian->id_peminjaman }}" title="Detail Buku" class="btn btn-primary btn-xs detail-peminjaman"><i class="fa fa-info-circle"> </i></a></td>
                                    </tr>
                                  @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts -->
    <div class="modal fade" id="modalDetail"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
       <div class="modal-content">
         <div class="modal-header">
             <h5 class="modal-title">Detail Peminjaman</h5>
             <button type="button" class="close" data-dismiss="modal" id="close">&times;</button>
         </div>
         <div class="modal-body">
           <div class="container">
             <div class="row">
               <div class="table-responsive">
                 <table class="table">
                   <thead class="table-primary table-border-vertical ">
                     <tr>
                       <th scope="col" width="20%">#</th>
                       <th scope="col">Data Peminjaman</th>
                     </tr>
                   </thead>
                   <tbody>
                     <tr>
                       <th scope="row" class="text-left">Anggota</th>
                       <td><span id="nama_anggota"></span></td>
                     </tr>
                     <tr>
                       <th scope="row" class="text-left">ISBN</th>
                       <td><span id="isbn"></span></td>
                     </tr>
                     <tr>
                       <th scope="row" class="text-left">Judul</th>
                       <td><span id="judul"></span></td>
                     </tr>
                     <tr>
                       <th scope="row" class="text-left">Peminjaman</th>
                       <td><span id="peminjaman"></span></td>
                     </tr>
                     <tr>
                       <th scope="row" class="text-left">Pengembalian</th>
                       <td><span id="pengembalian"></span></td>
                     </tr>
                   </tbody>
                 </table>
                 <hr>
               </div>
             </div>
           </div>

         </div>
         <div class="modal-footer">
           <button type="button" class="btn btn-danger" data-dismiss="modal" id="close">Close</button>
         </div>
       </div>
     </div>
    </div>

</div>
@include('admin/_template/js')
@include('admin/_template/footer')
<script type="text/javascript">
    $('#table_pengembalian').DataTable({

    });
    $('.detail-peminjaman').click(function() {
      var id_peminjaman = $(this).data('id');
      var terlambat = 0;
      $.get("{{ url('sirkulasi/get_detail_peminjaman') }}/"+id_peminjaman, function(resp) {
        var data = JSON.parse(resp);
        $('#nama_anggota').html(data.nama_anggota);
        $('#isbn').html(data.isbn);
        $('#judul').html(data.judul);
        $('#peminjaman').html(data.tgl_pinjam);
        $('#pengembalian').html(data.tgl_kembali);
        $('#modalDetail').modal('show');
        $('.form-perpanjangan').attr('action', '{{ url('sirkulasi/perpanjangan_buku') }}'+'/'+data.id_peminjaman);
        $('#pengembalian-button').data('id', data.id_peminjaman);      
      });

      $('#close').click(function() {
        $('#nama_anggota').html("");
        $('#isbn').html("");
        $('#judul').html("");
        $('#peminjaman').html("");
        $('#pengembalian').html("");
      });

      $('#modalDetail').modal({
        backdrop: 'static',
        keyboard: false
      })
    });
</script>
