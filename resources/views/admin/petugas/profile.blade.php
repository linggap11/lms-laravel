@include('admin/_template/css')
<title>{{ $data['title'] }}</title>
@include('admin/_template/header')
<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">

                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Profile</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->

    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Profile</h5>
                    </div>
                    <div class="card-body">
                        <div class="container">
                            <form class="" enctype="multipart/form-data" action="" method="post">
                              {{ csrf_field() }}
                              {{ method_field('PUT') }}

                            <div class="row">
                              <!-- left column -->
                              <div class="col-md-3">
                                <div class="text-center">
                                  <img id="preview"  width="150px" height="150px" src="{{ asset('uploads/petugas/'.$data['data_petugas']->foto.'') }}" class="avatar img-circle" alt="avatar">
                                  {{-- <h6>Upload a different photo...</h6> --}}

                                  {{-- <input type="file" name="foto" id="imgInp" class="form-control" value="{{ asset('uploads/petugas/'.$data['data_petugas']->foto.'') }}"> --}}
                                </div>
                              </div>

                              <!-- edit form column -->
                              <div class="col-md-9 personal-info">
                                <h3>Petugas info</h3>

                                  <div class="form-group">
                                    <label class="col-lg-3 control-label">Nama Lengkap</label>
                                    <div class="col-lg-8">
                                      <input class="form-control" name="nama" type="text" value="{{ $data['data_petugas']->nama_petugas }}" readonly>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-lg-3 control-label">Jenis Kelamin</label>
                                    <div class="col-lg-8">
                                      @if ($data['data_petugas']->jenis_kelamin == 'W')
                                        <input class="form-control" name="jenis_kelamin" type="text" value="WANITA" readonly>
                                      @else
                                        <input class="form-control" name="jenis_kelamin" type="text" value="PRIA" readonly>
                                      @endif
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-lg-3 control-label">Alamat</label>
                                    <div class="col-lg-8">
                                      <textarea name="alamat" class="form-control" readonly>{{ $data['data_petugas']->alamat }}</textarea>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-lg-3 control-label">No Telp</label>
                                    <div class="col-lg-8">
                                      <input class="form-control" name="telp" type="text" value="{{ $data['data_petugas']->telp }}" readonly>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-lg-3 control-label">Email</label>
                                    <div class="col-lg-8">
                                      <input class="form-control" type="text" name="email" value="{{ $data['data_petugas']->email }}" readonly>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-lg-3 control-label">Username</label>
                                    <div class="col-lg-8">
                                      <input class="form-control" type="text" name="username" value="{{ $data['data_petugas']->username }}" readonly>
                                    </div>
                                  </div>

                                </form>
                              </div>

                              <a href="{{ url('admin/setting_petugas') }}" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Kembali</a>
                          </div>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts -->
</div>
@include('admin/_template/js')
@include('admin/_template/footer')
<script type="text/javascript">
  function readURL(input) {

  if (input.files && input.files[0]) {
      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#imgInp").change(function() {
    readURL(this);
  });
  var reader = new FileReader();
      reader.onload = function(e) {
        $('#preview').attr('src', e.target.result);
      }

</script>
