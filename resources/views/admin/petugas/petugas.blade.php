@include('admin/_template/css')
<title>{{ $data['title'] }}</title>
<style type="text/css" media="screen">
  th {
    text-align: center; border-right: 2px solid #dddddd;
    border: bo
  }
  td {
    border-right: 2px solid #dddddd;
  }
</style>
@include('admin/_template/header')
<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">

                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Petugas</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->

    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Data Petugas</h5>
                    </div>
                    <div class="card-body">
                      @if (Session::get('role') == 'SU' || Session::get('role') == 'KEPALA PERPUSTAKAAN')
                        <button type="button" id="tambah-modal" class="btn btn-primary"><i class="icofont icofont-plus"></i>&nbsp; Data Petugas</button>
                        <br><br>
                      @endif

                       <div class="table-responsive">
                            <table id="table_anggota" class="display">
                                <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th width="25%">Nama Lengkap</th>
                                        <th width="20%">Email</th>
                                        <th width="15%">Username</th>
                                        <th width="20%">Bagian</th>
                                        <th align="center">Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  @php
                                    $no = 1;
                                  @endphp
                                  @foreach ($data['data_petugas'] as $petugas)
                                    @if ($petugas->role != 'SU')
                                      <tr>
                                          <td align="center">{{ $no++ }}</td>
                                          <td>{{ $petugas->nama_petugas }}</td>
                                          <td>{{ $petugas->email }}</td>
                                          <td>{{ $petugas->username }}</td>
                                          <td>{{ $petugas->role }}</td>
                                          <td align="center">
                                            <a href="{{ url('admin/setting_petugas/'.$petugas->id_petugas.'') }}" target="_blank" title="Detail Petugas" class="detail-petugas btn btn-primary btn-xs">
                                              <i class="fa fa-info-circle"> </i>
                                            </a>
                                            @if (Session::get('role') == 'SU' || Session::get('role') == 'KEPALA PERPUSTAKAAN')
                                              <a href="#" onclick="return false" title="Edit Petugas" class="edit-petugas btn btn-warning btn-xs">
                                                <i class="icofont icofont-edit"></i>
                                              </a>
                                              <a href="#" onclick="return false" title="Hapus Petugas" class="hapus-petugas btn btn-danger btn-xs">
                                                <i class="icofont icofont-ui-delete"></i>
                                              </a>
                                              <form class="" action="{{ url('admin/setting_petugas/'.$petugas->id_petugas.'') }}" method="post">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}

                                              </form>
                                            @endif
                                          </td>
                                      </tr>
                                    @endif
                                  @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts -->
    <div class="modal fade" id="tambah_dataModal" aria-hidden="true">
     <div class="modal-dialog modal-lg" role="document">
       <div class="modal-content">
         <form class="" action="{{ url('admin/setting_petugas') }}" method="post">
           {{ csrf_field() }}
           <div class="modal-header">
               <h5 class="modal-title">Tambah Data Petugas</h5>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
               </button>
           </div>

           <div class="modal-body">

             <div class="row">
               <!-- left column -->
               <div class="col-lg-4">
                 <div class="text-center">
                   <img width="150px"  src="{{ asset('uploads/petugas/admin-png-8.png') }}" class="preview avatar" alt="Foto">
                   <h6>Upload Foto...</h6>

                   <input type="file" name="foto" class="imgInp form-control" value="">
                 </div>
               </div>

               <!-- edit form column -->
               <div class="col-lg-8 personal-info">

                 <div class="form-group">
                     <label for="recipient-name" class="col-form-label" >Nama Lengkap:</label>
                     <input type="text" class="form-control" name="nama_petugas" required="required">
                 </div>
                 <div class="form-group">
                     <label for="recipient-name" class="col-form-label" >Jenis Kelamin:</label>
                     <select class="form-control" name="jenis_kelamin">
                       <option value="P">Pria</option>
                       <option value="W">Wanita</option>
                     </select>
                 </div>
                 <div class="form-group">
                     <label for="recipient-name" class="col-form-label" >No Telp:</label>
                     <input type="text" class="form-control" name="telp" >
                 </div>
                 <div class="form-group">
                     <label for="message-text" class="col-form-label">Alamat:</label>
                     <textarea class="form-control" name="alamat" ></textarea>
                 </div>
                 <div class="form-group">
                     <label for="recipient-name" class="col-form-label" >email:</label>
                     <input type="email" class="form-control" name="email" >
                 </div>
                 <div class="form-group">
                     <label for="recipient-name" class="col-form-label" >username:</label>
                     <input type="text" class="form-control" name="username" >
                 </div>
                 <div class="form-group">
                     <label for="recipient-name" class="col-form-label" >Bagian:</label>
                     <select class="form-control" name="role">
                       <option value="ENTRY BUKU">ENTRY BUKU</option>
                       <option value="SIRKULASI">SIRKULASI</option>
                       <option value="HUMAS">HUMAS</option>
                       <option value="KEPALA PERPUSTAKAAN">KEPALA PERPUSTAKAAN</option>
                     </select>
                 </div>
                 <div class="form-group">
                     <label for="recipient-name" class="col-form-label" >Password:</label>
                     <input type="password" class="form-control" name="password" >
                 </div>
               </div>
           </div>
           <div class="modal-footer">
             <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
             <button type="submit" class="btn btn-warning">Simpan</button>
           </div>
         </div>
       </div>
     </form>
    </div>
  </div>
</div>
@include('admin/_template/js')
@include('admin/_template/footer')
<script type="text/javascript">
    $('#table_anggota').DataTable({

    });

    $('#tambah-modal').click(function() {
      @if (Session::get('role') == "SU" || Session::get('role') == "KEPALA PERPUSTAKAAN")
        $('#tambah_dataModal').modal('show');
      @else
        alert('Anda tidak punya hak akses untuk ini ...');
      @endif
    });

    $('.hapus-petugas').click(function() {
      var id = $(this).data('id');
      swal({
        title: "Anda Yakin ?",
        text: "Data yang anda hapus tidak akan kembali lagi...",
        icon: "warning",
        buttons: ["Batal", "Hapus?"],
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          $('form#hapus').submit()
        } else {
          swal("Data batal dihapus!");
        }
      });
    });


    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          $('.preview').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
      }
    }

    $(".imgInp").change(function() {
      readURL(this);
    });
</script>
