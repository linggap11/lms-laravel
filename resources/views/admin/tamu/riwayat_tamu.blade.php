@include('admin/_template/css')
<title>{{ $data['title'] }}</title>
<style type="text/css" media="screen">
  th {
    text-align: center; border-right: 2px solid #dddddd;
    border: bo
  }
  td {
    border-right: 2px solid #dddddd;
  }
</style>
@include('admin/_template/header')
<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">

                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Riwayat Tamu</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->

    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Data Riwayat Terakhir Tamu</h5>
                        <span>Riwayat Tamu Terakhir Yang Berkunjung </span>
                    </div>
                    <div class="card-body">
                       <div class="table-responsive">
                            <table id="table_anggota" class="display">
                                <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th>Nama Lengkap</th>
                                        <th width="">Waktu Kunjung</th>
                                        <th width="">Waktu Pulang</th>
                                        <th>Total Kunjung</th>
                                        <th width="">Kelas</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        for ($i = 1; $i < 15; $i++) {
                                            ?>
                                            <tr>
                                        <td><?= $i ?></td>
                                        <td>John Doe <?= $i ?></td>
                                        <td align="center">2018-02-07 08.31 </td>
                                        <td align="center">2018-02-07 13.31 </td>
                                        <td align="center">10x</td>
                                        <td>Kelas AB - <?= $i ?></td>
                                        <td align="center"><a href="#" title="Detail Anggota" class="btn btn-primary btn-xs"><i class="fa fa-info-circle"> </i> Detail Anggota</a></td>
                                    </tr>
                                            <?php
                                        }
                                     ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts -->

</div>
@include('admin/_template/js')
@include('admin/_template/footer')
<script type="text/javascript">
    $('#table_anggota').DataTable({

    });
</script>
