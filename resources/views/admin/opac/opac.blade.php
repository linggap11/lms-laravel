@include('admin/_template/css')
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<title>{{ $data['title'] }}</title>
<style type="text/css" media="screen">
  th {
    text-align: center; border-right: 2px solid #dddddd;
    border: bo
  }
  td {
    border-right: 2px solid #dddddd;
  }
</style>
@include('admin/_template/header')
<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">

                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Opac</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->

    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>O P A C</h5>
                        <span><i> Open Public Access Catalogue</i></span>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <!-- Nav tabs -->
                                <div class="card">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Sederhana</a></li>
                                    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Spesifik</a></li>
                                  

                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="home">
                                        <form action="{{ url('Opac/pencarian_sederhana') }}" method="get" accept-charset="utf-8">
                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <label for="">Cari Di</label>
                                              <select name="" class="form-control" style="height: 30px">
                                                  <option value="SESKOAU">Di Perpustakaan SESKO AU</option>
                                              </select>
                                            </div>
                                            <div class="form-group">
                                              <input type="text" name="keyword" value="" placeholder="Kata Kunci" class="form-control">
                                            </div>
                                            <div class="form-group">
                                              <button type="submit" class="btn btn-primary">Cari</button>
                                            </div>
                                          </div>
                                        </form>

                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="profile">
                                        <form action="{{ url('Opac/pencarian_spesifik') }}" method="get" accept-charset="utf-8">
                                          <div class="col-md-6">
                                            <div class="form-group">
                                              <label for="">Cari Di</label>
                                              <select name="" class="form-control" style="height: 30px">
                                                  <option value="all">Semua</option>
                                                  <option value="SESKOAU">Lokasi A</option>
                                              </select>
                                              </div>
                                              <div class="form-group">
                                                <label for="">Judul</label>
                                                <input type="text" name="judul" value="" placeholder="Kata Kunci" class="form-control">
                                              </div>
                                              <div class="form-group">
                                                <label for="">Subyek</label>
                                                <input type="text" name="subyek" value="" placeholder="Kata Kunci" class="form-control">
                                              </div>
                                              <div class="form-group">
                                                <label for="">Pengarang</label>
                                                <input type="text" name="pengarang" value="" placeholder="Kata Kunci" class="form-control">
                                              </div>

                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                  <label for="">Penerbit</label>
                                                  <input type="text" name="penerbit" value="" placeholder="Kata Kunci" class="form-control">
                                                </div>
                                                <div class="form-group">
                                                  <label for="">Jenis</label>
                                                  <select name="jenis" class="form-control" style="height: 30px">
                                                      <option value="PUBLIC">Public</option>
                                                      <option value="PRIVATE">Private</option>
                                                  </select>
                                                </div>
                                                <div class="form-group">
                                                  <label for="">ISBN</label>
                                                  <input type="text" name="isbn" value="" placeholder="Kata Kunci" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <button type="submit" class="btn btn-primary btn-block">Cari</button>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                                @if (isset($data['data_buku']))
                                  <div class="table-responsive">
                                    <div class="col-md-6">
                                      <i>Pencarian dengan kata kunci <b>"{{ $data['keyword'] }}"</b></i>
                                    </div>
                                    <table id="data_buku" class="display">
                                        <thead>
                                            <tr>
                                             <th rowspan="2" width="5%">No</th>
                                             <th colspan="3">Title</th>
                                             <th colspan="3">Publisher</th>
                                             <th rowspan="2"></th>
                                            </tr>
                                            <tr>
                                              <th>ISBN</th>
                                              <th>Judul</th>
                                              <th width="5%">Edisi</th>
                                              <th>Pengarang</th>
                                              <th>Penerbit</th>
                                              <th>Tahun</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          @php
                                            $no=1;
                                          @endphp
                                          @foreach ($data['data_buku'] as $buku)
                                            <tr>
                                              <td align="center">{{ $no++ }}</td>
                                              <td>{{ $buku->isbn }}</td>
                                              <td>{{ $buku->judul }}</td>
                                              <td align="center">{{ $buku->edisi }}</td>
                                              <td>{{ $buku->pengarang }}</td>
                                              <td>{{ $buku->penerbit }}</td>
                                              <td>{{ $buku->tahun_terbit }}</td>
                                              <td align="center"><a href="#" title="Detail Buku {{ $buku->judul }}" class="btn btn-info btn-xs">Detail</a></td>
                                            </tr>
                                          @endforeach

                                        </tbody>
                                    </table>
                                  </div>
                                @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts -->
</div>
@include('admin/_template/js')
@include('admin/_template/footer')
<script type="text/javascript">
  $(document).ready(function(){
    $('#data_buku').DataTable({
      "bLengthChange": false,
      "pageLength": 25,
      "columnDefs": [ {

        } ]
    });
  });
</script>
