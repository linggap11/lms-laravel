</body>
</html>
<script>
  $( document ).ready(function() {
    /** add active class and stay opened when selected */
    var url = window.location;
    if (url == "{{ url('admin/home') }}") {
      $('._dashboard').addClass('active')
    }
    $('ul.sidebar-menu a').filter(function() {
         return this.href == url;
    }).closest('a').addClass('active');

   	$('ul.sidebar-submenu a').filter(function() {
			 return this.href == url;
		}).closest('.tree-sub').addClass('active');
 });

</script>
