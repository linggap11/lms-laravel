<body main-theme-layout="main-theme-layout-1">
<!-- Loader starts -->
<div class="loader-wrapper">
    <div class="loader bg-white">
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
    </div>
</div>
<!-- Loader ends -->

<!--page-wrapper Start-->
<div class="page-wrapper">
    <!--Page Header Start-->
    <div class="page-main-header">
        <div class="main-header-left" semilight-bg-color="bg-secondary-light-color">
            <div class="logo-wrapper">
                <a href="{{ url('/admin/home') }}">
                    <div class="" style="margin-top: 15px;">
                        <img src="{{ asset('uploads/'.Session::get('logo_perpus').'') }}" alt="" style="margin: 25px 0 40px 0; width: 60px; height: 60px" >
                        <span style="color: #ffff; font-family: arial; font-size: 25px; margin-left: 10px;">{{ Session::get('alias_perpus') }}</span>
                    </div>
                </a>
            </div>
        </div>
        <div class="main-header-right row" header-bg-color="bg-secondary-light-color">
            <div class="mobile-sidebar">
                <div class="media-body text-right switch-sm">
                    <label class="switch">
                        <input type="checkbox" id="sidebar-toggle" checked>
                        <span class="switch-state"></span>
                    </label>
                </div>
            </div>
            <div class="nav-right col">
                <ul class="nav-menus">
                    <li>
                        <form class="form-inline search-form">
                            <div class="form-group">
                                <label class="sr-only">Email</label>
                                <input type="search"  class="form-control-plaintext" placeholder="Search.." >
                                <span class="d-sm-none mobile-search">
                                </span>
                            </div>
                        </form>
                    </li>
                    <li class="onhover-dropdown">
                        <div class="media  align-items-center">
                            <img class="align-self-center pull-right mr-2" src="{{ asset('assets/universal/images/dashboard/user.png') }}" alt="header-user"/>
                            <div class="media-body">
                                <h6 class="m-0 txt-dark f-16">
                                    My Account
                                    <i class="fa fa-angle-down pull-right ml-2"></i>
                                </h6>
                            </div>
                        </div>
                        <ul class="profile-dropdown onhover-show-div p-20">
                            <li>
                                <a href="{{ url('/admin/setting_petugas/'.Session::get('id_petugas').'' ) }}">
                                    <i class="icon-user"></i>
                                    Edit Profile
                                </a>
                            </li>
                            <li>
                                <a href="{{ url('/admin/logout') }}">
                                    <i class="icon-power-off"></i>
                                    Logout
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <div class="d-lg-none mobile-toggle">
                    <i class="icon-more"></i>
                </div>
            </div>
        </div>
    </div>
    <!--Page Header Ends-->
    @include('admin._template.side_menu')
