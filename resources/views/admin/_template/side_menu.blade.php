<!--Page Body Start-->
    <div class="page-body-wrapper">
        <!--Page Sidebar Start-->
        <div class="page-sidebar custom-scrollbar">
            <div class="sidebar-user text-center">
                <div>
                    @if (Session::get('foto') == null)
                      <img class="img-60 rounded-circle pull-left" src="{{ asset('uploads/petugas/admin-png-8.png') }}" alt="Profile">
                    @else
                      <img class="img-60 rounded-circle pull-left" src="{{ asset('uploads/petugas/'.Session::get('foto').'') }}" alt="Profile">
                    @endif
                </div>
                <div class="admin">
                    <h6 class="mt-3 f-12"><a style="color: #ffff;" href="{{ url('admin/petugas/profile/'.Session::get('id_petugas').'' ) }}" >{{ Session::get('nama_petugas') }}</a></h6>
                    <span style="color: #ffff; font-size: 10px"><i class="fa fa-circle text-success"></i> Online</span>
                </div>
            </div>
            <ul class="sidebar-menu">
                <div class="sidebar-title">Menu Utama <i class="icofont icofont-navigation-menu pull-right" style="margin-top: 3px;"></i></div>
                <li>
                    <a href="{{ url('/admin/home') }}" class="menu sidebar-header _dashboard" id="dashboard">
                        <i class="icofont icofont-dashboard-web"></i><span> Dashboard</span>
                    </a>
                </li>
                @if (Session::get('role') == 'SU' || Session::get('role') == 'KEPALA PERPUSTAKAAN' || Session::get('role') == 'ENTRY BUKU' )
                  <li class="tree-sub">
                      <a href="#" class="sidebar-header">
                          <i class="icofont icofont-book-alt"></i><span> Buku</span>
                          <i class="fa fa-angle-right pull-right"></i>
                      </a>
                      <ul class="sidebar-submenu">
                          <li><a href="{{ url('/admin/buku') }}"><i class="fa fa-angle-right"></i>Daftar Buku</a></li>
                          <li><a href="{{ url('/admin/kategori_buku') }}"><i class="fa fa-angle-right"></i>Kategori Buku</a></li>

                          <li><a href="{{ url('/admin/label_buku') }}"><i class="fa fa-angle-right"></i>Cetak Label</a></li>
                      </ul>
                  </li>
                @endif

                @if (Session::get('role') == 'SU' || Session::get('role') == 'SIRKULASI' || Session::get('role') == 'KEPALA PERPUSTAKAAN' )
                  <li>
                      <a href="{{ url('/admin/opac') }}" class="menu sidebar-header" id="opac">
                          <i class="icofont icofont-library"></i><span> OPAC</span>
                      </a>
                  </li>
                @endif
                @if (Session::get('role') == 'SU' || Session::get('role') == 'KEPALA PERPUSTAKAAN' || Session::get('role') == 'SIRKULASI' )
                  <li class="tree-sub">
                      <a href="#" class="sidebar-header">
                          <i class="icofont icofont-ui-user"></i><span> Anggota</span>
                          <i class="fa fa-angle-right pull-right"></i>
                      </a>
                      <ul class="sidebar-submenu">
                          <li><a class="" href="{{ url('/admin/anggota') }}"><i class="fa fa-angle-right"></i>Daftar Anggota</a></li>
                          <li><a class="" href="{{ url('/admin/generate_memberid') }}"><i class="fa fa-angle-right"></i>Generate Kartu Anggota</a></li>
                      </ul>
                  </li>
                @endif

                @if (Session::get('role') == 'SU' || Session::get('role') == 'KEPALA PERPUSTAKAAN' || Session::get('role') == 'HUMAS'  )
                  <li class="tree-sub">
                      <a href="#" class="sidebar-header">
                          <i class="fa fa-book"></i><span> Buku Tamu</span>
                          <i class="fa fa-angle-right pull-right"></i>
                      </a>
                      <ul class="sidebar-submenu">
                          <li><a class="" href="{{ url('/admin/tamu') }}"><i class="fa fa-angle-right"></i>Daftar Tamu</a></li>
                          <li><a class="" href="{{ url('/admin/riwayat_tamu') }}"><i class="fa fa-angle-right"></i>Riwayat Tamu</a></li>
                      </ul>
                  </li>
                @endif
                @if (Session::get('role') == 'SU' || Session::get('role') == 'KEPALA PERPUSTAKAAN' || Session::get('role') == 'SIRKULASI' )
                  <li class="tree-sub">
                      <a href="#" class="sidebar-header">
                          <i class="fa fa-retweet"></i><span> Sirkulasi</span>
                          <i class="fa fa-angle-right pull-right"></i>
                      </a>
                      <ul class="sidebar-submenu">
                          <li><a href="{{ url('/admin/peminjaman') }}"><i class="fa fa-angle-right"></i>Peminjaman</a></li>
                          <li><a href="{{ url('/admin/pengembalian') }}"><i class="fa fa-angle-right"></i>Pengembalian</a></li>
                          <li><a href="{{ url('/admin/riwayat_sirkulasi') }}"><i class="fa fa-angle-right"></i>Riwayat Terakhir</a></li>
                      </ul>
                  </li>
                @endif

                @if (Session::get('role') == 'SU' || Session::get('role') == 'KEPALA PERPUSTAKAAN' || Session::get('role') == 'HUMAS')
                  <li>
                      <a href="{{ url('/admin/berita') }}" class="sidebar-header">
                          <i class="icofont icofont-newspaper"></i><span> Berita & Artikel</span>
                      </a>
                  </li>
                @endif
                 @if (Session::get('role') == 'SU' || Session::get('role') == 'KEPALA PERPUSTAKAAN' || Session::get('role') == 'ENTRY BUKU')
                   <li>
                       <a href="{{ url('/admin/usulan_buku') }}" class="sidebar-header">
                           <i class="icofont icofont-inbox"></i><span> Usulan Buku</span>
                       </a>
                   </li>
                 @endif
                 @if (Session::get('role') == 'SU')
                   <li>
                      <a href="{{ url('/admin/log') }}" class="sidebar-header">
                          <i class="icofont icofont-listine-dots"></i><span> Log</span>
                      </a>
                  </li>
                 @endif
                @if (Session::get('role') != 'HUMAS') 
                  <li class="tree-sub">
                      <a href="#" class="sidebar-header">
                          <i class="icofont icofont-file-pdf"></i><span> Laporan</span>
                          <i class="fa fa-angle-right pull-right"></i>
                      </a>
                      <ul class="sidebar-submenu">
                          <li><a href="{{ url('/admin/laporan_buku') }}"><i class="fa fa-angle-right"></i>Laporan Data Koleksi Buku</a></li>
                          <li><a href="{{ url('/admin/laporan_anggota') }}"><i class="fa fa-angle-right"></i>Laporan Data Anggota</a></li>
                          <li><a href="{{ url('/admin/laporan_ketersidiaan_buku') }}"><i class="fa fa-angle-right"></i>Laporan Ketersediaan Buku</a></li>
                          <li><a href="{{ url('/admin/laporan_sirkulasi') }}"><i class="fa fa-angle-right"></i>Laporan Buku Yang Dipinjam</a></li>
                          <li><a href="{{ url('/admin/laporan_lokasi_buku') }}"><i class="fa fa-angle-right"></i>Laporan Data Lokasi Buku</a></li>
                      </ul>
                  </li>
                @endif
                @if (Session::get('role') == 'SU')
                  <li class="tree-sub">
                      <a href="#" class="sidebar-header">
                          <i class="icofont icofont-database"></i><span> Backup & Restore</span>
                          <i class="fa fa-angle-right pull-right"></i>
                      </a>
                      <ul class="sidebar-submenu">
                          <li><a href="{{ url('/admin/backup') }}"><i class="fa fa-angle-right"></i>Data Master</a></li>
                      </ul>
                  </li>
                @endif
                @if (Session::get('role') == 'SU' || Session::get('role') == 'HUMAS')
                  <li>
                    <div class="sidebar-title">Pengaturan Website<i class="icofont icofont-gear pull-right" style="margin-top: 3px;"></i></div>
                      <li>
                      <a href="{{ url('/admin/setting_tampilan') }}" class="sidebar-header">
                          <i class="icon-palette"></i><span> Tampilan</span>
                      </a>
                      </li>
                  </li>
                @endif
                <li>
                    <div class="sidebar-title">Pengaturan <i class="icofont icofont-gear pull-right" style="margin-top: 3px;"></i></div>
                    <li>
                    <a href="{{ url('/admin/setting_perpus') }}" class="sidebar-header">
                        <i class="icofont icofont-library"></i><span> Perpustakaan</span>
                    </a>
                    <a href="{{ url('/admin/setting_petugas') }}" class="sidebar-header">
                        <i class="icofont icofont-support"></i><span> Petugas</span>
                    </a>
                    <a href="{{ url('/admin/setting_sop') }}" class="sidebar-header">
                        <i class="icofont icofont-listine-dots"></i><span> S.O.P</span>
                    </a>
                    @if (Session::get('role') == 'SU' || Session::get('role') == 'KEPALA PERPUSTAKAAN' )
                      <a href="{{ url('/admin/setting_keylock') }}" class="sidebar-header">
                          <i class="icofont icofont-lock"></i><span> Keylock</span>
                      </a>
                    @endif
                </li>
              </li>
            </ul>
            <div class="sidebar-widget text-center">
                <div class="sidebar-widget-top">
                    <h6 class="mb-2 fs-14">Need Help ?</h6>
                    <i class="icon-bell"></i>
                </div>
                <div class="sidebar-widget-bottom p-20 m-20">
                    <p>+62 234 567 899
                        <br>superadmin@email.com
                        <br><a href="#">Lihat Dokumentasi</a>
                    </p>
                </div>
            </div>
        </div>
        <!--Page Sidebar Ends-->
