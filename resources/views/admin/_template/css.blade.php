@if (Session::get('id_petugas') == null)
  <script>window.location = "/admin";</script>
@endif
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Library management system">
    <meta name="keywords" content="">
    <meta name="author" content="Lingga Pangestu">
    <link rel="icon" href="{{ asset('assets/universal/images/favicon.png') }}" type="image/x-icon" />
    <link rel="shortcut icon" href="{{ asset('assets/universal/images/favicon.png') }}" type="image/x-icon" />

    <!--Google font-->


    <!--Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/universal/css/font-awesome/css/font-awesome.min.css') }}">
    <!-- ico-font -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/universal/css/icofont/icofont.css') }}">
    <!-- Themify icon -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/universal/css/themify/themify-icons.css') }}">
    <!-- Bootstrap css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/universal/css/bootstrap.css') }}">
    <!--JSGrid css-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/universal/css/datatables.css') }}"/>
    <!-- App css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/universal/css/style.css') }}">
    <!-- Responsive css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/universal/css/responsive.css') }}">
    <style type="text/css" media="screen">

    </style>
</head>
