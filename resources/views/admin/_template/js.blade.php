  <!-- latest jquery-->
<script src="{{ asset('assets/universal/js/jquery-3.2.1.min.js') }}"></script>
<!-- Bootstrap js-->
<script src="{{ asset('assets/universal/js/bootstrap/popper.min.js') }}"></script>
<script src="{{ asset('assets/universal/js/bootstrap/bootstrap.js') }}"></script>

<!--Datatable js-->
<script src="{{ asset('assets/universal/js/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/universal/js/datatables/datatable.custom.js') }}"></script>

<!-- Sidebar jquery-->
<script src="{{ asset('assets/universal/js/sidebar-menu.js') }}"></script>
<!-- Theme js-->
<script src="{{ asset('assets/universal/js/script.js') }}"></script>
