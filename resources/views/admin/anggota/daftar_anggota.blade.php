@include('admin/_template/css')
<title>{{ $data['title'] }}</title>
<style type="text/css" media="screen">
  th {
    text-align: center; border-right: 2px solid #dddddd;
    border: bo
  }
  td {
    border-right: 2px solid #dddddd;
  }
</style>
@include('admin/_template/header')
<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">

                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Anggota</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->

    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        @if (session()->has('message_tambah'))
                          <div class="alert alert-success alert-dismissible fade show" role="alert" style="color:black">
                              Anggota <strong>{{ session()->get('message_tambah') }}</strong> berhasil ditambahkan!
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                        @elseif (session()->has('message_update'))
                          <div class="alert alert-warning alert-dismissible fade show" role="alert" style="color:black">
                              Anggota <strong>{{ session()->get('message_update') }}</strong> berhasil diupdate!
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                        @elseif (session()->has('message_hapus'))
                          <div class="alert alert-danger alert-dismissible fade show" role="alert" style="color:black">
                              Anggota <strong>{{ session()->get('message_hapus') }}</strong> berhasil dihapus!
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                        @endif
                        <h5>Data Anggota</h5>
                        <span>Data Anggota Yang Telah Terdaftar Di Perpustakaan </span>
                    </div>
                    <div class="card-body">
                        <button type="" class="btn btn-primary" data-toggle="modal" data-target="#tambah_dataModal" data-whatever="@mdo"><i class="icofont icofont-plus"></i>&nbsp; Data Anggota</button> <br><br><br><br>
                       <div class="table-responsive">
                          <table id="table_anggota" class="display">
                            <thead>
                              <tr>
                                <th width="5%">No</th>
                                <th>Nama Lengkap</th>
                                <th width="">Email</th>
                                <th width="5%">Pangkat</th>
                                <th width="5%">Status</th>
                                <th width="15%">Info</th>
                              </tr>
                            </thead>
                            <tbody>
                              @php
                                $no = 1;
                              @endphp
                              @foreach ($data['data_anggota'] as $anggota)
                                <tr>
                                  <td align="center">{{ $no++ }}</td>
                                  <td>{{ $anggota->nama_anggota }}</td>
                                  <td>{{ $anggota->email }}</td>
                                  <td class="text-center">{{ $anggota->pangkat }}</td>
                                  @if ($anggota->status == "AKTIF")
                                    <td align="center"><span class="btn btn-success btn-xs">Aktif</span></td>
                                  @else
                                    <td align="center"><span class="btn btn-danger btn-xs">Tidak Aktif</span></td>
                                  @endif
                                  <td class="text-center">
                                    @if (Session::get('role') == "SU" || Session::get('role') == "SIRKULASI")
                                      <a href="#" onclick="return false" title="Edit Anggota" class="editClick btn btn-warning btn-xs" data-toggle="modal" data-target="#edit_dataModal" data-id="{{ $anggota->id_anggota }}"><i class="fa fa-pencil"> </i> Edit</a> &nbsp
                                    @endif

                                    <a href="#" onclick="return false" title="Detail Anggota" class="detailClick btn btn-primary btn-xs" data-toggle="modal" data-target="#modalDetail" data-id="{{ $anggota->id_anggota }}"><i class="fa fa-info-circle"> </i> Detail</a>
                                  </td>
                                  <form class="" action="{{ url('admin/anggota/'.$anggota->id_anggota.'') }}" method="post">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}

                                  </form>
                                </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts -->
    <div class="modal fade" id="tambah_dataModal" aria-hidden="true">
     <div class="modal-dialog modal-lg" role="document">
       <div class="modal-content">
         <form enctype="multipart/form-data" class="" action="{{ url('admin/anggota') }}" method="post">
           {{ csrf_field() }}
           <div class="modal-header">
               <h5 class="modal-title">Tambah Data Anggota</h5>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                   <span aria-hidden="true">&times;</span>
               </button>
           </div>

           <div class="modal-body">

             <div class="row">
               <!-- left column -->
               <div class="col-lg-4">
                 <div class="text-center">
                   <img width="150px"  src="{{ asset('uploads/anggota/default.png') }}" class="preview avatar" alt="Cover Buku">
                   <h6>Upload Foto...</h6>

                   <input type="file" name="foto" class="imgInp form-control" value="">
                 </div>
               </div>

               <!-- edit form column -->
               <div class="col-lg-8 personal-info">
                 <div class="form-group">
                     <label for="recipient-name" class="col-form-label" >Kode RFID <span class="btn btn-primary btn-xs">SCAN !</span></label>
                     <input type="text" readonly="" class="form-control" >
                 </div>
                 <div class="form-group">
                     <label for="recipient-name" class="col-form-label" >NIM: </label>
                     <input type="text" class="form-control" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"  name="nim" required="required">
                 </div>
                 <div class="form-group">
                     <label for="recipient-name" class="col-form-label" >Nama Anggota:</label>
                     <input type="text" class="form-control" name="nama_anggota" required="required" pattern="[a-zA-Z]*">
                 </div>
                 <div class="form-group">
                     <label for="recipient-name" class="col-form-label" >Email:</label>
                     <input type="text" class="form-control" name="email" >
                 </div>
                 <div class="form-group">
                     <label for="recipient-name" class="col-form-label" >Pangkat:</label>
                     <input type="text" class="form-control" name="pangkat" >
                 </div>
                 <div class="form-group">
                     <label for="recipient-name" class="col-form-label" >No Telp:</label>
                     <input type="text" class="form-control" name="telp" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" >
                 </div>
                 <div class="form-group">
                     <label for="message-text" class="col-form-label">Alamat:</label>
                     <textarea class="form-control" name="alamat" ></textarea>
                 </div>
                 <div class="form-group">
                     <label for="recipient-name" class="col-form-label" >Password:</label>
                     <input type="password" class="form-control" name="password" id="password" >
                 </div>
                 <div class="form-group">
                     <label for="recipient-name" class="col-form-label" >Re-Password:</label>
                     <input type="password" class="form-control" name="repassword" id="confirm_password" >
                     <span id="message"></span>
                 </div>
               </div>
           </div>
           <div class="modal-footer">
             <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
             <button type="submit" class="btn btn-warning">Simpan</button>
           </div>
         </div>
       </div>
     </form>
    </div>
    </div>
</div>

<div class="modal fade" id="modalDetail"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
 <div class="modal-dialog modal-lg" role="document">
   <div class="modal-content">
       <div class="modal-header">
           <h5 class="modal-title">Detail Anggota</h5>
       </div>
       <div class="modal-body">
         <div class="container">
             <div class="row">
              <div class="col-lg-12">
                <center>
                  <table id='tb_detail' class="table table-borderless">
                    <thead>
                      <tr>
                        <th width="20%"></th>
                        <th width="80%"></th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </center>

              </div>
             </div>
           </div>
       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
       </div>
   </div>
 </div>
</div>

<div class="modal fade" id="edit_dataModal" aria-hidden="true">
 <div class="modal-dialog modal-lg" role="document">
   <div class="modal-content">
     <form enctype="multipart/form-data" id="edit_anggota" class="" action="" method="post">
       {{ csrf_field() }}
       {{ method_field('PUT') }}
       <div class="modal-header">
           <h5 class="modal-title">Edit Data Anggota</h5>
           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
           </button>
       </div>

       <div class="modal-body">

         <div class="row">
           <!-- left column -->
           <div class="col-lg-4">
             <div class="text-center">
               <img width="150px"  src="{{ asset('uploads/anggota/default.png') }}" class="preview avatar" alt="Cover Buku">
               <h6>Upload Foto...</h6>

               <input type="file" name="foto" class="imgInp form-control" value="">
             </div>
           </div>

           <!-- edit form column -->
           <div class="col-lg-8 personal-info">
             <div class="form-group">
                 <label for="recipient-name" class="col-form-label" >NIM: </label>
                 <input type="text" class="form-control" name="nim" required="required" id="nim" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                 <input type="hidden" name="id_anggota" id="id_anggota">
             </div>
             <div class="form-group">
                 <label for="recipient-name" class="col-form-label" >Nama Anggota:</label>
                 <input type="text" class="form-control" name="nama_anggota" required="required" id="nama_anggota"  pattern="[a-zA-Z]*">
             </div>
             <div class="form-group">
                 <label for="recipient-name" class="col-form-label" >Email:</label>
                 <input type="text" class="form-control" name="email" id="email">
             </div>
             <div class="form-group">
                 <label for="recipient-name" class="col-form-label" >Pangkat:</label>
                 <input type="text" class="form-control" name="pangkat" id="pangkat">
             </div>
             <div class="form-group">
                 <label for="recipient-name" class="col-form-label" >No Telp:</label>
                 <input type="text" class="form-control" name="telp" id="telp" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
             </div>
             <div class="form-group">
                 <label for="message-text" class="col-form-label">Alamat:</label>
                 <textarea class="form-control" name="alamat" id="alamat"></textarea>
             </div>
           </div>
       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
         <button type="submit" class="btn btn-warning">Simpan</button>
       </div>
     </div>
   </div>
 </form>
</div>
</div>
@include('admin/_template/js')
@include('admin/_template/footer')
<script type="text/javascript">
    $('#table_anggota').DataTable({

    });

    $('.detailClick').click(function() {
      var id_anggota = $(this).data('id');
      $.get("{{ url('admin/anggota/') }}"+'/'+id_anggota, function(resp) {
        var data = JSON.parse(resp);
        var content = "";
        if (data.foto == null) {
          content = '<tr><td class="text-center"><img src="{{ asset('uploads/anggota/default.png') }}" title="Tidak Tersedia" width="150px"  alt="cover"></td>'+
          '<td class="detail"><strong>'+data.nama_anggota+' ('+data.nim+')</strong><br>'+
          'Pangkat : <a href="#" onclick="return false" title="Pangkat">'+data.pangkat+'</a> <br>'+
          '<a href="#" onclick="return false" title="'+data.email+'">'+data.email+'</a>  <br>'+
          '<strong>Alamat: </strong> '+data.alamat+'<br>'+
          '<span class="btn btn-primary btn-xs">'+data.status+'</span>&nbsp;<tr>';
        } else {
          content = '<tr><td class="text-center"><img src="{{ asset('uploads/anggota/') }}'+'/'+data.foto+'" title="'+data.foto+'" width="150px" alt="cover"></td>'+
          '<td class="detail"><strong>'+data.nama_anggota+' ('+data.nim+')</strong><br>'+
          'Pangkat : <a href="#" onclick="return false" title="Pangkat">'+data.pangkat+'</a> <br>'+
          '<a href="#" onclick="return false" title="'+data.email+'">'+data.email+'</a>  <br>'+
          '<strong>Alamat: </strong> '+data.alamat+'<br>'+
          '<span class="btn btn-primary btn-xs">'+data.status+'</span>&nbsp;<tr>';
        }

        $('#tb_detail tbody').append(content);

      });
    });

    $('.editClick').click(function() {
      var id_anggota = $(this).data('id');
      $.get("{{ url('admin/anggota/') }}"+'/'+id_anggota+'edit', function(resp) {
        var data = JSON.parse(resp);
        $('#id_anggota').val(data.id_anggota);
        $('#nim').val(data.nim);
        $('#nama_anggota').val(data.nama_anggota);
        $('#email').val(data.email);
        $('#pangkat').val(data.pangkat);
        $('#telp').val(data.no_hp);
        $('#alamat').val(data.alamat);
        $('#password').val(data.password);
        if (data.foto != null) {
          $('.preview').attr('src', '{{ asset('uploads/anggota/') }}'+'/'+data.foto);
        } else {
          $('.preview').attr('src', '{{ asset('uploads/anggota/default.png') }}');
        }
        $('form#edit_anggota').attr('action', '{{ url('admin/anggota/') }}'+'/'+data.id_anggota+'');
      });
    });

    $(".modal").on("hidden.bs.modal", function(){
        $("#tb_detail tbody tr").remove();
        $(".preview").attr('src','{{ asset('uploads/anggota/default.png') }}');
    });


    function readURL(input) {
      if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
          $('.preview').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
      }
    }

    $(".imgInp").change(function() {
      readURL(this);
    });

    // delete confirm
    $('.confirmDelete').click(function() {
      var id = $(this).data('id');
      swal({
        title: "Anda Yakin ?",
        text: "Data yang anda hapus tidak akan kembali lagi...",
        icon: "warning",
        buttons: ["Batal", "Hapus?"],
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          $('form#hapus').submit()
        } else {
          swal("Data batal dihapus!");
        }
      });
    });

    $('#password, #confirm_password').on('keyup', function () {
      if ($('#password').val() == $('#confirm_password').val()) {
        $('#message').html('Match!').css('color', 'green');
      } else
        $('#message').html('Password Tidak Sama').css('color', 'red');
    });
</script>
