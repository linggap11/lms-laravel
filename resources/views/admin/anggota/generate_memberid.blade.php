@include('admin/_template/css')
<title>{{ $data['title'] }}</title>
<style type="text/css" media="screen">
  th {
    text-align: center; border-right: 2px solid #dddddd;
    border: bo
  }
  td {
    border-right: 2px solid #dddddd;
  }
</style>
@include('admin/_template/header')
<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">

                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Generate Kartu Anggota</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->

    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Data Anggota</h5>
                        <span>Generate Kartu Anggota Yang Telah Terdaftar Di Perpustakaan </span>
                    </div>
                    <div class="card-body">
                       <form target="_blank" action="{{ url('admin/generate_memberid/cetak_kartu') }}" onsubmit="return checkbox_submit()" method="post">
                         {{ csrf_field() }}
                         @if (Session::get('role') == "SU" || Session::get('role') == "SIRKULASI")
                           <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#tambah_dataModal" data-whatever="@mdo"><i class="icofont icofont-barcode"></i>&nbsp; Cetak Kartu Anggota</button>
                         @endif
                         <div class="table-responsive">
                              <table id="table_anggota" class="display">
                                  <thead>
                                      <tr>
                                          <th width="5%">No</th>
                                          <td align="center"><input type="checkbox" name="cetak_all" id="check_all" onclick="toggle(this)"/></td>
                                          <th>Nama Lengkap</th>
                                          <th width="">Email</th>
                                          <th width="">Pangkat</th>
                                          <th width="">No Telp</th>
                                          <th>Status</th>
                                          <th>Masa Berlaku</th>
                                          <th>Aksi</th>
                                      </tr>
                                  </thead>
                                  <tbody>
                                    @php
                                      $no = 1;
                                    @endphp
                                    @foreach ($data['data_anggota'] as $anggota)
                                      <tr>
                                          <td align="center">{{ $no++ }}</td>
                                          <td align="center"><input type="checkbox" name="cetak_kartu[]" value="{{ $anggota->id_anggota }}" /></td>
                                          <td>{{ $anggota->nama_anggota }}</td>
                                          <td>{{ $anggota->email }}</td>
                                          <td align="center">{{ $anggota->pangkat }}</td>
                                          <td>{{ $anggota->no_hp }}</td>
                                          @if ($anggota->status == "AKTIF")
                                              <td align="center"><span class="btn btn-success btn-xs">Aktif</span></td>
                                            @else
                                              <td align="center"><span class="btn btn-danger btn-xs">Tidak Aktif</span></td>
                                          @endif
                                          <td align="center"><strong>{{ $anggota->masa_berlaku }}</strong></td>
                                          <td align="center"><a href="#" onclick="return false" title="Detail Anggota" class="detailClick btn btn-primary btn-xs" data-toggle="modal" data-target="#modalDetail" data-id="{{ $anggota->id_anggota }}"><i class="fa fa-info-circle"> </i></a></td>
                                      </tr>
                                    @endforeach

                                  </tbody>
                              </table>
                          </div>
                       </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalDetail"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog modal-lg" role="document">
       <div class="modal-content">
           <div class="modal-header">
               <h5 class="modal-title">Detail Buku</h5>
           </div>
           <div class="modal-body">
             <div class="container">
                 <div class="row">
                  <div class="col-lg-12">
                    <center>
                      <table id='tb_detail' class="table table-borderless">
                        <thead>
                          <tr>
                            <th width="30%"></th>
                            <th width="70%"></th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                      </table>
                    </center>

                  </div>
                 </div>
               </div>
           </div>
           <div class="modal-footer">
             <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
           </div>
       </div>
     </div>
   </div>
</div>
@include('admin/_template/js')
@include('admin/_template/footer')
<script type="text/javascript">
    $('#table_anggota').DataTable({
      "bLengthChange": false,
      "pageLength": 25,
      "bSort" : false
    });

    $('.detailClick').click(function() {
      var id_anggota = $(this).data('id');
      $.get("{{ url('admin/anggota/') }}"+'/'+id_anggota, function(resp) {
        var data = JSON.parse(resp);
        var content = "";
        if (data.foto == null) {
          content = '<tr><td class="text-center"><img src="{{ asset('uploads/anggota/default.png') }}" title="Tidak Tersedia" width="150px"  alt="cover"></td>'+
          '<td class="detail"><strong>'+data.nama_anggota+' ('+data.nim+')</strong><br>'+
          'Pangkat : <a href="#" onclick="return false" title="Pangkat">'+data.pangkat+'</a> <br>'+
          '<a href="#" onclick="return false" title="'+data.email+'">'+data.email+'</a>  <br>'+
          '<strong>Alamat: </strong> '+data.alamat+'<br>'+
          '<span class="btn btn-primary btn-xs">'+data.status+'</span>&nbsp;<tr>';
        } else {
          content = '<tr><td class="text-center"><img src="{{ asset('uploads/anggota/') }}'+'/'+data.foto+'" title="'+data.foto+'" width="150px" alt="cover"></td>'+
          '<td class="detail"><strong>'+data.nama_anggota+' ('+data.nim+')</strong><br>'+
          'Pangkat : <a href="#" onclick="return false" title="Pangkat">'+data.pangkat+'</a> <br>'+
          '<a href="#" onclick="return false" title="'+data.email+'">'+data.email+'</a>  <br>'+
          '<strong>Alamat: </strong> '+data.alamat+'<br>'+
          '<span class="btn btn-primary btn-xs">'+data.status+'</span>&nbsp;<tr>';
        }

        $('#tb_detail tbody').append(content);

      });
    });

    $(".modal").on("hidden.bs.modal", function(){
        $("#tb_detail tr").remove();
    });


    function toggle(source) {
      checkboxes = document.getElementsByName('cetak_kartu[]');
      for(var i=0, n=checkboxes.length;i<n;i++) {
        checkboxes[i].checked = source.checked;
      }
    }

    function checkbox_submit() {
      checkboxes = document.getElementsByName('cetak_kartu[]');
      var checked = 0;
      for(var i=0, n=checkboxes.length;i<n;i++) {
        if (checkboxes[i].checked) {
          checked++;
        }
      }
      if (checked == 0){
        alert('Tidak Ada Buku Yang Dicetak');
        return false;
      } else {
        return true;
      }
    }
</script>
