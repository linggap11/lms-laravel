@if (Session::has('id_petugas'))
	<script>window.location = "/admin/home";</script>
@endif
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Library management system.">
    <meta name="keywords" content="LMS">
    <meta name="author" content="Lingga">
    <link rel="icon" href="{{ asset('/assets/universal/images/favicon.png') }}" type="image/x-icon"/>
    <link rel="shortcut icon" href="{{ asset('/assets/universal/images/favicon.png') }}" type="image/x-icon"/>
    <title>Halaman Login | Admin</title>

    <!--Google font-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,700" rel="stylesheet">

    <!-- Font Awesome -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/universal/css/font-awesome/css/font-awesome.min.css') }}">

    <!-- ico-font -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/universal/css/icofont/icofont.css') }}">

    <!-- Themify icon -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/universal/css/themify/themify-icons.css') }}">

    <!-- Bootstrap css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/universal/css/bootstrap.css') }}">

    <!-- App css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/universal/css/style.css') }}">

    <!-- Responsive css -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/assets/universal/css/responsive.css') }}">
		<script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>
<body>

<!-- Loader starts -->
<div class="loader-wrapper">
    <div class="loader bg-white">
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
        <div class="line"></div>
        <h4>Halaman Login<span>&#x263A;</span></h4>
    </div>
</div>
<!-- Loader ends -->

<!--Page start-->
<div class="page-wrapper">
    <div class="auth-bg">
        <div class="authentication-box">
            <h4 class="text-center">LOGIN</h4>
            <h6 class="text-center">Login Khusus Petugas Perpustakaan!<br></h6>
            <div class="card mt-4 p-4 mb-0">
							@if(Session::has('alert'))
									<div class="alert alert-danger">
											<div>{{Session::get('alert')}}</div>
									</div>
							@endif
                <form class="theme-form" method="post" action="{{ url('/admin/login') }}">
                  {{ csrf_field() }}
                    <div class="form-group">
                        <label class="col-form-label pt-0">USERNAME</label>
                        <input type="text" id="username" class="form-control" name="username">
                    </div>
                    <div class="form-group">
                        <label class="col-form-label">PASSWORD</label>
                        <input type="password" class="form-control" placeholder="Password" name="password">
                    </div>
										<div class="form-group">
	                    <div class="g-recaptcha" data-sitekey="6LepNJsUAAAAADPeJFYGJ8dEUNT1-uF2Th3G_TkD"></div>
	                  </div>
                    <div class="checkbox p-0">
                        <input id="checkbox1" type="checkbox">
                    </div>
                    <div class="form-group form-row mt-3 mb-0">
                        <div class="col-md-4">
                            <button type="submit" class="btn btn-primary">LOGIN</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
<!--Page ends-->

<!-- latest jquery-->
<script src="{{ asset('/assets/universal/js/jquery-3.2.1.min.js') }}" ></script>

<!-- Bootstrap js-->
<script src="{{ asset('/assets/universal/js/bootstrap/popper.min.js') }}" ></script>
<script src="{{ asset('/assets/universal/js/bootstrap/bootstrap.js') }}" ></script>

<!-- Theme js-->
<script src="{{ asset('/assets/universal/js/script.js') }}" ></script>

</body>
</html>
