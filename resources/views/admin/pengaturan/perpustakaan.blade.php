@include('admin/_template/css')
<title>{{ $data['title'] }}</title>
@include('admin/_template/header')
<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h3>
                        <small></small>
                    </h3>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Pengaturan Perpustakaan</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->

    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Profile Perpustakaan</h5>
                    </div>
                    <div class="card-body">
                      @if (session()->has('message_update'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>Pengaturan Berhasil Diupdate !</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                      @endif
                        <div class="container">
                          <form class="" action="{{ url('admin/setting_perpus/'.$data['data_perpus']->id.'') }}"  method="post" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="row">
                              <!-- left column -->
                              <div class="col-md-3">
                                <div class="text-center">
                                  <img id="preview"  width="150px" height="150px" src="{{ asset('uploads/'.$data['data_perpus']->logo.'') }}" class="avatar img-circle" alt="avatar">
                                  
                                  @if (Session::get('role') == 'SU' || Session::get('role') == 'KEPALA PERPUSTAKAAN')
                                    <h6>Upload a different photo...</h6>
                                    <input type="file" name="logo" id="imgInp" class="form-control" value="{{ $data['data_perpus']->logo }}">
                                  @endif
                                </div>
                              </div>

                              <!-- edit form column -->
                              <div class="col-md-9 personal-info">
                                <h3>Perpustakaan info</h3>

                                  <div class="form-group">
                                    <label class="col-lg-3 control-label">Nama Perpustakaan</label>
                                    <div class="col-lg-8">
                                        <input class="form-control" readonly name="nama" class="nama" type="text" value="{{ $data['data_perpus']->nama_perpus }}">
                                        <input type="hidden" readonly name="id_petugas" value="{{ $data['data_perpus']->id_petugas }}">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-lg-9 control-label">Nama Alias (Nama Ini Akan Ditampilkan Di Logo MAKS 7 Karakter)</label>
                                    <div class="col-lg-8">
                                        <input class="form-control" readonly name="alias" maxlength="7" class="form-control" type="text" value="{{ $data['data_perpus']->alias }}">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-lg-3 control-label">Alamat</label>
                                    <div class="col-lg-8">
                                      <textarea name="alamat" readonly name="alamat" class="form-control" >{{ $data['data_perpus']->alamat }}</textarea>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-lg-3 control-label">No Telp</label>
                                    <div class="col-lg-8">
                                      <input class="form-control" readonly name="telp" type="text" value="{{ $data['data_perpus']->no_telp }}">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-lg-3 control-label">Email</label>
                                    <div class="col-lg-8">
                                      <input class="form-control" readonly type="text" name="email" value="{{ $data['data_perpus']->email }}">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-md-3 control-label"></label>
                                    @if (Session::get('role') == 'SU' || Session::get('role') == 'KEPALA PERPUSTAKAAN')
                                      <div class="col-md-8">
                                      <button type="submit" class="btn btn-primary"> Simpan</button>
                                      <span></span>
                                      <input type="reset" class="btn btn-default" value="Cancel">
                                    </div>
                                    @endif
                                  </div>
                                </form>
                              </div>
                          </div>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts -->
</div>
@include('admin/_template/js')
@if (Session::get('role') == 'SU' || Session::get('role') == 'KEPALA PERPUSTAKAAN')
<script>
  $('.form-control').prop('readonly', false);
</script>      
@endif
@include('admin/_template/footer')
<script type="text/javascript">
  

  function readURL(input) {

  if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('#preview').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#imgInp").change(function() {
    readURL(this);
  });
</script>
