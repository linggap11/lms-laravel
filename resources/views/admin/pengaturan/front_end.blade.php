@include('admin/_template/css')
<!-- Gallery icon -->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/universal/css/photoswipe.css') }}">
<title>{{ $data['title'] }}</title>
@include('admin/_template/header')
<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h3>
                        <small></small>
                    </h3>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Pengaturan SOP</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->

    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Pengaturan Website</h5>
                        <span>Slider Website</span>
                    </div>
                    <div class="card-body">
                        <div class="container">
                          <div class="row">
                            <form class="" action="{{ url('/admin/update_slider') }}" method="post" enctype="multipart/form-data">
                              {{ csrf_field() }}
                              {{ method_field('PUT') }}
                              <div id="aniimated-thumbnials" class="row my-gallery gallery">
                                <figure itemprop="associatedMedia" class="col-md-4 col-6 img-hover hover-1">
                                  <h6>SLIDER <span class="digits">1</span></h6>
                                  <a href="{{ asset("uploads/slider/". $data["slider"]->pict1 ."") }}" itemprop="contentUrl" data-size="1600x950">
                                    <div class="">
                                      <img src="{{ asset("uploads/slider/". $data["slider"]->pict1 ."") }}" id="preview1" itemprop="thumbnail" alt="Slider 1" height="150px" />
                                    </div>
                                  </a>
                                  <input type="file" name="slider1" id="imgInp1" class="form-control" value="{{ $data["slider"]->pict1 }}">
                                  <figcaption itemprop="caption description">Slider 1</figcaption>
                                </figure>
                                <figure itemprop="associatedMedia" class="col-md-4 col-6 img-hover hover-1">
                                  <h6>SLIDER <span class="digits">2</span></h6>
                                  <a href="{{ asset("uploads/slider/". $data["slider"]->pict2 ."") }}" itemprop="contentUrl" data-size="1600x950">
                                    <div class="">
                                      <img src="{{ asset("uploads/slider/". $data["slider"]->pict2 ."") }}" id="preview2" itemprop="thumbnail" alt="Slider 2" height="150px" />
                                    </div>
                                  </a>
                                  <input type="file" name="slider2" id="imgInp2" class=" form-control" value="{{ $data["slider"]->pict2 }}">
                                  <figcaption itemprop="caption description">Slider 2</figcaption>
                                </figure>
                                <figure itemprop="associatedMedia" class="col-md-4 col-6 img-hover hover-1">
                                  <h6>SLIDER <span class="digits">3</span></h6>
                                  <a href="{{ asset("uploads/slider/". $data["slider"]->pict3 ."") }}" itemprop="contentUrl" data-size="1600x950">
                                    <div class="">
                                      <img src="{{ asset("uploads/slider/". $data["slider"]->pict3 ."")}}" id="preview3" itemprop="thumbnail" alt="Slider 3" height="150px"  />
                                    </div>
                                  </a>
                                  <input type="file" name="slider3" id="imgInp3" class=" form-control" value="{{ $data["slider"]->pict3 }}">
                                  <figcaption itemprop="caption description">Slider 3</figcaption>
                                </figure>
                              </div>
                              <br>
                              <div class="form-group pull-right">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> SIMPAN</button>
                              </div>
                            </form>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts -->
</div>
@include('admin/_template/js')
<!-- lightgallery js-->
<script src='{{ asset('assets/universal/js/photoswipe/photoswipe.min.js') }}'></script>
<script src='{{ asset('assets/universal/js/photoswipe/photoswipe-ui-default.min.js') }}'></script>
<!-- Gallery Hover js-->
<script src="{{ asset('assets/universal/js/gallery-hover.js') }}"></script>
@include('admin/_template/footer')
<script type="text/javascript">
  function readURL1(input) {

    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('#preview1').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }

  function readURL2(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('#preview2').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }

  function readURL3(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('#preview3').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }
  $("#imgInp1").change(function() {
    readURL1(this);
  });
  $("#imgInp2").change(function() {
    readURL2(this);
  });
  $("#imgInp3").change(function() {
    readURL3(this);
  });
</script>
