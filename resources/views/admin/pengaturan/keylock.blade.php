@if (Session::get('role') == 'SU' || Session::get('role') == 'KEPALA PERPUSTAKAAN')
  @include('admin/_template/css')
  <title>{{ $data['title'] }}</title>
  @include('admin/_template/header')
  <div class="page-body">
      <!-- Container-fluid starts -->
      <div class="container-fluid">
          <div class="page-header">
              <div class="row">
                  <div class="col-lg-6">
                      <h3>
                          <small></small>
                      </h3>
                  </div>
                  <div class="col-lg-6">
                      <ol class="breadcrumb pull-right">
                          <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                          <li class="breadcrumb-item active">Keylock</li>
                      </ol>
                  </div>
              </div>
          </div>
      </div>
      <!-- Container-fluid Ends -->

      <!-- Container-fluid starts -->
      <div class="container-fluid">
          <div class="row">
              <div class="col-sm-12">
                  <div class="card">
                      <div class="card-header">
                          <h5>Keylock Perpustakaan</h5>
                          <span></span>
                      </div>
                      <div class="card-body">
                        <div class="container">
                          @if (session()->has('message_update'))
                            <div class="alert alert-warning alert-dismissible fade show" style="color:black" role="alert">
                                <strong>Keylock Berhasil Diperbaharui!</strong>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                          @endif
                          <div class="row">
                            <div class="col-sm-12">
                              <form action="setting_keylock/update" method="post" accept-charset="utf-8">
                                   {{ csrf_field() }}
                                   <div class="col-sm-4 ">
                                     <div class="form-group">
                                       <label><strong>KEY 1</strong></label>
                                       <input type="hidden" name="id" value="{{ $data['data_keylock']->id }}">
                                       <input type="text" name="key1" class="form-control" value="{{ $data['data_keylock']->key1 }}">
                                     </div>
                                   </div>
                                   <div class="col-sm-4">
                                     <div class="form-group">
                                       <label><strong>KEY 2</strong></label>
                                       <input type="text" name="key2" class="form-control" value="{{ $data['data_keylock']->key2 }}">
                                     </div>
                                   </div>
                                   <div class="col-sm-4">
                                     <div class="form-group">
                                       <label><strong>KEY 3</strong></label>
                                       <input type="text" name="key3" class="form-control" value="{{ $data['data_keylock']->key3 }}">
                                     </div>
                                   </div>
                                   <div class="col-sm-4">
                                     <div class="form-group">
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <button type="submit" class="btn btn-primary">Simpan</button>
                                     </div>
                                   </div>

                               </form>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <!-- Container-fluid starts -->
  </div>
  @include('admin/_template/js')
  @include('admin/_template/footer')

@else
  <script>window.location = "/home";</script>
@endif
