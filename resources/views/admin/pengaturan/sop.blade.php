@include('admin/_template/css')
<title>{{ $data['title'] }}</title>
@include('admin/_template/header')
<style>
.pdfobject-container { height: 40rem; }
</style>
<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h3>
                        <small></small>
                    </h3>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Pengaturan SOP</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->

    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>S.O.P</h5>
                        <span>Standar Operasional Prosedur Sirkulasi Perpustakaan</span>
                    </div>
                    <div class="card-body">
                      <form action="{{ url('/admin/submit_sop') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                          <label for="upload_file" class="control-label col-sm-3">Upload File (PDF)</label>
                           <div class="col-sm-9">
                             <input class="form-control" type="file" name="sop" id="upload_file" accept="application/pdf" required>
                           </div>
                        </div>
                        <div class="form-group">
                          <div class="col-sm-9">
                            <button type="submit" class="btn btn-primary" name="button">UPLOAD</button>
                          </div>
                        </div>
                        <div id="sop"></div>
                      </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts -->
</div>
@include('admin/_template/js')
<script src="{{ asset('plugins/pdfobj/pdfobject.js') }}" charset="utf-8"></script>
<script>PDFObject.embed("{{ asset('uploads/files/'.$data['sop']->file.'') }}", "#sop");</script>
@include('admin/_template/footer')
<script type="text/javascript">

</script>
