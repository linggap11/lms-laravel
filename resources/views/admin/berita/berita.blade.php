@include('admin/_template/css')
<title>{{ $data['title'] }}</title>
@include('admin/_template/header')
<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h3>
                    </h3>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Berita & Artikel </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->

    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Berita & Artikel </h5>
                    </div>
                    <div class="card-body">
                      @if (session()->has('message_tambah'))
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            Beita <strong>{{ session()->get('message_tambah') }}</strong> berhasil ditambahkan!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                      @elseif (session()->has('message_update'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            Beita <strong>{{ session()->get('message_update') }}</strong> berhasil diupdate!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                      @elseif (session()->has('message_hapus'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            Beita <strong>{{ session()->get('message_hapus') }}</strong> berhasil dihapus!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                      @elseif (session()->has('message_pengumuman'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>Pengumuman berhasil diupdate! </strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                      @endif
                        @if (Session::get('role') == 'SU' || Session::get('role') == 'HUMAS')
                          <a href="{{ url('admin/tambah_berita') }}" class="btn btn-danger">Tambah Data</a>
                        @endif
                        <div class="table-responsive">
                            <table id="table-berita" style="border-style: none">
                                <thead>
                                    <tr>
                                        <th width="5%"></th>
                                        <th width="75%"></th>
                                        <th width="15%"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                      $no = 1;
                                    @endphp
                                    @foreach ($data['data_berita'] as $berita)
                                      <tr>
                                          <td><?= $no++ ?></td>
                                          <td><a href="#" title="{{ $berita->judul }}">{{ $berita->judul }}</a><br>{{ maks_length_admin($berita->body) }}<a href="" title="{{ $berita->judul }}"> Read More</a><br><hr>
                                            <a href="" title="{{ $berita->nama_petugas }}">{{ $berita->nama_petugas }}</a> | <i>{{ $berita->waktu }}</i></td>
                                          <td class="text-center">
                                            @if (Session::get('role') == 'SU' || Session::get('role') == 'HUMAS')
                                              <a href="" title="Edit" class="btn btn-primary btn-xs">Lihat</a>
                                              <a href="{{ url('admin/berita/'.$berita->id_berita.'/edit') }}" title="Edit" class="btn btn-warning btn-xs">Edit</a>
                                              <a href="#" data-id="{{ $berita->id_berita }}"  onclick="return false" title="Hapus" class="confirmDelete btn btn-danger btn-xs">Hapus</a>
                                            @else
                                              <a href="" title="Edit" class="btn btn-primary btn-xs">Lihat</a>
                                            @endif

                                          </td>
                                            <form id="hapus" class="" action="{{ url('admin/berita/'.$berita->id_berita.'') }}" method="post">
                                              {{ csrf_field() }}
                                              {{ method_field('DELETE') }}
                                            </form>
                                      </tr>

                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h5>Pengumuman</h5>
                    </div>
                    <div class="card-body">
                       <form action="{{ url('admin/update_pengumuman/'.$data['pengumuman']->id_pengumuman.'') }}" method="post" accept-charset="utf-8">
                           {{ csrf_field() }}
                           <div class="form-group">
                              <label><strong>Header Pengumuman</strong></label>
                              <input type="text" class="form-control" name="header" placeholder="Header Pengumuman" value="{{ $data['pengumuman']->header }}">
                              <input type="hidden" name="id_petugas" value="{{ Session::get('id_petugas') }}">
                           </div>
                           <div class="form-group">
                              <label><strong>Isi Pengumuman</strong></label>
                              <textarea name="isi" rows="8" class="form-control">{{ $data['pengumuman']->body }}</textarea>
                           </div>
                           <div class="form-group">
                             @if (Session::get('role') == 'SU' || Session::get('role') == 'HUMAS')
                               <button type="reset" class="btn btn-default">Reset</button>
                               <button type="submit" class="btn btn-primary">Simpan</button>
                             @else
                               <button type="reset" class="btn btn-default disabled">Reset</button>
                               <button type="submit" class="btn btn-primary disabled">Simpan</button>
                             @endif

                           </div>
                       </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('admin/_template/js')
@include('admin/_template/footer')

<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" charset="utf-8"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#table-berita').dataTable({
            "orderable": false,
            "ordering": false,
            "searching": false,
            "bInfo": false,

            "bLengthChange": false,
             dom: "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>>" +
                     "<'row'<'col-sm-12'tr>>" +
                     "<'row'<'col-sm-5'i><'col-sm-7'p>>",
        });
    })
    // delete confirm
    $('.confirmDelete').click(function() {
      var id = $(this).data('id');
      swal({
        title: "Anda Yakin ?",
        text: "Data yang anda hapus tidak akan kembali lagi...",
        icon: "warning",
        buttons: ["Batal", "Hapus?"],
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          $('form#hapus').submit()
        } else {
          swal("Data batal dihapus!");
        }
      });
    });
</script>
