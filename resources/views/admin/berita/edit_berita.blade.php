@include('admin/_template/css')
<title>{{ $data['title'] }}</title>
@include('admin/_template/header')
<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h3>
                    </h3>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="{{ url('admin/home') }}"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="{{ url('admin/berita') }}">Berita & Artikel </a></li>
                        <li class="breadcrumb-item active">Tambah Artikel </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->

    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Edit Berita  </h5>
                    </div>
                    <div class="card-body">
                        <form action="{{ url('admin/berita/'.$data['berita']->id_berita.'') }}" method="post" accept-charset="utf-8">
                          {{ csrf_field() }}
                          {{ method_field('PUT') }}
                           <div class="form-group">
                              <label for="recipient-name" class="col-form-label" >Judul</label>
                              <input type="text" name="judul" class="form-control" value="{{ $data['berita']->judul }}" >
                              <input type="hidden" name="id_berita" value="{{ $data['berita']->id_berita }}">
                          </div>
                          <div class="form-group">
                              <label for="recipient-name" class="col-form-label" >Body</label>
                              <textarea name="body" rows="15" class="form-control">{{ $data['berita']->body }}</textarea>
                          </div>
                          <div class="form-group">
                              <button type="reset" class="btn btn-warning">Reset</button>
                              <button type="submit" class="btn btn-primary">Simpan</button>
                          </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin/_template/js')
@include('admin/_template/footer')
<script type="text/javascript">
    $(document).ready(function(){
        $('#table-berita').dataTable({
            "orderable": false,
            "ordering": false,
            "searching": false,
            "bInfo": false,

            "bLengthChange": false,
             dom: "<'row'<'col-sm-3'l><'col-sm-3'f><'col-sm-6'p>>" +
                     "<'row'<'col-sm-12'tr>>" +
                     "<'row'<'col-sm-5'i><'col-sm-7'p>>",
        });
    })
</script>
