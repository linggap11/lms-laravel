@include('admin/_template/css')
<title>{{ $data['title'] }}</title>
@include('admin/_template/header')
<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h3>
                    </h3>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Log</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->

    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card" style="height: 655px">
                    <div class="card-header">
                        <h5>Logging</h5>
                        <span>Log Aktifitas Perpustakaan</span>
                    </div>
                    <div class="card-body">
                        <div class="timeline-item-content">
                            <div class="row">
                                <div class="col-3 date">
                                    <i class="fa fa-file-text"></i>6:00 am
                                    <br />
                                    <small>4 hour ago</small>
                                </div>
                                <div class="col-7 content">
                                    <h6>Peminjaman Buku</h6>
                                    <p class="mb-0">Lorem Ipsum is simply dummy text of the printing.
                                </div>
                            </div>
                        </div>
                        <div class="timeline-item-content">
                            <div class="row">
                                <div class="col-3 date">
                                    <i class="fa fa-file-text"></i>6:00 am
                                    <br />
                                    <small>4 hour ago</small>
                                </div>
                                <div class="col-7 content">
                                    <h6>Peminjaman Buku</h6>
                                    <p class="mb-0">Lorem Ipsum is simply dummy text of the printing.
                                </div>
                            </div>
                        </div>
                        <div class="timeline-item-content">
                            <div class="row">
                                <div class="col-3 date">
                                    <i class="fa fa-file-text"></i>6:00 am
                                    <br />
                                    <small>4 hour ago</small>
                                </div>
                                <div class="col-7 content">
                                    <h6>Peminjaman Buku</h6>
                                    <p class="mb-0">Lorem Ipsum is simply dummy text of the printing.
                                </div>
                            </div>
                        </div>
                        <div class="timeline-item-content">
                            <div class="row">
                                <div class="col-3 date">
                                    <i class="fa fa-file-text"></i>6:00 am
                                    <br />
                                    <small>4 hour ago</small>
                                </div>
                                <div class="col-7 content">
                                    <h6>Peminjaman Buku</h6>
                                    <p class="mb-0">Lorem Ipsum is simply dummy text of the printing.
                                </div>
                            </div>
                        </div>
                        <div class="timeline-item-content">
                            <div class="row">
                                <div class="col-3 date">
                                    <i class="fa fa-file-text"></i>6:00 am
                                    <br />
                                    <small>4 hour ago</small>
                                </div>
                                <div class="col-7 content">
                                    <h6>Peminjaman Buku</h6>
                                    <p class="mb-0">Lorem Ipsum is simply dummy text of the printing.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts -->
</div>
@include('admin/_template/js')
@include('admin/_template/footer')
