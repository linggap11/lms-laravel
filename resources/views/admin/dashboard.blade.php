@include('admin/_template/css')
<title>{{ $data['title'] }}</title>
@include('admin/_template/header')
<div class="page-body">

    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h3>Halaman Dashboard
                        <small>Selamat Datang {{ $data['data_petugas']->nama_petugas }}</small>
                    </h3>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item active"><a href="#"><i class="fa fa-home"></i></a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="row">

            <div class="col-sm-6 col-xl-3 col-lg-6">
                <div class="card o-hidden">
                    <div  class="bg-primary b-r-4 card-body">
                        <div class="media static-top-widget">
                            <div class="align-self-center text-center">
                                <i class="icon-book"></i>
                            </div>
                            <div class="media-body">
                                <span class="m-0">Total Koleksi Buku</span>
                                <h4 class="counter">6659</h4>
                                <i class="icon-book icon-bg"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xl-3 col-lg-6">
                <div class="card o-hidden">
                    <div  class="bg-info b-r-4 card-body">
                        <div class="media static-top-widget">
                            <div class="align-self-center text-center">
                                <i class="icon-user"></i>
                            </div>
                            <div class="media-body">
                                <span class="m-0">Anggota</span>
                                <h4 class="counter">9856</h4>
                                <i class="icon-user icon-bg"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xl-3 col-lg-6">
                <div class="card o-hidden">
                    <div  class="bg-secondary b-r-4 card-body">
                        <div class="media static-top-widget">
                            <div class="align-self-center text-center">
                                <i class="icofont icofont-sign-out"></i>
                            </div>
                            <div class="media-body">
                                <span class="m-0">Peminjaman Buku</span>
                                <h4 class="counter">893</h4>
                                <i class="icofont icofont-sign-out icon-bg"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xl-3 col-lg-6">
                <div class="card o-hidden">
                    <div  class="bg-danger b-r-4 card-body">
                        <div class="media static-top-widget">
                            <div class="align-self-center text-center">
                                <i class="icon-alert"></i>
                            </div>
                            <div class="media-body">
                                <span class="m-0">Keterlambatan</span>
                                <h4 class="counter">2</h4>
                                <i class="icon-alert icon-bg"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-8 col-lg-12">
                <div class="card" style="height: 670px">
                    <div class="card-header">
                        <h5>Timeline</h5>
                        <span>lorem ipsum dolor sit amet, consectetur adipisicing elit</span>
                    </div>
                    <div class="card-body">
                        <div class="timeline-item-content">
                            <div class="row">
                                <div class="col-3 date">
                                    <i class="fa fa-file-text"></i>6:00 am
                                    <br />
                                    <small>4 hour ago</small>
                                </div>
                                <div class="col-7 content">
                                    <h6>Peminjaman Buku</h6>
                                    <p class="mb-0">Lorem Ipsum is simply dummy text of the printing.
                                </div>
                            </div>
                        </div>
                        <div class="timeline-item-content">
                            <div class="row">
                                <div class="col-3 date">
                                    <i class="fa fa-file-text"></i>6:00 am
                                    <br />
                                    <small>4 hour ago</small>
                                </div>
                                <div class="col-7 content">
                                    <h6>Peminjaman Buku</h6>
                                    <p class="mb-0">Lorem Ipsum is simply dummy text of the printing.
                                </div>
                            </div>
                        </div>
                        <div class="timeline-item-content">
                            <div class="row">
                                <div class="col-3 date">
                                    <i class="fa fa-file-text"></i>6:00 am
                                    <br />
                                    <small>4 hour ago</small>
                                </div>
                                <div class="col-7 content">
                                    <h6>Peminjaman Buku</h6>
                                    <p class="mb-0">Lorem Ipsum is simply dummy text of the printing.
                                </div>
                            </div>
                        </div>
                        <div class="timeline-item-content">
                            <div class="row">
                                <div class="col-3 date">
                                    <i class="fa fa-file-text"></i>6:00 am
                                    <br />
                                    <small>4 hour ago</small>
                                </div>
                                <div class="col-7 content">
                                    <h6>Peminjaman Buku</h6>
                                    <p class="mb-0">Lorem Ipsum is simply dummy text of the printing.
                                </div>
                            </div>
                        </div>
                        <div class="timeline-item-content">
                            <div class="row">
                                <div class="col-3 date">
                                    <i class="fa fa-file-text"></i>6:00 am
                                    <br />
                                    <small>4 hour ago</small>
                                </div>
                                <div class="col-7 content">
                                    <h6>Peminjaman Buku</h6>
                                    <p class="mb-0">Lorem Ipsum is simply dummy text of the printing.
                                </div>
                            </div>
                        </div>
                        <a class="pull-right" href="#">Lihat Selengkapnya..</a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-12">
                <div class="card default-widget-count">
                    <div class="card-body">
                        <div class="media">
                            <div class="mr-3 left b-primary">
                                <div class="bg bg-primary"></div>
                                <i class="icon-user"></i>
                            </div>
                            <div class="media-body align-self-center">
                                <h4 class="mt-0 counter">55316</h4>
                                <span>Jumlah Pengunjung Hari Ini</span>
                                <i class="icon-user icon-bg"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card default-widget-count">
                    <div class="card-body">
                        <div class="media">
                            <div class="mr-3 left b-danger">
                                <div class="bg bg-danger"></div>
                                <i class="icofont icofont-share-alt"></i>
                            </div>
                            <div class="media-body align-self-center">
                                <h4 class="mt-0 counter">6643</h4>
                                <span>Peminjaman Hari Ini</span>
                                <i class="icofont icofont-share-alt icon-bg"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card default-widget-count">
                    <div class="card-body">
                        <div class="media">
                            <div class="mr-3 left b-success">
                                <div class="bg bg-success"></div>
                                <i class="icofont icofont-reply"></i>
                            </div>
                            <div class="media-body align-self-center">
                                <h4 class="mt-0 counter">25994</h4>
                                <span>Pengembalian Hari Ini </span>
                                <i class="icofont icofont-reply icon-bg"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card default-widget-count">
                    <div class="card-body">
                        <div class="media">
                            <div class="mr-3 left b-info">
                                <div class="bg bg-info"></div>
                                <i class="icofont icofont-paper"></i>
                            </div>
                            <div class="media-body align-self-center">
                                <h4 class="mt-0 counter">25994</h4>
                                <span>Penambahan Data Buku Baru <br>Hari Ini</span>
                                <i class="icofont icofont-paper icon-bg"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Chart -->
            <div class="col-sm-12 col-xl-12">
                <div class="card" >
                    <div class="card-header">
                        <center><h5>Grafik Pengunjung Enam Bulan Terakhir</h5></center>
                    </div>
                    <div class="card-body chart-block">
                        <div id="column-chart1" class="chart-overflow"></div>
                    </div>
                </div>
            </div>

            <div class="col-xl-6 xl-100">
                <div class="card">
                    <div class="card-header">
                        <h5>Anggota Yang Paling Aktif</h5>
                        <span>Daftar anggota yang sering mengunjungi perpustakaan pada tahun ini (2018)</span>
                    </div>
                    <div class="card-body">
                        <div class="user-status table-responsive">
                            <table class="table table-bordernone">
                                <thead>
                                <tr>
                                    <th scope="col" align="center">Nama Lengkap</th>
                                    <th scope="col" align="center">Kelas</th>
                                    <th scope="col" align="center">Total Kunjung</th>
                                    <th scope="col" align="center">Total Baca Buku</th>
                                    <th scope="col" align="center">Total Pinjam Buku</th>
                                    <th scope="col" align="center">Rank</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="bd-t-none u-s-tb">
                                        <div class="align-middle image-sm-size">
                                            <a href="" title=""><img src="{{ asset('assets/universal/images/user/4.jpg') }}" alt="" class="img-radius align-top m-r-15 rounded-circle">
                                            <div class="d-inline-block">
                                                <h6>John Deo</h6>
                                            </div></a>
                                        </div>
                                    </td>
                                    <td>IPA - 1</td>
                                    <td class="digits">100x</td>
                                    <td class="digits">55x</td>
                                    <td class="digits">30x</td>
                                    <td class="digits font-primary">#1</td>
                                </tr>
                                <tr>

                                    <td class="bd-t-none u-s-tb">
                                        <div class="align-middle image-sm-size">
                                            <a href="" title=""><img src="{{ asset('assets/universal/images/user/1.jpg') }}" alt="" class="img-radius align-top m-r-15 rounded-circle">
                                            <div class="d-inline-block">
                                                <h6>Holio Mako</h6>
                                            </div></a>
                                        </div>
                                    </td>
                                    <td>IPA - 1</td>
                                    <td class="digits">10x</td>
                                    <td class="digits">2x</td>
                                    <td class="digits">2x</td>
                                    <td class="digits font-success">#2</td>
                                </tr>
                                <tr>
                                    <td class="bd-t-none u-s-tb">
                                        <div class="align-middle image-sm-size">
                                            <a href="" title=""><img src="{{ asset('assets/universal/images/user/5.jpg') }}" alt="" class="img-radius align-top m-r-15 rounded-circle">
                                            <div class="d-inline-block">
                                                <h6>Mohsib lara</h6>
                                            </div></a>
                                        </div>
                                    </td>
                                    <td>IPA - 2</td>
                                    <td class="digits">3x</td>
                                    <td class="digits">1x</td>
                                    <td class="digits">1x</td>
                                    <td class="digits font-danger">#3</td>
                                </tr>
                                <tr>
                                    <td class="bd-t-none u-s-tb">
                                        <div class="align-middle image-sm-size">
                                            <a href="" title=""><img src="{{ asset('assets/universal/images/user/6.jpg') }}" alt="" class="img-radius align-top m-r-15 rounded-circle">
                                            <div class="d-inline-block">
                                                <h6>Hileri Soli</h6>
                                            </div></a>
                                        </div>
                                    </td>
                                    <td>IPA - 4</td>
                                    <td class="digits">1x</td>
                                    <td class="digits">1x</td>
                                    <td class="digits">1x</td>
                                    <td class="digits">#4</td>

                                </tr>
                                <tr>
                                    <td class="bd-t-none u-s-tb">
                                        <div class="align-middle image-sm-size">
                                            <a href="" title=""><img src="{{ asset('assets/universal/images/user/7.jpg') }}" alt="" class="img-radius align-top m-r-15 rounded-circle">
                                            <div class="d-inline-block">
                                                <h6>Pusiz bia</h6>
                                            </div></a>
                                        </div>
                                    </td>
                                    <td>IPA - 1</td>
                                    <td class="digits">1x</td>
                                    <td class="digits">1x</td>
                                    <td class="digits">1x</td>
                                    <td class="digits">#5</td>

                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 xl-100">
                <div class="card">
                    <div class="card-header">
                        <h5>Top Buku Tahun Ini</h5>
                        <span>Daftar buku yang sering Dibaca tahun ini (2018)</span>
                    </div>
                    <div class="card-body">
                        <div class="user-status table-responsive">
                            <table class="table table-bordernone">
                                <thead>
                                <tr>
                                    <th scope="col" align="center">Judul Buku</th>
                                    <th scope="col" align="center">Pengarang</th>
                                    <th scope="col" align="center">Penerbit</th>
                                    <th scope="col" align="center">Total Dibaca</th>
                                    <th scope="col" align="center">Total Dipinjam</th>
                                    <th scope="col" align="center">Rank</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="bd-t-none u-s-tb">
                                        <div class="align-middle image-sm-size">
                                            <div class="d-inline-block">
                                                <h6><a href="" title="">Alpro 1</a></h6>
                                            </div>
                                        </div>
                                    </td>
                                    <td>Rinaldi Munir</td>
                                    <td class="digits">INFORMATIKA</td>
                                    <td class="digits">55x</td>
                                    <td class="digits">30x</td>
                                    <td class="digits font-primary">#1</td>
                                </tr>
                                <tr>
                                    <td class="bd-t-none u-s-tb">
                                        <div class="align-middle image-sm-size">
                                            <div class="d-inline-block">
                                                <h6><a href="" title="">Alpro 2</a></h6>
                                            </div>
                                        </div>
                                    </td>
                                    <td>Rinaldi Munir</td>
                                    <td class="digits">INFORMATIKA</td>
                                    <td class="digits">2x</td>
                                    <td class="digits">2x</td>
                                    <td class="digits font-success">#2</td>
                                </tr>
                                <tr>
                                    <td class="bd-t-none u-s-tb">
                                        <div class="align-middle image-sm-size">
                                            <div class="d-inline-block">
                                                <h6><a href="" title="">Alpro 3</a></h6>
                                            </div>
                                        </div>
                                    </td>
                                    <td>Rinaldi Munir</td>
                                    <td class="digits">INFORMATIKA</td>
                                    <td class="digits">1x</td>
                                    <td class="digits">1x</td>
                                    <td class="digits font-danger">#3</td>
                                </tr>
                                <tr>
                                    <td class="bd-t-none u-s-tb">
                                        <div class="align-middle image-sm-size">
                                            <div class="d-inline-block">
                                                <h6><a href="" title="">Alpro 4</a></h6>
                                            </div>
                                        </div>
                                    </td>
                                    <td>Rinaldi Munir</td>
                                    <td class="digits">INFORMATIKA</td>
                                    <td class="digits">1x</td>
                                    <td class="digits">1x</td>
                                    <td class="digits">#4</td>

                                </tr>
                                <tr>
                                    <td class="bd-t-none u-s-tb">
                                        <div class="align-middle image-sm-size">
                                            <div class="d-inline-block">
                                                <h6><a href="" title="">Alpro 5</a></h6>
                                            </div>
                                        </div>
                                    </td>
                                    <td>Rinaldi Munir</td>
                                    <td class="digits">INFORMATIKA</td>
                                    <td class="digits">1x</td>
                                    <td class="digits">1x</td>
                                    <td class="digits">#5</td>

                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts -->
</div>
@include('admin/_template/js')
    <!-- Counter js-->
    <script src="{{ asset('assets/universal/js/counter/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('assets/universal/js/counter/jquery.counterup.min.js') }}"></script>
    <script src="{{ asset('assets/universal/js/counter/counter-custom.js') }}"></script>
    <!-- google Chart JS-->
    <script  src="{{ asset('assets/universal/js/chart/google-chart-loader.js') }}"></script>
    <script  src="{{ asset('assets/universal/js/chart/google-chart.js') }}"></script>
@include('admin/_template/footer')
