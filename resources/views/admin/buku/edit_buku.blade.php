@include('admin/_template/css')
<title>{{ $data['title'] }}</title>
@include('admin/_template/header')
<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h3>
                    </h3>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="{{ url('admin/home') }}"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="{{ url('admin/buku') }}">Buku </a></li>
                        <li class="breadcrumb-item active">Edit Buku </li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->

    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>{{ $data['data_buku']->judul }}</h5>
                    </div>

                    <div class="card-body">
                        <div class="container">
                          <hr>
                          <form class=""  action="{{ url('admin/buku/'.$data['data_buku']->id_buku) }}" method="post" enctype='multipart/form-data'>
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="row">
                              <!-- left column -->
                              <div class="col-md-3">
                                <div class="text-center">
                                  @if ($data['data_buku']->gambar_cover != '' || $data['data_buku']->gambar_cover != null)
                                      <img id="preview"  width="150px" src="{{ asset('uploads/buku/'.$data['data_buku']->gambar_cover) }}" class="avatar" alt="Cover Buku">
                                    @else
                                      <img id="preview"  width="150px" src="{{ asset('uploads/buku/buku_cover_main.png') }}" class="avatar" alt="Cover Buku">
                                  @endif
                                  <h6>Upload Cover Buku...</h6>

                                  <input type="file" name="cover_buku" id="imgInp" class="form-control" value="">
                                </div>
                              </div>

                              <!-- edit form column -->
                              <div class="col-md-9 personal-info">
                                  <div class="form-group">
                                    <label class="col-lg-3 control-label">ISBN</label>
                                    <div class="col-lg-8">
                                        <input class="form-control" name="isbn" class="form-control" type="text" value="{{ $data['data_buku']->isbn }}">
                                        <input class="form-control" name="id_buku" class="form-control" type="hidden" value="{{ $data['data_buku']->id_buku }}">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-lg-9 control-label">Judul</label>
                                    <div class="col-lg-8">
                                        <input class="form-control" name="judul" class="form-control" type="text" value="{{ $data['data_buku']->judul }}">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-lg-9 control-label">Pengarang</label>
                                    <div class="col-lg-8">
                                        <input class="form-control" name="pengarang" class="form-control" type="text" value="{{ $data['data_buku']->pengarang }}">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-lg-9 control-label">Penerbit</label>
                                    <div class="col-lg-8">
                                        <input class="form-control" name="penerbit" class="form-control" type="text" value="{{ $data['data_buku']->penerbit }}">
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-lg-9 control-label">Edisi</label>
                                    <div class="col-lg-8">
                                        <input class="form-control" name="edisi" class="form-control" type="text" value="{{ $data['data_buku']->edisi }}">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-lg-9 control-label">Tahun Terbit</label>
                                    <div class="col-lg-8">
                                        <input class="form-control" name="tahun_terbit" class="form-control" type="text" value="{{ $data['data_buku']->tahun_terbit }}">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-lg-9 control-label">Jumlah Buku</label>
                                    <div class="col-lg-8">
                                        <input class="form-control" name="jumlah_buku" class="form-control" type="number" value="{{ $data['data_buku']->jumlah_buku }}">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-lg-9 control-label">Bentuk Fisik</label>
                                    <div class="col-lg-8">
                                        <select class="form-control" name="bentuk_fisik">
                                          @switch($data['data_buku']->bentuk_fisik)
                                              @case('BUKU')
                                                  <option value="BUKU" selected>BUKU</option>
                                                  <option value="JOURNAL">JOURNAL</option>
                                                  <option value="CD/DVD">CD/DVD</option>
                                                  <option value="MANUSKRIP">MANUSKRIP</option>
                                                @break
                                              @case('JOURNAL')
                                                  <option value="BUKU" >BUKU</option>
                                                  <option value="JOURNAL" selected>JOURNAL</option>
                                                  <option value="CD/DVD">CD/DVD</option>
                                                  <option value="MANUSKRIP">MANUSKRIP</option>
                                                @break
                                              @case('CD/DVD')
                                                  <option value="BUKU" >BUKU</option>
                                                  <option value="JOURNAL">JOURNAL</option>
                                                  <option value="CD/DVD" selected>CD/DVD</option>
                                                  <option value="MANUSKRIP">MANUSKRIP</option>
                                                @break
                                              @case('MANUSKRIP')
                                                  <option value="BUKU" >BUKU</option>
                                                  <option value="JOURNAL">JOURNAL</option>
                                                  <option value="CD/DVD">CD/DVD</option>
                                                  <option value="MANUSKRIP" selected>MANUSKRIP</option>
                                                @break
                                              @default
                                                <option value="BUKU" selected>BUKU</option>
                                                <option value="JOURNAL">JOURNAL</option>
                                                <option value="CD/DVD">CD/DVD</option>
                                                <option value="MANUSKRIP">MANUSKRIP</option>
                                          @endswitch
                                        </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-lg-9 control-label">Kategori</label>
                                    <div class="col-lg-8">
                                      <select class="form-control" name="kategori">
                                        @foreach ($data['kategori'] as $kategori)
                                          @if ($kategori->id_kategori == $data['data_buku']->id_kategori)
                                              <option value="{{ $kategori->id_kategori }}">{{ strtoupper($kategori->nama_kategori) }}</option>
                                            @else
                                              <option value="{{ $kategori->id_kategori }}">{{ strtoupper($kategori->nama_kategori) }}</option>
                                          @endif
                                        @endforeach
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-lg-9 control-label">Jenis Buku</label>
                                    <div class="col-lg-8">
                                      <select class="form-control" name="jenis">
                                        @if ($data['data_buku']->jenis == 'PUBLIC')
                                            <option value="PUBLIC" selected>PUBLIC</option>
                                            <option value="PRIVATE">PRIVATE</option>
                                          @else
                                            <option value="PUBLIC" >PUBLIC</option>
                                            <option value="PRIVATE" selected>PRIVATE</option>
                                        @endif
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-md-3 control-label"></label>
                                    <div class="col-md-8">
                                      <button type="submit" class="btn btn-primary"> Simpan</button>
                                      <input type="button" id="batal" class="btn btn-danger" value="Batal">
                                    </div>
                                  </div>
                                </form>
                              </div>
                          </div>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('admin/_template/js')
@include('admin/_template/footer')
<script type="text/javascript">
  $('#batal').click(function() {
    window.location.replace("{{ url('admin/buku') }}");
  });


  function readURL(input) {

  if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('#preview').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#imgInp").change(function() {
    readURL(this);
  });
</script>
