<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <style>
    .page-break {
        page-break-after: always;
    }
    table {
      border-collapse: collapse;
    }

    table, th {
      border: 3px solid black;
      padding: 10px;
      font-size: 20px
    }
    </style>
  </head>
  <body>
    @foreach ($data_buku as $buku)
      <table>
        <thead>
          <tr>
            <th align="center">PERPUSTAKAAN <br /> SESKOAU</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td align="center">{{ $buku->isbn }}</td>
          </tr>
          <tr>
            @php
              $pengarang = explode(" ", $buku->pengarang);
              $acronym = "";
              foreach ($pengarang as $p) {
                $acronym .= $p[0];
              }
            @endphp
            <td align="center">{{ strtoupper($acronym) }}</td>
          </tr>
          <tr>
            <td align="center">{{ strtolower(substr($buku->judul,0,1)) }}</td>
          </tr>
        </tbody>
      </table>
    @endforeach
  </body>
</html>
