@include('admin/_template/css')
<title>{{ $data['title'] }}</title>
<style type="text/css" media="screen">
  th {
    text-align: center; border-right: 2px solid #dddddd;
    border: bo
  }
  td {
    border-right: 2px solid #dddddd;
  }
  .detail {
    font-size: 18px;
  }
</style>

@include('admin/_template/header')
<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h3>
                        <small></small>
                    </h3>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="{{ url('/admin/home') }}"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Buku</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Koleksi Buku Perpustakaan</h5>
                    </div>
                    <div class="card-body">
                      @if (session()->has('message_tambah'))
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            Buku <strong>{{ session()->get('message_tambah') }}</strong> berhasil ditambahkan!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                      @elseif (session()->has('message_update'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            Buku <strong>{{ session()->get('message_update') }}</strong> berhasil diupdate!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                      @elseif (session()->has('message_hapus'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            Buku <strong>{{ session()->get('message_hapus') }}</strong> berhasil dihapus!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                      @endif
                      @if (Session::get('role') == 'SU' || Session::get('role') == 'ENTRY BUKU')
                        <button type="" class="btn btn-primary" data-toggle="modal" data-target="#tambah_dataModal" data-whatever="@mdo"><i class="icofont icofont-plus"></i>&nbsp; Tambah Data</button>
                      @endif
                        <div class="table-responsive">
                            <table id="data_buku" class="display">
                                <thead>
                                    <tr>
                                     <th rowspan="2" width="5%">No</th>
                                     <th colspan="3">Title</th>
                                     <th colspan="3">Publisher</th>
                                     <th rowspan="2" width="15%">Aksi</th>
                                    </tr>
                                    <tr>
                                      <th>ISBN</th>
                                      <th>Judul</th>
                                      <th width="5%">Edisi</th>
                                      <th>Pengarang</th>
                                      <th>Penerbit</th>
                                      <th>Tahun</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  @php
                                    $no = 1;
                                  @endphp
                                  @foreach ($data['data_buku'] as $buku)
                                    <tr>
                                      <td align="center">{{ $no++ }}</td>
                                      <td>{{ $buku->isbn }}</td>
                                      <td>{{ $buku->judul }}</td>
                                      <td align="center">{{ $buku->edisi }}</td>
                                      <td>{{ $buku->pengarang }}</td>
                                      <td>{{ $buku->penerbit }}</td>
                                      <td align="center">{{ $buku->tahun_terbit }}</td>
                                      <td align="center">
                                        <a href="#" onclick="return false" title="Detail Buku" class="detailClick btn btn-info btn-xs" data-toggle="modal" data-target="#modalDetail" data-id="{{ $buku->id_buku }}"><i class="icon-info-alt"></i></a>
                                        @if (Session::get('role') == 'SU' || Session::get('role') == 'ENTRY BUKU')
                                          <a href="{{ url('admin/buku/'.$buku->id_buku.'/edit') }}"class="btn btn-warning btn-xs" title="Edit"><i class="fa fa-pencil-square-o"></i></a>
                                          <a href="#" onclick="return false" class="confirmDelete btn btn-danger btn-xs" title="Hapus" data-id="{{ $buku->id_buku }}"><i class="fa fa-trash-o"></i> </a>
                                        <form id="hapus" class="" action="{{ url('admin/buku/'.$buku->id_buku) }}" method="post">
                                          {{ csrf_field() }}
                                          {{ method_field('DELETE') }}
                                        </form>
                                        @endif

                                        </td>
                                    </tr>
                                  @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
            </div>
          </div>
        </div>
    </div>
    <!-- Container-fluid starts -->.
    <div class="modal fade" id="modalDetail"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog modal-lg" role="document">
       <div class="modal-content">
           <div class="modal-header">
               <h5 class="modal-title">Detail Buku</h5>
           </div>
           <div class="modal-body">
             <div class="container">
                 <div class="row">
                  <div class="col-lg-12">
                    <center>
                      <table id='tb_detail' class="table table-borderless">
                        <thead>
                          <tr>
                            <th width="30%"></th>
                            <th width="70%"></th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                      </table>
                    </center>

                  </div>
                 </div>
               </div>
           </div>
           <div class="modal-footer">
             <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
           </div>
       </div>
     </div>
   </div>

    <!-- Modal tambah data -->
     <div class="modal fade" id="tambah_dataModal" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <form class=""  action="buku" method="post" enctype='multipart/form-data'>
            {{ csrf_field() }}
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data Buku</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">

              <div class="row">
                <!-- left column -->
                <div class="col-lg-4">
                  <div class="text-center">
                    <img id="preview"  width="150px" height="220px" src="{{ url('uploads/buku/buku_cover_main.png') }}" class="avatar" alt="Cover Buku">
                    <h6>Upload Cover Buku...</h6>

                    <input type="file" name="cover_buku" id="imgInp" class="form-control" value="">
                  </div>
                </div>

                <!-- edit form column -->
                <div class="col-lg-8 personal-info">
                    <div class="form-group">
                      <label for="recipient-name" class="col-form-label" >Kode RFID <span class="btn btn-primary btn-xs">SCAN !</span></label>
                      <input type="text" readonly="" class="form-control">
                    </div>
                    <div class="form-group">
                      <label class="col-lg-3 control-label">ISBN</label>
                      <input class="form-control" name="isbn" class="form-control" type="text" >
                      <input type="hidden" name="id_petugas" value="{{ Session::get('id_petugas') }}">

                    </div>
                    <div class="form-group">
                      <label class="control-label">Judul</label>
                      <input class="form-control" name="judul" class="form-control" type="text" >

                    </div>
                    <div class="form-group">
                      <label class="control-label">Pengarang</label>
                      <input class="form-control" name="pengarang" class="form-control" type="text" >

                    </div>
                    <div class="form-group">
                      <label class="control-label">Penerbit</label>
                      <input class="form-control" name="penerbit" class="form-control" type="text" >

                    </div>

                    <div class="form-group">
                      <label class="control-label">Edisi</label>
                      <input class="form-control" name="edisi" class="form-control" type="text" >

                    </div>
                    <div class="form-group">
                      <label class="control-label">Tahun Terbit</label>
                      <input class="form-control" name="tahun_terbit" class="form-control" type="text" >

                    </div>
                    <div class="form-group">
                      <label class="control-label">Jumlah Buku</label>
                      <input class="form-control" name="jumlah_buku" class="form-control" value="1" type="number" >
                    </div>
                    <div class="form-group">
                      <label class="control-label">Bentuk Fisik</label>
                      <select class="form-control" name="bentuk_fisik">
                        <option value="BUKU" selected>BUKU</option>
                        <option value="JOURNAL">JOURNAL</option>
                        <option value="CD/DVD">CD/DVD</option>
                        <option value="MANUSKRIP">MANUSKRIP</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label class="control-label">Kategori</label>
                      <select class="form-control" name="kategori">
                        @foreach ($data['kategori'] as $kategori)
                          <option value="{{ $kategori->id_kategori }}">{{ strtoupper($kategori->nama_kategori) }}</option>
                        @endforeach
                      </select>

                    </div>
                    <div class="form-group">
                      <label class="control-label">Jenis Buku</label>
                      <select class="form-control" name="jenis">
                        <option value="PUBLIC" selected>PUBLIC</option>
                        <option value="PRIVATE">PRIVATE</option>
                      </select>
                    </div>
                  </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Tambah</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

</div>



@include('admin/_template/js')
@include('admin/_template/footer')
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" charset="utf-8"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#data_buku').DataTable({
      "bLengthChange": false,
      "pageLength": 25,
      "columnDefs": [ {

        } ]
    });
  });


  $('.detailClick').click(function() {
    var id_buku = $(this).data('id');
    $.get("{{ url('admin/buku/') }}"+'/'+id_buku, function(resp) {
      var data = JSON.parse(resp);
      var content = "";
      if (data.gambar_cover == null || data.gambar_cover == "") {
        content = '<tr><td class="text-center"><img src="{{ url('/uploads/buku/buku_cover_main.png') }}" title="Tidak Tersedia" width="150px" alt="cover"></td>'+
        '<td class="detail"><strong>'+data.judul+'</strong><br>'+
        'Oleh <a href="#" onclick="return false" title="'+data.pengarang+'">'+data.pengarang+'</a> <br>'+
        'Penerbit <a href="#" onclick="return false" title="'+data.penerbit+'">'+data.penerbit+'</a> '+data.tahun_terbit+' <br>'+
        '<strong>ISBN: </strong>'+data.isbn+'<br>'+
        '<strong>Lokasi: </strong> Rak 25 - Lantai 1 <br>'+
        '<span class="btn btn-primary btn-xs">Komputer</span>&nbsp; <span class="btn btn-info btn-xs">Public</span></td><tr>';
      } else {
        content = '<tr><td class="text-center"><img src="{{ url('/uploads/buku/') }}'+'/'+data.gambar_cover+'" title="'+data.gambar_cover+'" width="150px" alt="cover"></td>'+
        '<td class="detail"><strong>'+data.judul+'</strong><br>'+
        'Oleh <a href="#" onclick="return false" title="">'+data.pengarang+'</a> <br>'+
        'Penerbit <a href="#" onclick="return false" title="'+data.penerbit+'">'+data.penerbit+'</a>'+data.tahun_terbit+' <br>'+
        '<strong>ISBN: </strong>'+data.isbn+' <br>'+
        '<strong>Lokasi: </strong> Rak 25 - Lantai 1 <br>'+
        '<span class="btn btn-primary btn-xs">Komputer</span>&nbsp; <span class="btn btn-info btn-xs">Public</span></td><tr>';
      }


      $('#tb_detail tbody').append(content);

    });
  });

  $(".modal").on("hidden.bs.modal", function(){
      $("#tb_detail tr").remove();
  });

  function readURL(input) {

  if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function(e) {
        $('#preview').attr('src', e.target.result);
      }
      reader.readAsDataURL(input.files[0]);
    }
  }

  $("#imgInp").change(function() {
    readURL(this);
  });


  // delete confirm
  $('.confirmDelete').click(function() {
    var id_buku = $(this).data('id');
    swal({
      title: "Anda Yakin ?",
      text: "Data yang anda hapus tidak akan kembali lagi...",
      icon: "warning",
      buttons: ["Batal", "Hapus?"],
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $('form#hapus').submit()
      } else {
        swal("Data buku batal dihapus!");
      }
    });
  });
</script>
