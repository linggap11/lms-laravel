@include('admin/_template/css')
<title>{{ $data['title'] }}</title>
<style type="text/css" media="screen">
  th {
    text-align: center; border-right: 2px solid #dddddd;
    border: bo
  }
  td {
    border-right: 2px solid #dddddd;
  }
  .detail {
    font-size: 18px;
  }
</style>
@include('admin/_template/header')
<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h3>
                        <small></small>
                    </h3>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="{{ url('/admin/home') }}"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item active">Label Buku</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Label Buku</h5>
                        <span>Cetak Label Buku </span>
                    </div>
                    <div class="card-body">
                      <form target="_blank" action="{{ url('/admin/label_buku/cetak_label') }}" onsubmit="return checkbox_submit()" method="post">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-primary" data-toggle="modal" data-target="#tambah_dataModal" data-whatever="@mdo"><i class="icofont icofont-barcode"></i>&nbsp; Cetak Label Buku</button>

                        <div class="table-responsive">
                            <table id="data_buku" class="display">
                                <thead>
                                    <tr>
                                     <th rowspan="2" width="5%">No</th>
                                     <th align="center" rowspan="2" ><input type="checkbox" id="check_all" onclick="toggle(this)" value="all"></th>
                                     <th colspan="3">Title</th>
                                     <th colspan="3">Publisher</th>
                                     <th rowspan="2" width="5%">Aksi</th>
                                    </tr>
                                    <tr>
                                      <th>ISBN</th>
                                      <th>Judul</th>
                                      <th width="5%">Edisi</th>
                                      <th>Pengarang</th>
                                      <th>Penerbit</th>
                                      <th width="5%">Tahun</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  @php
                                    $no = 1;
                                  @endphp
                                  @foreach ($data['data_buku'] as $buku)
                                    <tr>
                                      <td>{{ $no++ }}</td>
                                      <td align="center"><input type="checkbox" name="cetak_label[]" value="{{ $buku->id_buku }}"></td>
                                      <td>{{ $buku->isbn }}</td>
                                      <td>{{ $buku->judul }}</td>
                                      <td align="center">{{ $buku->edisi }}</td>
                                      <td>{{ $buku->pengarang }}</td>
                                      <td>{{ $buku->penerbit }}</td>
                                      <td align="center ">{{ $buku->tahun_terbit }}</td>
                                      <td align="center">
                                        <a href="#" onclick="return false" title="Detail Buku" class="detailClick btn btn-info btn-xs" data-toggle="modal" data-target="#modalDetail" data-id="{{ $buku->id_buku }}"><i class="icon-info-alt"></i></a>
                                      </td>
                                    </tr>
                                  @endforeach

                                </tbody>
                            </table>
                        </div>
                      </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts -->.
    <div class="modal fade" id="modalDetail"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
     <div class="modal-dialog modal-lg" role="document">
       <div class="modal-content">
           <div class="modal-header">
               <h5 class="modal-title">Detail Buku</h5>
           </div>
           <div class="modal-body">
             <div class="container">
                 <div class="row">
                  <div class="col-lg-12">
                    <center>
                      <table id='tb_detail' class="table table-borderless">
                        <thead>
                          <tr>
                            <th width="30%"></th>
                            <th width="70%"></th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                      </table>
                    </center>

                  </div>
                 </div>
               </div>
           </div>
           <div class="modal-footer">
             <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
           </div>
       </div>
     </div>
   </div>
</div>
@include('admin/_template/js')
@include('admin/_template/footer')
<script type="text/javascript">
  $(document).ready(function() {
    $('#data_buku').DataTable({
      "bLengthChange": false,
      "pageLength": 25,
      "bSort" : false
    });
  });


  function toggle(source) {
    checkboxes = document.getElementsByName('cetak_label[]');
    for(var i=0, n=checkboxes.length;i<n;i++) {
      checkboxes[i].checked = source.checked;
    }
  }

  $('.detailClick').click(function() {
    var id_buku = $(this).data('id');
    $.get("{{ url('admin/buku/') }}"+'/'+id_buku, function(resp) {
      var data = JSON.parse(resp);
      var content = "";
      if (data.gambar_cover == null || data.gambar_cover == "") {
        content = '<tr><td class="text-center"><img src="{{ url('/uploads/buku/buku_cover_main.png') }}" title="Tidak Tersedia" width="150px" alt="cover"></td>'+
        '<td class="detail"><strong>'+data.judul+'</strong><br>'+
        'Oleh <a href="#" onclick="return false" title="'+data.pengarang+'">'+data.pengarang+'</a> <br>'+
        'Penerbit <a href="#" onclick="return false" title="'+data.penerbit+'">'+data.penerbit+'</a> '+data.tahun_terbit+' <br>'+
        '<strong>ISBN: </strong>'+data.isbn+'<br>'+
        '<strong>Lokasi: </strong> Rak 25 - Lantai 1 <br>'+
        '<span class="btn btn-primary btn-xs">Komputer</span>&nbsp; <span class="btn btn-info btn-xs">Public</span></td><tr>';
      } else {
        content = '<tr><td class="text-center"><img src="{{ url('/uploads/buku/') }}'+'/'+data.gambar_cover+'" title="'+data.gambar_cover+'" width="150px" alt="cover"></td>'+
        '<td class="detail"><strong>'+data.judul+'</strong><br>'+
        'Oleh <a href="#" onclick="return false" title="">'+data.pengarang+'</a> <br>'+
        'Penerbit <a href="#" onclick="return false" title="'+data.penerbit+'">'+data.penerbit+'</a>'+data.tahun_terbit+' <br>'+
        '<strong>ISBN: </strong>'+data.isbn+' <br>'+
        '<strong>Lokasi: </strong> Rak 25 - Lantai 1 <br>'+
        '<span class="btn btn-primary btn-xs">Komputer</span>&nbsp; <span class="btn btn-info btn-xs">Public</span></td><tr>';
      }


      $('#tb_detail tbody').append(content);

    });
  });

  $(".modal").on("hidden.bs.modal", function(){
      $("#tb_detail tr").remove();
  });


  function checkbox_submit() {
    checkboxes = document.getElementsByName('cetak_label[]');
    var checked = 0;
    for(var i=0, n=checkboxes.length;i<n;i++) {
      if (checkboxes[i].checked) {
        checked++;
      }
    }
    if (checked == 0){
      alert('Tidak Ada Buku Yang Dicetak');
      return false;
    } else {
      return true;
    }
  }
</script>
