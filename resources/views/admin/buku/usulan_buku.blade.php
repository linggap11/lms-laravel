@include('admin/_template/css')
<title>{{ $data['title'] }}</title>
<style type="text/css" media="screen">
  th {
    text-align: center; border-right: 2px solid #dddddd;
    border: bo
  }
  td {
    border-right: 2px solid #dddddd;
  }
</style>
@include('admin/_template/header')
<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h3>
                        <small></small>
                    </h3>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="{{ url('/admin/home') }}"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="{{ url('/admin/buku') }}">Buku</a></li></li>
                        <li class="breadcrumb-item active">Usulan Buku</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Usulan Buku</h5>
                        <span>Daftar Pengusulan Buku</span>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="data_buku" class="display">
                                <thead>
                                    <tr>
                                      <th width="5%">No</th>
                                      <th>Tanggal</th>
                                      <th>Judul Buku</th>
                                      <th>Karangan</th>
                                      <th>Nama Pengusul</th>
                                      <th width="15%">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  @php
                                    $no=1;
                                  @endphp
                                  <tr>
                                    <td align="center">{{ $no++ }}</td>
                                    <td align="center">{{ date('d-m-Y') }}</td>
                                    <td>Pengolahan Citra</td>
                                    <td>Munir</td>
                                    <td>Rinaldi (10115319)</td>
                                    <td align="center">
                                      @if (Session::get('role') == 'SU' || Session::get('role') == 'ENTRY BUKU')
                                        <a href="#"  data-toggle="modal" data-target="#edit_kategoriModal" data-id="" onclick="return false" class=" btn btn-success btn-xs" title="Edit">Approve</a>&nbsp;
                                        <a href="#" onclick="return false" class="confirmDelete btn btn-danger btn-xs" title="Hapus" data-id="">Tolak</a>
                                      @else
                                        <a href="#" onclick="return false" class="btn btn-success btn-xs disabled" title="Edit">Approve</a>&nbsp;
                                        <a href="#" onclick="return false" class="btn btn-danger btn-xs disabled" title="Hapus" data-id="">Tolak</a>
                                      @endif
                                    </td>
                                  </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts -->.
</div>
@include('admin/_template/js')
@include('admin/_template/footer')
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" charset="utf-8"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#data_buku').DataTable({
      "bLengthChange": false,
      "pageLength": 25,
      "columnDefs": [ {

        } ]
    });
  });

  $(".modal").on("hidden.bs.modal", function(){
      $("#kategori_baru").val('');
      $("#id_kategori_baru").val('');
  });


  // delete confirm
  $('.confirmDelete').click(function() {
    var id = $(this).data('id');
    swal({
      title: "Anda Yakin ?",
      text: "Data yang anda hapus tidak akan kembali lagi...",
      icon: "warning",
      buttons: ["Batal", "Hapus?"],
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        window.location.replace("{{ url('Buku/hapus_kategori/') }}"+id);
      } else {
        swal("Data batal dihapus!");
      }
    });
  });
</script>
