@include('admin/_template/css')
<title>{{ $data['title'] }}</title>
<style type="text/css" media="screen">
  th {
    text-align: center; border-right: 2px solid #dddddd;
    border: bo
  }
  td {
    border-right: 2px solid #dddddd;
  }
</style>
@include('admin/_template/header')
<div class="page-body">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col-lg-6">
                    <h3>
                        <small></small>
                    </h3>
                </div>
                <div class="col-lg-6">
                    <ol class="breadcrumb pull-right">
                        <li class="breadcrumb-item"><a href="{{ url('/admin/home') }}"><i class="fa fa-home"></i></a></li>
                        <li class="breadcrumb-item"><a href="{{ url('/admin/buku') }}">Buku</a></li></li>
                        <li class="breadcrumb-item active">Kategori Buku</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid Ends -->
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Daftar Kategori Buku Perpustakaan</h5>
                    </div>
                    <div class="card-body">
                      @if (session()->has('message_tambah'))
                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                            Kategori <strong>{{ session()->get('message_tambah') }}</strong> berhasil ditambahkan!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                      @elseif (session()->has('message_update'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            Kategori <strong>{{ session()->get('message_update') }}</strong> berhasil diupdate!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                      @elseif (session()->has('message_hapus'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            Kategori <strong>{{ session()->get('message_hapus') }}</strong> berhasil dihapus!
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                      @endif
                      @if (Session::get('role') == 'SU' || Session::get('role') == 'ENTRY BUKU')
                        <button type="" class="btn btn-primary" data-toggle="modal" data-target="#tambah_dataModal" data-whatever="@mdo"><i class="icofont icofont-plus"></i>&nbsp; Tambah Kategori</button>
                      @endif
                        <div class="table-responsive">
                            <table id="data_buku" class="display">
                                <thead>
                                    <tr>
                                      <th width="7%">No</th>
                                      <th>Nama Kategori</th>
                                      <th width="15%">Jumlah Buku</th>
                                      @if (Session::get('role') == 'SU' || Session::get('role') == 'ENTRY BUKU')
                                        <th width="10%">Aksi</th>
                                      @endif

                                    </tr>
                                </thead>
                                <tbody>
                                  @php
                                    $no=1;
                                  @endphp
                                  @foreach ($data['kategori'] as $kategori)
                                    <tr>
                                      <td align="center">{{ $no++ }}</td>
                                      <td>{{ $kategori->nama_kategori }}</td>
                                      <th>{{ $kategori->jumlah_buku }}</th>
                                      @if (Session::get('role') == 'SU' || Session::get('role') == 'ENTRY BUKU')
                                        <td align="center">
                                          <a href="#"  data-toggle="modal" data-target="#edit_kategoriModal" data-id="{{ $kategori->id_kategori }}" onclick="return false" class="edit_kategori btn btn-warning btn-xs" title="Edit"><i class="fa fa-pencil-square-o"></i></a>&nbsp;
                                          <a href="#" onclick="return false" class="confirmDelete btn btn-danger btn-xs" title="Hapus" data-id="{{ $kategori->id_kategori }}"><i class="fa fa-trash-o"></i></a>
                                        </td>
                                      @endif
                                        <form id="hapus" class="" action="{{ url('admin/kategori_buku/'.$kategori->id_kategori) }}" method="post">
                                          {{ csrf_field() }}
                                          {{ method_field('DELETE') }}
                                        </form>
                                    </tr>
                                  @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Container-fluid starts -->.
    <!-- Modal tambah data -->
     <div class="modal fade" id="tambah_dataModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Kategori Buku</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{ url('admin/kategori_buku') }}" method="post" accept-charset="utf-8">
                  {{ csrf_field() }}
                  <div class="form-group">
                      <label for="recipient-name" class="col-form-label" >Nama Kategori :</label>
                      <input type="text" class="form-control" name="nama_kategori" required="required" Value="">
                  </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Tambah</button>
            </div>
            </form>
        </div>
    </div>
  </div>
  <div class="modal fade" id="edit_kategoriModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
   <div class="modal-dialog" role="document">
     <div class="modal-content">
         <div class="modal-header">
             <h5 class="modal-title">Edit Kategori Buku</h5>
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                 <span aria-hidden="true">&times;</span>
             </button>
         </div>
         <div class="modal-body">
             <form id="formEdit" action="" method="post" accept-charset="utf-8">
               {{ csrf_field() }}
               {{ method_field('PUT') }}
                 <div class="form-group">
                     <label for="recipient-name" class="col-form-label" >Nama Kategori :</label>
                     <input type="text" class="form-control" id="kategori_baru"  name="nama_kategori" required="required" >
                     <input type="hidden" name="id_kategori" id="id_kategori_baru" >
                 </div>

         </div>
         <div class="modal-footer">
             <button type="reset" class="btn btn-warning">Reset</button>
             <button type="submit" class="btn btn-primary">Simpan</button>
         </div>
         </form>
     </div>
 </div>
</div>
</div>
@include('admin/_template/js')
@include('admin/_template/footer')
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" charset="utf-8"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#data_buku').DataTable({
      "bLengthChange": false,
      "pageLength": 25,
      "columnDefs": [ {

        } ]
    });

    $('.edit_kategori').click(function() {
      var id_kategori = $(this).data('id');
      $('#formEdit').attr('action', '{{ url('admin/kategori_buku') }}'+'/'+id_kategori);
      $.get('{{ url('admin/kategori_buku/') }}'+'/'+id_kategori, function(respon) {
        var data = JSON.parse(respon);
        $('#kategori_baru').val(data.nama_kategori);
        $('#id_kategori_baru').val(data.id_kategori);
      });
    });
  });

  $(".modal").on("hidden.bs.modal", function(){
      $("#kategori_baru").val('');
      $("#id_kategori_baru").val('');
  });


  // delete confirm
  $('.confirmDelete').click(function() {
    var id = $(this).data('id');
    swal({
      title: "Anda Yakin ?",
      text: "Data yang anda hapus tidak akan kembali lagi...",
      icon: "warning",
      buttons: ["Batal", "Hapus?"],
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {
        $('form#hapus').submit();
      } else {
        swal("Data batal dihapus!");
      }
    });
  });
</script>
