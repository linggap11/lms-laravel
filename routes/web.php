<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {;
});

// front end
Route::get('/', 'Main\MainController@index');
Route::get('/login', 'Main\MainController@login_page');
Route::get('/register', 'Main\MainController@register_page');
Route::get('/akun', 'Main\MainController@akun');
Route::get('/dashboard', 'Main\AnggotaController@dashboard');
// login sistem
Route::post('login_proses','Main\LoginController@login_proses');
Route::get('logout', 'Main\LoginController@logout');
Route::post('register_submit', 'Main\LoginController@register_submit');
// end login

Route::put('akun/update/{id}', 'Main\AnggotaController@update');

Route::get('/opac', 'Main\SearchController@opac_page');


// dashboard Anggota
Route::get('/anggota/riwayat_peminjaman', 'Main\AnggotaController@riwayat_peminjaman');
Route::get('/anggota/usulan_buku', 'Main\AnggotaController@usulan_buku');
Route::get('/anggota/profile/{id}', 'Main\AnggotaController@profile');

// end front end

// back end
Route::get('/admin', 'Admin\LoginController@index');
Route::post('/admin/login', 'Admin\LoginController@login');
Route::get('/admin/logout', 'Admin\LoginController@logout');

// dashboard
Route::get('/admin/home', 'Admin\AdminController@index');

// Petugas
Route::get('admin/petugas/profile/{id}', 'Admin\PetugasController@show');


// Buku
Route::resource('/admin/buku','Admin\BukuController');
Route::resource('/admin/kategori_buku','Admin\KategoriBukuController');
Route::get('/admin/label_buku', 'Admin\BukuController@label_buku');
Route::get('/admin/usulan_buku', 'Admin\BukuController@usulan_buku');

Route::post('/admin/label_buku/cetak_label', 'Admin\BukuController@cetak_label');

// End Buku

// Anggota
Route::resource('/admin/anggota','Admin\AnggotaController');
Route::get('admin/generate_memberid', 'Admin\AnggotaController@generate_memberid');
Route::post('admin/generate_memberid/cetak_kartu', 'Admin\AnggotaController@cetak_kartu');
// End Anggota

// Opac
Route::get('/admin/opac', 'Admin\OpacController@index');
// End Opac


// Tamu
Route::get('/admin/tamu', 'Admin\TamuController@index');
Route::get('/admin/riwayat_tamu', 'Admin\TamuController@riwayat_tamu');

// End Tamu

// Sirkulasi
Route::get('/admin/peminjaman', 'Admin\SirkulasiController@peminjaman');
Route::get('/admin/pengembalian', 'Admin\SirkulasiController@pengembalian');
Route::get('/admin/riwayat_sirkulasi', 'Admin\SirkulasiController@riwayat_sirkulasi');

Route::get('sirkulasi/get_anggota', 'Admin\SirkulasiController@get_anggota');
Route::get('sirkulasi/get_buku', 'Admin\SirkulasiController@get_buku');
Route::post('sirkulasi/tambah_peminjaman', 'Admin\SirkulasiController@tambah_peminjaman');
Route::get('sirkulasi/get_detail_peminjaman/{id}', 'Admin\SirkulasiController@get_detail_peminjaman');

Route::put('sirkulasi/perpanjangan_buku/{id}', 'Admin\SirkulasiController@perpanjangan_buku');
Route::post('sirkulasi/pengembalian_buku/{id}', 'Admin\SirkulasiController@pengembalian_buku');

// End Sirkulasi


// berita
Route::resource('/admin/berita','Admin\BeritaController');
Route::get('/admin/tambah_berita', 'Admin\BeritaController@tambah_berita');

Route::post('/admin/update_pengumuman/{id}','Admin\BeritaController@update_pengumuman');
// End Berita


// Laporan
Route::get('/admin/laporan_buku', 'Admin\LaporanController@laporan_buku');
Route::get('/admin/laporan_anggota', 'Admin\LaporanController@laporan_anggota');
Route::get('/admin/laporan_ketersediaan_buku', 'Admin\LaporanController@laporan_ketersediaan_buku');
Route::get('/admin/laporan_sirkulasi', 'Admin\LaporanController@laporan_sirkulasi');
Route::get('/admin/laporan_lokasi_buku', 'Admin\LaporanController@laporan_lokasi_buku');
// End Laporan

// Back Up & Restore
Route::get('/admin/backup', 'Admin\BackupController@index');
// End Backup

// Log
Route::get('/admin/log', 'Admin\LogController@index');
// SU
// End Log

// Pengaturan
Route::get('/admin/setting_perpus', 'Admin\PengaturanController@setting_perpus');
Route::put('/admin/setting_perpus/{id}','Admin\PengaturanController@update_perpus');

Route::resource('/admin/setting_petugas', 'Admin\PetugasController');

Route::get('/admin/setting_tampilan', 'Admin\PengaturanController@setting_tampilan');
Route::get('/admin/setting_sop', 'Admin\PengaturanController@setting_sop');
Route::get('/admin/setting_keylock', 'Admin\PengaturanController@setting_keylock');
Route::post('/admin/setting_keylock/update', 'Admin\PengaturanController@update_keylock');

Route::put('/admin/update_slider', 'Admin\PengaturanController@update_slider');
Route::post('/admin/submit_sop', 'Admin\PengaturanController@submit_sop');
// End Pengaturan

// testing
Route::get('/testing', 'Library\SIP2@authenticate');
